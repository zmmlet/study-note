## 1.框架和库的区别

框架：是一套完整的解决方案；对项目的侵入性较大，项目如果需要更换框架，则需要重新架构整个项目
node 中的 express；
库（插件）：提供某一个小功能，对项目的侵入性较小，如果某个库无法完成某些需求，可以很容易切换到其它库实现需求。
从 Jquery 切换到 Zepto
从 EJS 切换到 art-template

## 2.Node（后端）中的 MVC 与 前端中的 MVVM 之间的区别

MVC 是后端的分层开发概念；
MVVM 是前端视图层的概念，主要关注于 视图层分离，也就是说：MVVM 把前端的视图层，分为了 三部分 Model, View , VM ViewModel

![](D:\learningSpace\marktext-images\2022-06-06-19-12-57-image.png)

## 3.Vue 基本代码结构

```html
<!-- 将来new的Vue实例，会控制这个 元素中的所有内容 -->
<!-- Vue 实例所控制字的这个元素区域就是 V -->
<div id="app">
  <p>{{msg}}</p>
  <!-- vue的模板语法{{}}双大括号插值 -->
</div>
```

```js
//1.引包
<script src="../vue-dev/dist/vue.js"></script>
<script>
// 2.创建一个Vue的实例
// 当我们导入包之后，在浏览器的内存中，就多了一个Vue构造函数
// 注意：我们 new 处理啊的这个 vm 对象，就是我们 MVVM 中的 VM调度者
var vm = new Vue({
  //el:表示，当我们new的这个Vue实例要控制页面上的哪个区域
  el: '#app',
  //这里的 data 就是 MVVM 中的M，专门用来保存 每个页面的数据的
  data:{//data属性中存放的是 el 中要用到的数据
    msg: '欢迎学习Vue' //通过Vue提供指令，将数据渲染到页面上（Vue之类的前端框架，不提倡手动操作DOM元素）
  }
})
</script>;

```

一个简单的 Vue 实例只需要四步
![](D:\learningSpace\marktext-images\2022-06-06-19-13-54-image.png)

```

```
