# 目录

- [目录](#目录)
- [Vue Router 笔记](#vue-router-笔记)
  - [全局路由守卫](#全局路由守卫)
  - [为某些路由单独配置守卫](#为某些路由单独配置守卫)
  - [路由组件内的守卫](#路由组件内的守卫)
  - [导航解析流程](#导航解析流程)
  - [点击重复路由控制台报错](#点击重复路由控制台报错)

# Vue Router 笔记

**vue-router 全局有三个守卫**
• router.beforeEach：全局前置守卫，进入路由之前
• router.beforeResolve：全局解析守卫，在 beforeRouteEnter 调用之后调用
• router.afterEach：全局后置钩子，进入路由之后

## 全局路由守卫

```js
import Vue from "vue";
import Router from "vue-router";
import route from "./router";
Vue.use(Router);
const router = new Router({
  routes: route, // 路由列表
});
// 模拟用户登录与否
const HAS_LOGIN = true;
// 全局前置守卫
// 在Router实例上进行守卫
router.beforeEach((to, from, next) => {
  // to和from都是路由实例
  // to：即将跳转到的路由
  // from：现在的要离开的路由
  // next：函数
  // 如果未登录，就跳到登录页，如果登录了，选择哪个页面跳到哪个页面；如果登录了还去了login页面，就跳到首页。
  if (to.name !== "login") {
    if (HAS_LOGIN) next();
    else next({ name: "login" });
  } else {
    if (HAS_LOGIN) next({ name: "home" });
    else next();
  }
});
// 全局解析守卫
router.beforeResolve((to, from.next) => {});
// 全局后置钩子
router.afterEach((to, form) => {});
export default router;
```

**其中 next()函数有几个取值**
• next()：进行管道中的下一个钩子，如果钩子全部都执行完了，那么导航的状态就是 confirmed
• next(false)：中断当前导航。如果 URL 改变了，实际上会重置到 URL 改变前的 from 导航那里。
• next("/")或者 next({path:"/"})：跳转到其他的地址。当前导航被中断，然后跳转到新导航。
• next(error)：如果传入的是一个 error 实例，则导航会被终止，且该错误会被传递给 router.onError()注册过的回调。
一定要确保调用 next()方法

## 为某些路由单独配置守卫

比如：给 home 页面单独配置守卫

```js
{
  path: '/',
  name: "home",
  component: Home,
  // 路由独享守卫
  beforeEnter: (to, from, next) => {
    if(from.name === 'about'){
      alert("这是从about来的")
    }else{
      alert("这不是从about来的")
    }
    next();  // 必须调用来进行下一步操作。否则是不会跳转的
  }
}
```

## 路由组件内的守卫

• beforeRouteEnter()：进入路由前
• beforeRouteUpdate()：路由复用同一个组件时
• beforeRouteLeave()：离开当前路由时
在 Home.vue 页面中举个例子

```js
export default {
  // 组件内守卫
  // 因为这个钩子调用的时候，组件实例还没有被创建出来，因此获取不到this
  beforeRouteEnter(to, from, next) {
    console.log(to.name)
    // 如果想获取到实例的话
    // next(vm=>{
    //   // 这里的vm是组件的实例（this）
    // });
    next()
  },
  // 路由即将要离开的时候调用此方法
  // 比如说，用户编辑了一个东西，但是还么有保存，这时候他要离开这个页面，就要提醒他一下，还没保存，是否要离开
  beforeRouteLeave(to, from, next) {
    const leave = confirm('确定要离开吗？')
    if (leave) next()
    // 离开
    else next(false) // 不离开
  }
}
```

对于 beforeRouteUpdate 的演示例子

```js
beforeRouteUpdate(to,from,next){
  console.log(to.name, from.name);
  next();
},
```

beforeRouteUpdate 被触发的条件是：当前路由改变，但是该组件被复用的时候。
比如说：argu/fu1 到 argu/f2 这个路由，都复用了 arg.vue 这个组件，这个时候 beforeRouteUpdate 就会被触发。可以获取到 this 实例

## 导航解析流程

1、导航被触发。
2、在失活的组件（即将离开的页面组件）里调用离开守卫。 beforeRouteLeave
3、调用全局的 beforeEach 守卫。
4、在重用的组件里调用 beforeRouteUpdate 守卫 (2.2+)。
5、在路由配置里调用（路由独享的守卫） beforeEnter。
6、解析异步路由组件
7、在被激活的组件（即将进入的页面组件）里调用 beforeRouteEnter。
8、调用全局的 beforeResolve 守卫 (2.5+)。
9、导航被确认。
10、调用全局的 afterEach 钩子。所有的钩子都触发完了。
11、触发 DOM 更新。
12、用创建好的实例调用 beforeRouteEnter 守卫中传给 next 的回调函数。

## 点击重复路由控制台报错

```js
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}
```
