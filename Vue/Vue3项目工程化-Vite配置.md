## 使用包管理工具 pnpm 进行依赖管理

[pnpm 官网地址](https://www.pnpm.cn/installation)
本质上他是一个包管理工具，和 npm/yarn 没有区别，主要优势在于

- 包安装速度极快（不会重复安装，重复包 pnpm 会使用直接使用 hard link 去.pnpm-store 找对应版本的包，直接写入到项目的 package.json 文件中）
- 磁盘空间利用效率高（体现在公共仓库.pnpm-store）

## 配置按需加载 ant Design Vue UI 组件

1. 安装 `pnpm add unplugin-vue-components -D`
2. 在 vite.config.js 引入配置

```javaScript
// 引入 ant design vue 按需加载
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";
export default defineConfig({
  // 插件
  plugins: [
    vue(),
    // ant design vue 按需加载
    Components({
      resolvers: [AntDesignVueResolver({ importStyle: "less" })],
    }),
  ]
})
```

## 配置 less 预处理，并自定义 ant Design Vue UI 主题

1. 安装 `pnpm add less -D`
2. 在 vite.config.js 引入配置

```javaScript
export default defineConfig({
  // 插件
  plugins: [
    vue(),
    // ant design vue 按需加载
    Components({
      resolvers: [AntDesignVueResolver({ importStyle: "less" })],
    }),
  ],

  css: {
    preprocessorOptions: {
      // 自定义 ant desing vue 主题样式
      less: {
        modifyVars: {
          "@primary-color": "red",
          "@border-radius-base": "0px", // 组件/浮层圆角
        },
        javascriptEnabled: true,
      },
    },
  },
});
```

## 配置 SCSS 作为项目 CSS 预处理

1. 安装 `pnpm add sass -D`
2. 在 vite.config.js 引入配置

```javaScript
export default defineConfig({

  // 插件
  plugins: [
    vue(),
  ],
  css: {
    preprocessorOptions: {
      // 配置 scss 预处理
      scss: {
        additionalData: '@import "@/scss/index.scss";',
      },
    },
  },
});
```

## 配置系统主题切换

## 配置 gzip 压缩

1. 安装 `pnpm add vite-plugin-compression -D`
2. 在 vite.config.js 引入配置

```javaScript
// 引入 gzip 压缩
import viteCompression from "vite-plugin-compression";
export default defineConfig({
  // 插件
  plugins: [
    vue(),
    // 打包压缩，主要是本地gzip，如果服务器配置压缩也可以
    viteCompression(),
  ]
})
```

3. 配置

## 配置项目打包

```javaScript
export default defineConfig({
  // 打包配置
  build: {
    assetsDir: './static', // 指定生成静态资源的存放路径
    chunkSizeWarningLimit: 500, // hunk 大小警告的限制（以 kbs 为单位）
    minify: 'terser', // 代码混淆
    cssCodeSplit: true, // 如果设置为 false，整个项目中的所有 CSS 将被提取到一个 CSS 文件中
    terserOptions: {
      compress: {
        // warnings: false,
        drop_console: true, // 打包时删除console
        drop_debugger: true, // 打包时删除 debugger
        pure_funcs: ['console.log']
      },
      output: {
        comments: true // 去掉注释内容
      }
    },
    rollupOptions: {
      output: {
        manualChunks: {
          // 拆分代码，这个就是分包，配置完后自动按需加载，现在还比不上webpack的splitchunk，不过也能用了。
          vue: ['vue', 'vue-router', 'vuex']
          // echarts: ['echarts'],
        }
      }
    },
    brotliSize: false // 启用/禁用 brotli 压缩大小报告
  },
})
```

## 配置项目代码风格

## 配置项目代码 .eslintrc 验证

## 完整配置文件 vite.config.js

```js
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// 引入 gzip 压缩
import viteCompression from 'vite-plugin-compression'
// 引入 eslint 插件
import eslintPlugin from 'vite-plugin-eslint'
// 引入 ant design vue 按需加载
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'
// 基于less、sass的 web 应用在线动态主题切换插件
import { themePreprocessorPlugin } from '@zougt/vite-plugin-theme-preprocessor'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  // 打包配置
  build: {
    assetsDir: './static', // 指定生成静态资源的存放路径
    chunkSizeWarningLimit: 500, // hunk 大小警告的限制（以 kbs 为单位）
    minify: 'terser', // 代码混淆
    cssCodeSplit: true, // 如果设置为 false，整个项目中的所有 CSS 将被提取到一个 CSS 文件中
    terserOptions: {
      compress: {
        // warnings: false,
        drop_console: true, // 打包时删除console
        drop_debugger: true, // 打包时删除 debugger
        pure_funcs: ['console.log']
      },
      output: {
        comments: true // 去掉注释内容
      }
    },
    rollupOptions: {
      output: {
        manualChunks: {
          // 拆分代码，这个就是分包，配置完后自动按需加载，现在还比不上webpack的splitchunk，不过也能用了。
          vue: ['vue', 'vue-router', 'vuex']
          // echarts: ['echarts'],
        }
      }
    },
    brotliSize: false // 启用/禁用 brotli 压缩大小报告
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.scss'],
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  },
  optimizeDeps: {
    // 【注意】 排除 import { toggleTheme } from "@zougt/vite-plugin-theme-preprocessor/dist/browser-utils"; 在vite的缓存依赖
    exclude: ['@zougt/vite-plugin-theme-preprocessor/dist/browser-utils']
  },
  // 插件
  plugins: [
    vue(),
    // 打包压缩，主要是本地gzip，如果服务器配置压缩也可以
    viteCompression(),
    // eslint 检查文件
    eslintPlugin({
      include: ['src/**/*.vue', 'src/**/*.js']
    }),
    // ant design vue 按需加载
    Components({
      resolvers: [AntDesignVueResolver({ importStyle: 'less' })]
    }),
    // 基于less、sass的 web 应用在线动态主题切换插件
    themePreprocessorPlugin({
      scss: {
        // 是否启用任意主题色模式，这里不启用
        arbitraryMode: false,
        // 提供多组变量文件
        multipleScopeVars: [
          {
            // 必需
            scopeName: 'theme-default',
            path: path.resolve('src/scss/theme/theme-default.scss')
          },
          {
            scopeName: 'theme-dark',
            path: path.resolve('src/scss/theme/theme-dark.scss')
          }
        ],
        // 【注意】 css中不是由主题色变量生成的颜色，也让它抽取到主题css内，可以提高权重
        includeStyleWithColors: [
          {
            color: '#ffffff'
          }
        ],
        // 在生产模式是否抽取独立的主题css文件，extract为true以下属性有效
        extract: false
      }
    })
  ],

  css: {
    // 配置 scss 预处理
    preprocessorOptions: {
      scss: {
        additionalData: '@import "@/scss/index.scss";'
      },
      // 自定义 ant desing vue 主题样式
      less: {
        modifyVars: {
          '@primary-color': 'red',
          '@border-radius-base': '0px' // 组件/浮层圆角
        },
        javascriptEnabled: true
      }
    }
  },
  server: {
    // open: true, // 自动在浏览器中打开
    host: '0.0.0.0', // 监听所有地址，包括局域网和公网地址
    https: false, // 是否开启https
    port: 3030, // 端口
    ws: false, // 开启 websocket 支持
    proxy: {
      '/api': {
        target: 'http://localhost:3950'
      }
    }
  }
})
```

## 参考连接

- [Vite + ant design vue 按需加载](https://github.com/antfu/unplugin-vue-components)
- [基于 Vite + vue3 + ant design 的定制主题使用方式](https://www.joynop.com/p/454.html)
