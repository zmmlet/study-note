# 开始

## 简介

### 什么是 Vue

1. Vue 用于构建用户界面的 javaScript 框架
2. 基于标准 HTML、CSS、javaScript 构建
3. 提供了一套声明式、组件化的编程模型

### 渐进式框架

Vue 被设计成具有灵活性和可逐步集成的特点，以适应不同需求场景

- 增强静态的 HTML 而无需构建步骤
- 在任何页面中作为 Web Components 嵌入
- 单页应用 (SPA)
- 全栈 / 服务端渲染 (SSR)
- Jamstack / 静态站点生成 (SSG)
- 目标为桌面端、移动端、WebGL，甚至是命令行终端

### 单文件组件

单文件组件，也被称为 \*.vue 文件，英文缩写 SFC

Vue 的单文件组件会将一个组件的逻辑 (JavaScript)，模板 (HTML) 和样式 (CSS) 封装在同一个文件里

### API 风格

Vue 的组件可以按两种不同的风格书写：选项式 API 和组合式 API

#### 选项式 API

我们可以用包含多个选项的对象来描述组件的逻辑，例如  `data`、`methods`  和  `mounted`

选项所定义的属性都会暴露在函数内部的  `this`  上

this 指向当前的组件实例

#### 组合式 API

组合式 API 可以使用导入的 API 函数来描述组件逻辑

在单文件组件中，组合式 API 通常会与 `<script setup>` 搭配使用

`setup` attribute 标识用于告诉 Vue 需要在编译时进行转换，来减少使用组合式 API 时的样板代码

#### 选谁

出于学习目的使用时，推荐采用自己更容易理解的方式

出于生产目的使用时，如果你不需要使用构建工具，或者只在低复杂度的场景中使用 Vue，可以采用选项式 API；当你想用 Vue 构建更大更完整的应用时，推荐使用组合式 API 和单文件组

## 快速开始

## 采用构建工具

### 1. 线上构建

可以通过 `StackBlitz` 在线是哦那个单文件组件尝试 `Vue`
[https://vite.new/vue](https://vite.new/vue)

### 2. 本地构建

命令行运行 `npm init vue@latest` 这一指令将会安装并执行 `create-vue`，它是 Vue 官方的项目脚手架工具
当进入项目目录，分别执行`npm install` 和 `npm run dev`，发布到线上环境使用 `npm run build` 此命令会在 `./dist` 文件夹中构建一个生产环境的版本

## 不使用构建工具

### 1. 在 HTML 文件中引入

### 2.通过 HTTP 提供服务

## 阅读指南

### 1.

### 2.

### 3.

# 基础

## 创建 Vue 应用

### 应用实例

每个 `Vue`应用都是通过 `createApp` 函数创建的一个新的应用实例

```javaScript
const app = createApp({
  /* 跟组件选项 */
})
```

### 根组件

传入 createApp 的对象实际上是一个“跟组件”，其他组件作为根组件的子组件，如果你使用单文件组件，根组件可以从其他文件导入

```javaScript
import App from './App.vue'
const app = createApp(App)
```

### 挂载应用

1. 应用实例必须在调用了 `.mount()` 方法后才会渲染出来
2. `mount()` 接收一个“容器”参数
3. `mount()` 在整个应用配置和资源注册完成后被调用
4. 返回值为根组件实例

### DOM 中的根组件模板

在未采用构建流程的情况下使用 `Vue` 时，可以在挂在容器中直接书写跟着组件模板

```HTML
<div id="app">
  <button @click="count++">{{count}}</button>
</div>
```

当根组件没有设置 `template` 选项时，`Vue`将自动使用容器的 `innerHTML` 作为模板

### 应用配置

应用实例会暴露一个 `.config` 对象允许我们配置一些应用级的选项。例如定义一个应用级的错误处理器

```javaScript
app.config.errorHandle = (err) => {
  /** 处理错误 */
}
```

应用实例还提供了一些方法类注册应用范围内可用的资源，例如注册一个组件

```javaScript
app.component(TodoDeleteButton)
```

### 多个使用实例

`createApp API`允许多个 `Vue`应用共存于同一个页面上，而且每个应用都拥有自己的用于配置和全局资源的作用域

```javaScript
const app1 = createApp({
  /** */
})
app1.mount(container_1)
const app2 = createApp({
  /** */
})
app2.mount(container_2)

```

## 模板语法

### 简介

1. Vue 使用一种基于 HTML 的模板语法
2. 底层机制中，Vue 会将模板编译成高度优化的 JavaScript 代码
3. 你也可以结合可选的 JSX 支持直接手写渲染函数而不采用模板，但这将不会享受到和模板同等级别的编译时优化

### 文本插值

1. 文本插值使用“Mustache”语法 (即双大括号) `Message: {{ msg }}`
2. 双大括号标签会被替换为相应组件实例中 `property`的值，同时具有响应式

### 原始 HTML

1. 若想在模板中插入 HTML，需要使用 `v-html` 指令
2. 注意
   - 不能使用 `v-html` 来拼接组合模板
   - 在网站上动态渲染任意 html 是非常危险的行为

### Attribute 绑定

1. 双大括号不能在 `HTML attributes` 中使用，但可以使用 `v-bind` 指令

```HTML
<div v-bind:id="dynamicId"></div>
```

将元素的 id attribute 与组件的 dynamicId property 保持一致，如果绑定的值是 null 或者 undefined，那么该 attribute 将会从渲染的元素上移除

2. 以上例子可以缩写为 `<div :id="dynamicId"></div>`
3. 布尔型 `Attribute`
   - 布尔型 `attribute` 就那些依据 `true/false` 值决定是否应该在该元素上存在的 `attribute`
   - 例如：`<button :disabled="isButtonDisabled">Button</button>`当 isButtonDisabled 为真值或一个空字符串 (即 <button disabled=''>) 时，元素会包含这个 `disabled attribute`。而当其为假值时 `attribute` 将被忽略。
4. 动态绑定多个值，如果你有一个包含多个 `attribute` 的 JavaScript 对象，通过不带参数的 `v-bind`，你可以将它们绑定到单个元素上`<div v-bind="objectOfAttrs"></div>`

### 使用 js 表达式

1. Vue 在所有的数据绑定中都支持完整的 JavaScript 表达式，这些表达式都会被作为 JavaScript ，以组件为作用域解析执行
2. 模板中的应用
   - 在文本插值中 (双大括号)
   - 在任何 Vue 指令 (以 **`v-`** 开头的特殊 attribute) `attribute` 的值中
3. 注意点
   - 仅支持表达式，不能是一个语句
   - 受限的全局访问
     - 模板中的表达式将被沙盒化，仅能够访问到有限的全局对象列表
     - 没有显式包含在列表中的全局对象将不能在模板内表达式中访问
     - 当然可以自行在 app.config.globalProperties 上显式地添加他们，供所有的 Vue 表达式使用

### 指令

#### 简介

1. 指令是带有 `v-` 前缀的特殊 `attribute`
2. 指令 `attribute` 的期望值为一个 JavaScript 表达式
3. 使用指令是为了在其表达式值变化时响应式地对 `DOM` 应用更新

#### 参数

某些指令会需要一个“参数”，使用冒号分隔`<a v-bind:href="url">...</a>`
这里 `href` 就是一个参数，它告诉 `v-bind` 指令将表达式 `url` 的值绑定到元素的 `href attribute` 上

#### 动态参数

1. 在指令参数上也可以使用一个 JavaScript 表达式，但需要包含在一对方括号内
2. 动态参数值的限制
   - 特殊值 null 意为显式移除该绑定
   - 任何其他非字符串的值都将触发一个警告
3. 动态参数语法的限制：空格和引号，在 HTML attribute 名称中都是不合法的

#### 修饰符

修饰符是以点开头的特殊后缀，表明指令需要以特殊的方式被绑定。语法`v-on:submit.prevent="onSubmit"`

## 响应式基础

### 声明响应式状态

#### 1.reactive

可以使用 reactive() 函数创建一个响应式对现象或数组

```javaScript
const state = reactive({count: 0})
```

Vue 能够跟踪对响应式对象的属性的访问与更改操作
如果要在组件模板中使用响应式状态，需要在 setup() 函数中定义并返回【缺点：每次手动暴露状态和方法非常繁琐，`<script setup>`孕育而生】

```javaScript
import { reactive } from vue;

export default {
  // `setup` 是一个专门用于组合式 API 的特殊钩子
  setup() {
    const state = reactive({ count: 0 })
    // 暴露 state 到模板
    return {
      state
    }
  }
}

// 模板中使用时
<div>{{ state.count }}</div>
```

#### 2.`<script setup>`

##### **特点：**

`<script setup>`中的顶层的导入和变量声明都将会自动的在该组件模板上可用

```javaScript
<script setup>
import { reactive } from vue;

const state = reactive({ count: 0 })

function increment() {
  state.count++
}
</script>

<template>
  <button @click='increment'>
    {{ state.count }}
  </button>
</template>
```

##### **DOM 更新机制：**

- 无论改变多少个状态，vue 都会将它们推入更新循环的 `下个tick`执行，以确保每个需要需要更新的组件都只会更新一次
- 若要等待一个状态改变后的 DOM 更新完成，你可以使用 `nextTick()`

```javaScript
import { nextTick } from vue;
function increment() {
  count.value++
  nextTick(() => {
    // 访问更新后的 DOM
  })
}
```

##### **深层响应性：**

在 Vue 中，状态都是默认深层响应式的

##### **响应式代理 vs. 原始对象：**

- reactive() 返回的是一个源对象的 Proxy
- 只有代理是响应式的，更改原始的对象不会触发更细腻，因此，使用 Vue 的响应式系统的最佳实践是仅使用代理作为状态
- 为保证访问代理的一致性，对同一个对象调用 reactive() 会总是返回同样的代理，而对代理调用 reactive() 则会返回它自己

```javaScript
// 在同一个对象上调用 reactive() 会返回相同的代理
console.log(reactive(raw) === proxy) // true

// 在一个代理上调用 reactive() 会返回它自己
console.log(reactive(proxy) === proxy) // true
```

##### **reactive() 的局限性：**

- 仅对对象类型有效（对象、数组和 Map、Set 这样的集合类型），而对 string、number 和 boolean 这样的基础类型无效
- 因为 Vue 的响应式系统是通过属性访问进行追踪的，因此我们必须始终保持对该响应式对象的引用

### ref() 定义响应式变量

#### ref

- 为了解决 `reactive()` 带来的限制，我们使用可以装载任何值类型的 ref 函数，带来响应式的 ref
- `ref()` 从参数中获取到值，将其包装为一个带 `.value` 属性的对象
- `ref` 的 `.value` 属性也是响应式的
- 当值为对象类型时，会用 `reactive()` 自动转换它的 `.value`
- 一个包含对象类型值的 fef 可以响应式的替换整个对象
- `ref` 被传递给函数或是从一般对象上解构时，不会丢失响应性
- 小结：`ref()`使我们能创造一种对任何值的“引用”并能够不丢失响应性的随意传递

#### ref 在模板中的解包

- 当 `ref` 模板中作为顶层 `property` 被访问时，它们会被自动“解包”，所有不需要使用 `.value`
- 访问深层级的 `ref` 则不会解包

#### ref 在响应式对象中的解包

- 当一个 `ref` 作为一个响应式对象的 `property` 被访问或更改时，它会自动解包
- 如果将一个新的 `ref`赋值给响应式对象某个已经为 `ref` 的属性，那么他会替换掉旧的 `ref`
- 只有当嵌套在一个深层反应是对象内时，才会发生 `fef` 解包
- **数组和集合类型的 ref 解包**当从数组或 `Map`这样的原生集合类型访问 ref 时，不会进行解包

### 响应式语法糖

1. 不得不对 ref 使用 .value 是一个受限于 javaScript 语言限制的缺点
2. Vue 提供了一个语法糖，可以在编译时作相应转换，但还处于实验性阶段

## 计算属性

### 简介

- 模板中的表达式虽然方便，但也只能用来做简单操作，如果在模板中写太多逻辑，会变得臃肿难以维护，所以推荐使用计算属性来描述依赖响应式的复杂逻辑
- computed() 方法期望收到一个 getter 函数，返回值为一个计算属性 ref
- Vue 的计算属性会自动追踪响应式依赖

### 计算属性缓存 vs 方法

- 若我们将同样的函数定义为一个方法而不是计算属性，两种方式在结果上确实是完全相同的
- 不同之处在于计算属性会基于其响应式依赖被缓存

### 可写计算属性

- 计算属性默认仅能通过计算函数得出结果，尝试修改一个计算属性时，你会收到一个运行时警告
- 只能在某些特殊场景中你可能才需要用到“可写”的属性，可以通过同时提供 getter 和 setter 来创建

```javaScript
<script setup>
import {ref, computed} from 'vue'
const firstName = ref("张三")
const lastName = ref("李四")

const fullName = computed({
  // getter
  get() {
    return firstName.value + lastName.value
  }
  // setter
  set(newValue) {
    // 使用解构赋值语法
    [firstName.value, lastName.value] = newValue.split(";")
  }
})
</script>
```

### 最佳实践

1. 计算后汉书不应有副作用：计算属性的计算函数应只做计算而没有其他任何副作用
2. 避免直接修改计算属性值
   - 从计算属性返回值是派生状态，可以把它看作是一个“临时快照”，每当源状态发生变化时，就会创建一个新的快照
   - 因此更改快照是没有意义的，应该更新它所以依赖的源状态，以触发新一次计算

## 类与样式绑定

### 简介

1. 数据绑定一个常见需求场景是操作元素的 css 类列表和内联演示
2. 因为他们都是 attribute 我们可以使用 v-bind 来绑定，只需通过表达式计算出一个字符串作为最终结果即可
3. 频繁的凭借字符串，很容易出错和影响性能，因为 Vue 专门为 class 和 style 的 v-bind 用法提供了特殊的功能增强，除了字符串外，表达式的结果还可以是对象或数组

### 绑定 HTML 类

#### 绑定对象

- 我们可以给 :class(v-bind:class 的缩写) 传递一个对象来动态切换类举例：`<div :class="{actice: isActive}"></div>` active 是否存在取决于 isActive 的真假
- 也可以在对象中写多个字段来操作多个类
- :class 指令也可以和一般的 class attribute 共存

```HTML
<div
  class='static'
  :class='{ active: isActive,;text-danger: hasError }'
></div>

<!-- 它将会被渲染成： -->

<div class='static active'></div>
```

- 绑定返回对象的一个计算属性

```javaScript
import {ref,computed} from 'vue'

const isActive = ref(true)
const error = ref(null)

const classObject = computed(() => {
  actice: isActive.value && !error.value;
})

<div :class="classObject"></div>
```

#### 绑定数组

- 可以给 :class 绑定一个数组以应用一系列 CSS 类

```javaScript
const activeClass = ref(&#39;active&#39;)
const errorClass = ref(&#39;text-danger&#39;)
<div :class='[activeClass, errorClass]'></div>
// 渲染的结果是：
<div class='active text-danger'></div>
```

- 按条件触发某个类，可以使用三元表达式
- 在有多个依赖条件的类时会有些冗长。因此也可以在数组中使用对象语法

```HTML
<div :class='[{ active: isActive }, errorClass]'></div>
```

#### 和组件配合

- 对于只有一个根元素的组件，当你使用了 class attribute 时，这些类会被添加到根元素上，并与该元素上已有的类合并
- 如果你的组件有多个根元素，你将需要指定哪个根元素来接收这个类。你可以通过组件的 $attrs property 来实现指定

### 绑定内联样式

1. 绑定对象
   - :style 支持绑定 JavaScript 对象值，对应的是 HTML 元素的 style 属性
   - 尽管推荐使用 camelCase，但 :style 也支持 kebab-cased 形式的 CSS 属性 key
   - 直接绑定一个样式对象通常是一个好主意，这样可以使模板更加简洁【推荐使用】
   ```javaScript
   const styleObject = reactive({
      color: 'red',
      fontSize: '13px';
   })
   <div :style='styleObject'></div>
   ```
2. 绑定数组
   可以给 :style 绑定一个包含多个样式对象的数组。这些对象会被合并和应用到同一元素上
3. 自动前缀
   - 当你在 :style 中使用了需要浏览器特殊前缀的 CSS 属性时，Vue 会自动为他们加上相应的前缀。
4. 样式多值
   - 可以对一个样式属性提供多个 (不同前缀的) 值
   - 数组仅会渲染浏览器支持的最后一个值

## 条件渲染

### v-if

1. **v-if 指令碑额用于按照条件渲染一个区块** `<h1 v-if="awesome">Vue is awesome!</h1>`
2. **这个区块只会在指令的表达式为 true 时被渲染**

### v-else

1. 可以使用 `v-else` 为 `v-if` 添加一个 `else` 区块
2. 一个 `v-else` 元素必须跟一个 `v-if` 或者 `v-else-if` 元素后面，否则将不会识别它
3. v-else-if
   - `v-else-if` 提供的时相应于 `v-if` 的 `else if` 区块
   - 可以连续多次重复使用
   - 和 `v-else` 相似，一个使用 `v-else-if` 的元素必须紧跟一个 `v-if` 或一个 `v-else-if` 元素后面

### `<template>上的 v-if`

1. 因为 v-if 是一个指令，它必须依附于某个元素，如果切换的不止一个元素？这种情况下我们可以在 `<template>` 元素上使用 v-if 这只是一个不可见的包装器元素，最后渲染的结果并不会包含这个 `<template>` 上使用
2. v-else 和 v-else-if 也可以使用 `<template>` 上使用
3. v-show
   - 也是用来按条件显示一个元素
   - 不同的是，v-show 仅切换了该元素上名为 display 的 CSS 属性
   - 而且，v-show 不支持在 `<template>` 元素上使用，也没有 v-else 来配合
4. v-if vs v-show
   - v-if 是“真实的”按条件渲染，因为它确保了条件区块内的事件监听和子组件都会在切换时销毁和重建
   - v-if 也是懒加载的：如果在初次渲染时条件为 false 则不会做任何事情，条件渲染区块直到条件首次变为 true 时才会渲染
   - 相比之下：v-show 简单许多，元素无论初始条件如何，始终会被渲染，仅作 css 类的切换
   - 总的来说 **v-if 在首次渲染时的切换成本比 v-show 更高，因此当你需要非常频繁切换时 v-show 会更好，而运行时不常改变的时候 v-if 更合适**
5. v-if 和 v-for
   - 当 v-if 和 v-for 同时存在于一个元素上的时候，v-if 会首先被执行
   - 同时使用 v-if 和 v-for 是不推荐的，因为这样二者的优先级不明显

## 列表渲染

### v-for

1. 可以使用 v-for 指令基于一个数组来渲染一个列表

```HTML
<li v-for='item in items'>
  {{ item.message }}
</li>
```

2. 在 v-for 块中可以完整的访问父级作用域内的 property
3. 也支持使用可选的第二个参数表示当前项的位置索引`<li v-for="(item,index) in items"></li>`
4. 对于多层嵌套的 v-for 作用域的工作方式和函数的作用域很类似，每个 v-for 作用域都可以访问到父级作用域
5. 也可以使用 of 作为分隔符来代替 in，这也和 javaScript 的迭代器语法非常相似

### v-for 与对象

1. 可以使用 v-for 来遍历一个对象的所有属性
2. 也可以提供第二个参数表示属性名
3. 第三个参数表示位置索引 `<li v-for="(value,key,index) in myObject"></li>`

### v-for 和范围值

1. 可以直接传递给 v-for 一整个数值 `<span v-for="n in 10">{{n}}</span>`
2. 在这种用例中，会将该模板基于 1...n 的去找范围重复多次

### `<template>上的 v-for`

与模板上的 `v-if` 类似，你可以在 `<template>` 标签上使用 `v-for` 来渲染一个包含多个元素的块

```HTML
<ul>
  <template v-for='item in items'>
    <li>{{ item.msg }}</li>
    <li class='divider' role='presentation'></li>
  </template>
</ul>
```

### v-for 和 v-if

1. 同时使用 `v-if` 和 `v-for` 是不推荐的，因为这样二者的优先级不明显
2. 当他们同时存在于一个节点上时，`v-if` 比 `v-for` 的优先级更高，这意味着 `v-if` 的条件将无法访问到 `v-for` 作用域内定义的变量别名
   ```HTML
   <!--
   这会抛出一个错误，因为属性 todo 此时
   没有在该实例上定义
   -->
   <li v-for='todo in todos' v-if='!todo.isComplete'>
     {{ todo.name }}
   </li>
   ```
3. 在外包装一层 `<template>` 再其上使用 `v-for` 可以解决这个问题

### 通过 key 管理状态

1. Vue 默认按照“就地更新”的策略来更新通过 `v-for` 渲染的元素列表。当数据项的顺序改变时，Vue 不会随之移动 `DOM` 元素的顺序，而就地更新每个元素，确保他们在本地指定的索引位置上渲染
2. 默认模板时高效的，但只适用于列表渲染输出不依赖子组件状态或者临时 `DOM` 状态（例如:表单输入值）
3. 为了给 Vue 一个提示，以便它可以跟踪每个节点的标识，从而重用和重新排序现有的元素，需要为每个项目提供一个唯一的 `key attribute`
4. 当使用 `<template v-for>` 时，也应该将 key 放在 `<template>` 容器上
   ```HTML
   <template v-for='todo in todos' :key='todo.name'>
    <li>{{ todo.name }}</li>
   </template>
   ```
5. 推荐在任何可行的时候，为 `v-for` 提供一个 `key attribute` 除非所迭代的 `DOM` 内容非常简单，或者有意依赖默认行为来获得性能增益
6. `key` 绑定的期望值是一个基础类型的值，列如字符串或 number 类型，不要用对象作为 `v-for` 的 `key`

### 组件上使用 v-for

1. 可以直接在组件使用 v-for 和其他任何一般的元素没区别
2. 但是，组件上使用 v-for 不会自动将任何组件传递给组件，因为组件有自己的作用域

### 数组变化侦测

### 展示过滤或排序后的结果

## 事件处理

## 表单输入绑定

## 生命周期钩子

## 模板 ref

## 组件基础

# 深入组件

# 可重用性

# 内置组件

# 升级规模

# 最佳实践

# TypeScript

# 进阶
