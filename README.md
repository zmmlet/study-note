# study-note 项目

记录学习笔记、项目问题记录，和知识总结等

## 笔记范围

```
gitee-study-note
├─ C++
│  ├─ C++.md
│  ├─ opencv
│  │  ├─ 00测试项目配置.cpp
│  │  ├─ 01图像、视频、网络摄像头.cpp
│  │  ├─ 02图像基本功能.cpp
│  │  ├─ 03调整大小并裁剪图像大小.cpp
│  │  ├─ 04图形形状和文本.cpp
│  │  ├─ 05扭曲和透视.cpp
│  │  ├─ 06颜色检测.cpp
│  │  ├─ 07形状轮廓检测.cpp
│  │  ├─ 08人脸检测.cpp
│  │  ├─ 09检测物体轮廓.cpp
│  │  ├─ 10文件扫描仪.cpp
│  │  ├─ 11车牌检测器.cpp
│  │  ├─ 12口罩识别.cpp
│  │  ├─ 13去除票据印章及通道分离.cpp
│  │  ├─ 14马赛克原理之简单提高图像算法性能.cpp
│  │  ├─ 15图像增强与图像去噪，图像分割之边缘检测.cpp
│  │  ├─ 16图像处理之线性滤波.cpp
│  │  ├─ 17图像噪声.cpp
│  │  ├─ 18视频运动目标捕捉.cpp
│  │  ├─ my_face_project.sln
│  │  ├─ my_face_project.vcxproj
│  │  ├─ my_face_project.vcxproj.filters
│  │  ├─ my_face_project.vcxproj.user
│  └─ OpenCV笔记.md
├─ CSS
│  ├─ CSS伪类与伪元素.md
│  ├─ CSS笔记.md
│  └─ SCSS语法笔记.md
├─ Dart
│  ├─ 001.变量常量数据类型.dart
│  ├─ 002.字符串类型.dart
│  ├─ 003.数值类型.dart
│  ├─ 004.布尔类型.dart
│  ├─ 005.List集和类型.dart
│  ├─ 006.Map类型.dart
│  ├─ 007.类型判断.dart
│  ├─ 008.算数运算符.dart
│  ├─ 009.关系运算符.dart
│  ├─ 010.逻辑运算符.dart
│  ├─ 011.赋值运算符.dart
│  ├─ 012.条件表达式.dart
│  ├─ 013.类型转换.dart
│  ├─ 014.for循环以及循环遍历List.dart
│  ├─ 015.while do..while.dart
│  ├─ 016.List Set Map 详解 以及循环语句 forEach map where any every.dart
│  ├─ 017.方法的定义 变量 方法作用域.dart
│  ├─ 018.方法传参 默认参数，可选参数，参数命名，方法作为参数.dart
│  ├─ 019.箭头函数 函数的相互调用.dart
│  ├─ 020.匿名方法 自执行方法 方法的递归.dart
│  ├─ 021.闭包.dart
│  ├─ 022.Dart面向对象介绍以及Dart内置对象.dart
│  ├─ 023.创建类，使用类.dart
│  ├─ 024.自定义类的默认构造函数.dart
│  ├─ 025.自定义类的命名构造函数.dart
│  ├─ 026.把类单独抽离成一个模块.dart
│  ├─ 027.私有方法和私有属性.dart
│  ├─ 028.类中的getter和setter修饰符的用法.dart
│  ├─ 029.类中的初始化列表.dart
│  ├─ 030.类的静态成员 静态方法.dart
│  ├─ 031.对象的操作符.dart
│  ├─ 032.类的继承-简单继承.dart
│  ├─ 033.类的继承 super 关键字的使用 实例化子类给父类构造函数传参.dart
│  ├─ 034.类的继承 覆写父类的方法.dart
│  ├─ 035.抽象类.dart
│  ├─ 036.多态.dart
│  ├─ 037.接口.dart
│  ├─ 038.接口-抽离.dart
│  ├─ 039.implements实现多个接口.dart
│  ├─ 040.mixins.dart
│  ├─ 041.mixins的类型.dart
│  ├─ 042.泛型 泛型方法.dart
│  ├─ 043.泛型 泛型类.dart
│  ├─ 044.泛型 泛型接口.dart
│  ├─ 045.库.dart
│  ├─ 046.导入自己本地库.dart
│  ├─ 047.导入系统内置库 math 库以及 Number类型上面的一些函数方法.dart
│  ├─ 048.导入系统内置库实现请求数据httpClient.dart
│  ├─ 049.关于 Async Await.dart
│  ├─ 050.导入 Pub管理系统中的库.dart
│  ├─ 051.库的重命名 Dart 冲突解决.dart
│  ├─ 052.库的部分导入.dart
│  ├─ 053.延迟加载.dart
├─ Java
│  ├─ 01-Java基础.md
│  ├─ 02-MySql.md
│  ├─ 03-开发框架.md
│  └─ 04缓存.md
├─ javaSctiprt
│  ├─ ES6.md
│  ├─ javaScript笔记.md
│  └─ utils
│     ├─ arr.js
│     ├─ check.js
│     ├─ client.js
│     ├─ file.js
│     ├─ object.js
│     ├─ performance.js
│     ├─ storage.js
│     ├─ string.js
│     ├─ time.js
│     └─ url.js
├─ LICENSE
├─ links
│  └─ 常用网址.md
├─ README.md
├─ Vue
│  ├─ electron + Vue 使用总结.md
│  ├─ MVVM&&Vue基本代码.md
│  ├─ vue+threejs web可视化搭建.md
│  ├─ Vue3+TS项目初始化配置.md
│  ├─ Vue3笔记.md
│  ├─ Vue3项目工程化-Vite配置.md
│  └─ VueRouter.md
├─ 流程图
│  ├─ HashMap底层源码扩容流程图.drawio
│  ├─ map-hashMap$Node关系图.drawio
│  ├─ Servlet 类的继承体系.drawio
│  ├─ Spring5模块.drawio
│  ├─ 双向链表.iodraw
│  └─ 网站架构图源文件.drawio.iodraw
├─ 设计
│  └─ 后台管理 UI 规范.md
└─ 项目
   └─ web程序性能优化.md
```
