## 创建动态 Web 工程

### JSP 空项目创建

1. 添加 File -> Project Structure... -> Artifacts 添加
   ![image-20220528113200177](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528113200177.png)
2. 创建一个普通的 java 项目
   ![image-20220528113735372](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528113735372.png)
3. 选中项目 根目录也就是创建的 java 空项目，点击右键选择 Add Framework Support...
   ![image-20220528114057808](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528114057808.png)
4. 添加 tomcat 服务到项目 ，新建 Tomcat Server -> Loacl 如下图
   ![image-20220528114407269](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528114407269.png)
5. 配置项目 tomcat 启动端口，项目更新内容和 dev 项目默认路径
   ![image-20220528114707353](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528114707353.png)
   ![image-20220528114807801](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528114807801.png)

6. 启动启动项目，启动后页面在浏览器显示说明配置完成

### 项目目录介绍

![image-20220528123115702](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528123115702.png)

## Servlet

### Servlet 类介绍和使用

1. Servlet 是 JavaEE 规范之一，规范就是接口
2. Servlet 是 JavaWeb 三大组件之一，三大组件分别是：Servlet 程序、Filter 过滤器、Listener 监听器
3. Servlet 是运行在服务器上的一个 Java 小程序。**它可以接收客户端发送过来的请求，并相应数据给客户端**

#### 手动实现 Servlet 程序

1. 下载[Servlet jar 包](https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api/3.1.0)添加到项目项目依赖中
2. 编写一个类去实现 Servlet 接口

```java
package com.servlet.study;
import javax.servlet.*;
import java.io.IOException;

public class HelloServlet implements  Servlet {}
```

3. 实现 service 方法，处理请求，并相应数据

```java
package com.servlet.study;
import javax.servlet.*;
import java.io.IOException;

public class HelloServlet implements  Servlet {

  @Override
  public void init(ServletConfig servletConfig) throws ServletException {

  }

  @Override
  public ServletConfig getServletConfig() {
    return null;
  }

  /**
   * @desc service 方法专门是用来处理请求和相应的
   * @param servletRequest
   * @param servletResponse
   * @throws ServletException
   * @throws IOException
   */
  @Override
  public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    System.out.println("service");
  }

  @Override
  public String getServletInfo() {
    return null;
  }

  @Override
  public void destroy() {

  }
}
```

4. 到 web.xml 中去配置 Servlet 程序的访问地址

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--servlet 标签给Tomcat配置 Servlet 程序-->
    <servlet>
        <!--servlet-name 标签是给 Servlet 程序起一个别名（一般是类名）-->
        <servlet-name>HelloServlet</servlet-name>
        <!--servlet-class 是 Servlet 程序的全类名-->
        <servlet-class>com.servlet.study.HelloServlet</servlet-class>
    </servlet>
    <!--servlet-mapping 标签给 servlet 程序配置访问地址-->
    <servlet-mapping>
        <!--servlet-name 标签的作用是告诉服务器，我当前配置的地址给哪个 servlet 程序使用-->
        <servlet-name>HelloServlet</servlet-name>
        <!--url-pattern 标签配置访问地址-->
        <!--/ 斜杠在服务器解析的时候表示地址为：http://ip:port/工程路径 -->
        <!--/hello 表示地址为：http://ip:port/工程路径/hello -->
        <url-pattern>/hello</url-pattern>
    </servlet-mapping>
</web-app>
```

5. 重启项目到浏览器中访问 http://localhost:8085/index/hello 可以看到控制台打印 servlet 实现的 service 接口
   ![image-20220528141154941](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528141154941.png)

#### Servlet 常见错误

1. `Caused by: java.lang.IllegalArgumentException: servlet映射中的<url pattern>[hello]无效` url-pattern 配置路径没有以斜杠（/）开头
2. `Caused by: java.lang.IllegalArgumentException: Servlet映射指定未知的Servlet名称[1HelloServlet]` servlet-mapping 标签下的 servlet-name 标签设置类名与 servlet 标签下的 servlet-name 标签设置类名不一致

#### Servlet-url 地址如何定位到 Servlet 程序进行访问

![image-20220528142927595](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528142927595.png)

#### Servlet 生命周期

第 1、2 步是在第一次访问，的时候创建 Servlet 程序会调用。第 3 步每次访问都会调用，第 4 步在 web 工程服务停止时执行

1. 执行 Servlet 构造器
2. 执行 init 初始化方法
3. 执行 service 方法
4. 执行 destroy 销毁方法

```java
package com.servlet.study;
import javax.servlet.*;
import java.io.IOException;

public class HelloServlet implements  Servlet {

  public HelloServlet() {
    System.out.println("1 === 构造方法");
  }

  @Override
  public void init(ServletConfig servletConfig) throws ServletException {
    System.out.println("2 === 初始化 init 构造方法");
  }

  /**
   * @desc service 方法专门是用来处理请求和相应的
   * @param servletRequest
   * @param servletResponse
   * @throws ServletException
   * @throws IOException
   */
  @Override
  public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    System.out.println("3 === service");
  }

  @Override
  public void destroy() {
    System.out.println("4 === destroy销毁方法");
  }
}
```

#### Servlet 请求的分发处理

```java

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class HelloServlet implements  Servlet {

  /**
   * @desc service 方法专门是用来处理请求和相应的
   * @param servletRequest
   * @param servletResponse
   * @throws ServletException
   * @throws IOException
   */
  @Override
  public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    //类型转换（因为它有getMethod() 方法 ）
    HttpServletRequest HttpServletRequest = (HttpServletRequest) servletRequest;
    //获取请求的类型
    String method = HttpServletRequest.getMethod();
    if ("GET".equals(method)) {
      getHelloMethod();
    } else {
      postHelloMethod();
    }
  }

  /**
   * GET 方法
   * @return
   */
  public void getHelloMethod() {

  }
  /**
   * POST 方法
   * @return
   */
  public void postHelloMethod() {

  }
}
```

#### 通过继承 HttpServlet 实现 Servlet 程序

一般在实际项目开发中，都是使用继承 HttpServlet 类的方式去实现 Servlet 程序

1. 编写一个类去继承 HttpServlet 类
2. 根据业务重写 doGet、doPost、doPut、或 doDelete 方法

```java
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zmm
 * @version 1.0
 * @date 2022/5/28 15:07
 */
public class ServletHttpDemo extends HttpServlet {
  /**
   * @desc 重写 doGet 方法
   * @param req
   * @param resp
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("doGet 方法");
  }

  /**
   * @desc 重写 doPost 方法
   * @param req
   * @param resp
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("doPost 方法");
  }

}
```

3. 到 web.xml 中配置 Servlet 程序的访问地址

```xml
<servlet>
   <servlet-name>ServletHttpDemo</servlet-name>
   <servlet-class>com.servlet.study.ServletHttpDemo</servlet-class>
</servlet>
<servlet-mapping>
   <servlet-name>ServletHttpDemo</servlet-name>
   <url-pattern>/servletHttpDemo</url-pattern>
</servlet-mapping>
```

#### 整个 Servlet 类的继承体系

![image-20220528165444749](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528165444749.png)

### ServletConfig 类

ServletConfig 类从类名上看，就知道是 Servlet 程序的配置信息类
Servlet 程序和 ServletConfig 对象都是由 Tomcat 负责创建，我们负责使用
Servlet 程序默认是第一次访问的时候创建，ServletConfig 是每个 Servlet 程序创建时就会创建一个对应的 ServletConfig 对象

#### ServletConfig 类的三大作用

1. 可以获取 Servlet 程序别名 servlet-name 的值
2. 获取初始化参数 init-param
3. 获取 ServletContext 对象

```java
public class HelloServlet implements  Servlet {

  @Override
  public void init(ServletConfig servletConfig) throws ServletException {
      System.out.println("2 === 初始化 init 构造方法");

      // 1. 可以获取 Servlet 程序别名 servlet-name 的值
      System.out.println("HelloServlet程序的别名是：" + servletConfig.getServletName());
      // 2. 获取初始化参数 init-param
      System.out.println("初始化参数username的值是：" + servletConfig.getInitParameter("username"));
      System.out.println("初始化参数url的值是：" + servletConfig.getInitParameter("url"));
      // 3. 获取 ServletContext 对象
      System.out.println(servletConfig.getServletContext());


  }
}
```

```xml
<!--servlet 标签给Tomcat配置 Servlet 程序-->
<servlet>
      <!--servlet-name 标签是给 Servlet 程序起一个别名（一般是类名）-->
      <servlet-name>HelloServlet</servlet-name>
      <!--servlet-class 是 Servlet 程序的全类名-->
      <servlet-class>com.servlet.study.HelloServlet</servlet-class>
      <!--init-param是初始化参数-->
      <init-param>
         <!--是参数名-->
         <param-name>username</param-name>
         <!--是参数值-->
         <param-value>root</param-value>
      </init-param>
      <init-param>
         <!--是参数名-->
         <param-name>url</param-name>
         <!--是参数值-->
         <param-value>jdbc:mysql://localhost:3006/test</param-value>
      </init-param>
</servlet>
```

### ServletContext 类

#### ServletContext 是什么

1. ServletContext 是一个接口，表示 Servlet 上下文
2. 一个 Web 工程，只有一个 ServletContext 对象实例
3. ServletContext 对象是一个域对象（域对象是可以像 Map 一样存取数据的对象，叫做域对象，这里的域指的是存取数据的操作范围）
   | | 存数据 | 取数据 | 删除数据 |
   | ------ | -------------- | -------------- | ----------------- |
   | Map | put() | get() | remove() |
   | 域对象 | setAttribute() | getAttribute() | removeAttribute() |

#### ServletContext 类的四个作用

1. 获取 web.xml 中配置的上下文参数 context-param
2. 获取当前的工程路径，格式：/工程路径
3. 获取工程部署后在服务器硬盘上的绝对路径
4. 像 Map 一样存取数据

## HTTP
