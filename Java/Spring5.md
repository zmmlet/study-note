# Spring 5

使用 [Spring 版本 5.2.6](https://repo.spring.io/ui/native/libs-release/org/springframework/spring/5.2.6.RELEASE/)

# Spring 项目创建

1. 创建新项目，选择 JDK1.8 版本

![image-20220527101035348](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527101035348.png)

2. 勾选项目模板

![image-20220527101159466](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527101159466.png)

3. 填写项目名，选择存储路径，点击完成，即可创建项目

![image-20220527101317210](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527101317210.png)

4. 创建成功

![image-20220527102056939](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527102056939.png)

5. 导入 Spring 相关 jar 包

   ![image-20220528162202471](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220528162202471.png)

6. 在项目根目录创建 lib 文件夹，放入日志 jar 包和 spring jar 包

   ![image-20220527103627074](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527103627074.png)

7. 选择 File->Project Structure 添加 jar 包到项目点击应用

   ![image-20220527104041457](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527104041457.png)

8. 使用 Spring 方式创建对象

   8.1 普通方式创建

   ```java
   // 普通方式创建类
   public class User {
     public static void main(String[] args) {

     }

     /**
      * 普通方式创建方法
      */
     public void add() {
       System.out.println("add...");
     }
   }
   ```

   8.2 创建 Spring 配置文件，在配置文件配置创建的对象

   - Spring 配置文件格式使用 xml 文件 选择 src 文件夹，右键选择 new ->XML Configuration File -> Spring Cinfig 创建 Spring 配置 xml 文件

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
       <!--配置User对象创建-->
       <bean id="User" class="com.company.User"></bean>
   </beans>
   ```

   8.3 进行测试代码编写

   ```java
   @Test
   public void testAddUser() {
      //1.加载spring配置文件
      ApplicationContext context = new ClassPathXmlApplicationContext("spring-options.xml");
      //2.获取配置创建的对象
      User user = context.getBean("User", User.class);
      System.out.println(user);
      user.add();
   }
   ```

   输出结果
   com.company.User@59906517
   add...

## Spring 概念

1. Spring 是 Java EE 编程领域的一个轻量级开源框架
2. Spring 可以解决企业应用开发的复杂性
3. Spring 有两个核心部分：IOC 和 Aop

   3.1 IOC：控制反转，把创建对象过程交给 Spring 进行管理

   3.2 Aop：面向切面，不修改源代码，进行增强

4. 特点

   4.1 方便解耦，简化开发

   4.2 Aop 编程支持

   4.3 方便程序测试

   4.4 方便进行事务操作

   4.5 降低 API 开发难度

# IOC 容器

## 一、IOC 概念

1. 控制反转：把对象的创建和对象之间的调用过程，从程序员手里转交给 Spring 进行管理
2. 使用 IOC 的目的：为了降低耦合度
3. 入门案例就是 IOC 实现(8.使用 Spring 方式创建对象)

## 二、IOC 底层原理

IOC 目的：耦合度降到最低

1. xml 解析、工厂模式、反射

   1.1 原始模式

   ```java
   class UserServive{
      execute() {
         UserDao dao = new UserDao()
         dao.add()
      }
   }
   class UserDao{
      add() {
         //...
      }
   }
   ```

   1.2 工厂模式（耦合度降低）

   ```java
   class UserServive{
      execute() {
         UserDao dao = UserFactory.getDao()
         dao.add()
      }
   }
   class UserDao{
      add() {
         //...
      }
   }
   // 创建工厂
   class UserFactory {
      public static UserDao getDao() {
         return new new UserDao();
      }
   }
   ```

2. IOC 过程（耦合度进一步降低）
   2.1 xml 配置文件，配置创建的对象

   ```xml
   <bean id="dao" class="com.atguigu.UserDao"></bean>
   ```

   2.2 有 service 类 和 dao 类，创建工厂类

   ```java
   class UserFactory {
      pubilc static UserDao getDao() {
         String classValue = class属性值; // a、xml 解析
         // b、通过反射创建对象
         Class clazz = Class.forName(classValue);
         retrun (UserDao)clazz.newInstance();
      }
   }
   ```

## 三、IOC 接口（BeanFactory）

1. IOC 思想，基于 IOC 容器完成，IOC 容器底层就是对象工程
2. Spring 提供 IOC 容器实现的两种方式（两个接口）：

   2.1. BeanFactory：IOC 容器基本实现，是 Spring 内部使用接口，不提供开发人员使用
   加载配置文件的时候不会创建对象，在获取对象（使用）才会创建对象

   2.2. ApplicationContext：BeanFactory 接口的子接口，提供更多接口，一般由开发人员使用
   加载配置文件的时候就会在配置文件对象进行创建

## 四、IOC 操作 Bean 管理

### 什么是 Bean 管理

Bean 管理的两个操作：1、Spring 创建对象 2、Spring 注入属性

### Bean 管理两种操作方式

1. 基于 XML 配置文件方式事项
2. 基于注解方式实现

### IOC 操作 Bean 管理（基于 XML）

#### 基于 xml 方式创建对象

```xml
<!--配置User对象创建-->
<bean id="User" class="com.company.User"></bean>
```

1. 在 spring 配置文件中，使用 bean 标签，标签里面添加对应属性，就可以实现对象创建
2. 在 bean 标签由很多属性，介绍常用属性
   - id 属性：唯一标识
   - class 属性：类全路径（包类路径）
   - name 属性：和 id 属性类似，但是 name 属性可以设置特殊符号，id 属性不能设置特殊符号
3. 创建对象的时候，默认也是执行无参构造方法（重写默认无参构造方法后报错如下：）

org.springframework.context.support.AbstractApplicationContext refresh
警告: Exception encountered during context initialization - cancelling refresh attempt: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'User' defined in class path resource [spring-options.xml]: Instantiation of bean failed;

```
public class User {
    private String userName;
    // 快捷键：Alt+insert
    public User(String userName) {
        this.userName = userName;
    }

    /**
     * 普通方式创建方法
     */
    public void add() {
        System.out.println("add...");
    }
}
```

#### 基于 xml 方式注入属性

常规方法

```java
package com.company;

public class Book {
    private String bookName;
    /**
     * Set方法注入
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    /**
     * 有参数构造注入
     */
    public Book(String bookName) {
        this.bookName = bookName;
    }
    /**
     * 原始方式，设置图书名称
     */
    public static void main(String[] args){
        Book book = new Book();
        book.setBookName("abc");
    }
}
```

1. DI：依赖注入，就是注入属性
2. 第一种方式：使用 Set 方法注入

   - 创建类，定义属性和对应的 set 方法

   ```java
   /**
   * 演示使用set方法进行属性注入
   */
   public class Book {
   //创建属性
      private String bookName;
      private String bauthor;
   //创建属性对应的set方法
      public void setBookName(String bookName) {
         this.bookName = bookName;
      }

      public void setBauthor(String bauthor) {
         this.bauthor = bauthor;
      }
   }
   ```

   - 在 Spring 配置文件配置对象创建，配置属性注入

   ```xml
   <!--set方法注入属性-->
    <bean id="book" class="com.company.Book">
        <!--使用 property 完成属性注入-->
        <property name="bookName" value="红楼梦"></property>
        <property name="bauthor" value="曹雪芹"></property>
    </bean>
   ```

   - 测试 Set 方法注入

   ```java
   public class BookTest {

    @Test
    public void testBook() {
        //1.加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-options.xml");
        //2.获取配置创建的对象(参数1：xml设置id,参数2：对应类名)
        Book book = context.getBean("book", Book.class);
        System.out.println(book);

        book.testBookDemo();

    }
   }

   ```

3. 第二种方式：有参构造注入属性

   - 创建类，定义属性和别写有参构造函数

   ```java
   public class Orders {
      //定义属性
      private String orderCode;
      private String orderName;
      private String address;
      //有参构造
      public Orders(String orderCode, String orderName, String address) {
         this.orderCode = orderCode;
         this.orderName = orderName;
         this.address = address;
      }
   }
   ```

   - 在 Spring 配置文件中进行配置

   ```xml
   <!--有参构造注入属性-->
    <bean id="orders" class="com.company.Orders">
        <constructor-arg index="0" name="orderCode" value="工单号"/>
        <constructor-arg index="1" name="orderName" value="工单名称"/>
        <constructor-arg index="2" name="address" value="详细地址"/>
    </bean>
   ```

   - 编写测试用例

   ```java
   @Test
   public void testOrders() {
      //1.加载spring配置文件
      ApplicationContext context = new ClassPathXmlApplicationContext("spring-options.xml");
      //2.获取配置创建的对象
      Orders orders = context.getBean("orders", Orders.class);
      System.out.println(orders);
   }
   ```

4. p 名称空间注入
   - 使用 p 名称空间注入，可以简化基于 xml 配置方式
   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:p="http://www.springframework.org/schema/p"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
   ```
   - 进行属性注入，在 bean 标签进行注入
   ```xml
   <!--使用p命名空间进行注入-->
    <bean id="book" class="com.company.Book" p:bookName="红楼梦" p:bauthor="曹雪芹">
    </bean>
   ```

#### 基于 xml 方式注入其他类型属性

##### 字面量

1. null 值

```xml
<property name="address" >
   <null/>
</property>
```

2. 属性值包含特殊符号

```xml
   <!--属性值中包含特殊属性
   1. 把<>进行转义 &lt; &gt;
   2. 把带特殊符号内容写到 CDATA
   -->
   <property name="address">
      <value><![CDATA[<<西游记>>]]></value>
   </property>
```

##### 注入属性-外部 bean

1. 创建两个类 service 类和 dao 类

```java
public interface UserDao {
   public void update();
}

```

```java
public class UserDaoImpl implements UserDao {

    @Override
    public void update() {
        System.out.println("dao update");
    }
}
```

2. 在 service 调用 dao 里面的方法

```java
public class UserService {
    /**
     * 创建UserDao 类型属性，生成 set方法
     * 属性：可以是方法、类、字符串等
     */
    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void add() {
        System.out.println("service add..........");
        /*
         * 原始方式：创建 UserDao 对象
         * */
//        UserDao user_dao = new UserDaoImpl();
//        user_dao.update();
    }
}
```

3. 在 Spring 配置文件中进行配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--1. service 和 dao 对象创建-->
    <bean id="userService" class="com.service.UserService">
        <!--注入 userDao 对象
            name属性值：类里面属性名称
            ref属性：创建 userDao 对象 bean 标签 id值
        -->
        <property name="userDao" ref="userDaoImpl"></property>
    </bean>
    <bean id="userDaoImpl" class="com.dao.UserDaoImpl"></bean>
</beans>
```

4. 编写测试用例

```java
@Test
public void testAddUser() {
   //1.加载spring配置文件
   ApplicationContext context = new ClassPathXmlApplicationContext("external-bean.xml");
   //2.获取配置创建的对象
   UserService userService = context.getBean("userService", UserService.class);
   System.out.println(userService);

   userService.add();
}
```

##### 注入属性-内部 bean

1. 一对多关系：部门和员工（一个部门有多个员工，一个员工属于一个部门，部门是一，员工是多）
2. 在实体类之间表示一对多的关系

```java
/**
 * 创建部门类
 */
public class Dept {
    private String name;

    public void setName(String name) {
        this.name = name;
    }
}

/**
 * 员工类
 */
public class Emp {
    private String name;
    private String gemder;

    /*员工属于某一个部门使用对象表示*/
    private Dept dept;

    public void setName(String name) {
        this.name = name;
    }

    public void setGemder(String gemder) {
        this.gemder = gemder;
    }

    public void setDept(Dept dept) {
    }
}
```

3. 通过 Spring 的 xml 配置文件进行配置

```xml
<!--内部bean-->
<bean id="emp" class="com.bean.Emp">
   <!--先设置普通属性-->
   <property name="name" value="张三"></property>
   <property name="gemder" value="男"></property>
   <!--设置对象类型属性-->
   <property name="dept">
      <bean id="dept" class="com.bean.Dept">
         <property name="name" value="研发部"></property>
      </bean>
   </property>
</bean>
```

4. 编写测试用例

```java
@Test
public void testInsideBean() {
   //1.加载spring配置文件
   ApplicationContext context = new ClassPathXmlApplicationContext("inside-bean.xml");
   //2.获取配置创建的对象
   Emp emp = context.getBean("emp", Emp.class);

   System.out.println(emp);

   emp.add();
   emp.toString();
}
```

##### 注入属性-级联赋值

1. 创建类

```java
/**
 * 创建部门类
 */
public class Dept {
    private String name;

    public void setName(String name) {
        this.name = name;
    }
}

/**
 * 员工类
 */
public class Emp {
    private String name;
    private String gemder;

    /*员工属于某一个部门使用对象表示*/
    private Dept dept;

    public void setName(String name) {
        this.name = name;
    }

    public void setGemder(String gemder) {
        this.gemder = gemder;
    }

    public void setDept(Dept dept) {
    }
}
```

2. xml 级联配置

   - 第一种写法

     ```xml
     <!--级联赋值-->
     <bean id="emp" class="com.bean.Emp">
        <!--先设置普通属性-->
        <property name="name" value="张三"></property>
        <property name="gemder" value="男"></property>
        <!--级联赋值-->
        <property name="dept" ref="dept"></property>
     </bean>
     <bean id="dept" class="com.bean.Dept">
        <property name="name" value="研发部"></property>
     </bean>
     ```

   - 第二种写法（dept.name 需要生成 dept 对应的 get 方法）

     ```xml
     <!--级联赋值-->
      <bean id="emp" class="com.bean.Emp">
         <!--先设置普通属性-->
         <property name="name" value="张三"></property>
         <property name="gemder" value="男"></property>
         <!--级联赋值-->
         <property name="dept" ref="dept"></property>
         <property name="dept.name" value="开发部"></property>
      </bean>
      <bean id="dept" class="com.bean.Dept">
         <property name="name" value="研发部"></property>
      </bean>
     ```

3. 编写测试用例

```java
@Test
public void testCascade() {
   //1.加载spring配置文件
   ApplicationContext context = new ClassPathXmlApplicationContext("cascade.xml");
   //2.获取配置创建的对象
   Emp emp = context.getBean("emp", Emp.class);

   System.out.println(emp);

   emp.add();
   emp.toString();
}
```

#### IOC 操作 Bean 管理（xml 注入集合属性）

1. 注入数组类型属性
2. 注入 List 集合类型属性
3. 注入 Map 集合类型属性

   - 创建类，定义数组、list map set 类型属性，并生成对应的 set 方法

   ```java
   public class Stu {
    //1.数组类型属性
    private String[] courses;

    //2.list 集合类型属性
    private List<String> list;

    //3.map集合类型属性
    private Map<String, String> maps;

    //set集合类型属性
    private Set<String> sets;

    public void setCourses(String[] courses) {
        this.courses = courses;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public void setMaps(Map<String, String> maps) {
        this.maps = maps;
    }

    public void setSets(Set<String> sets) {
        this.sets = sets;
    }

    public void test() {
        System.out.println(Arrays.toString(courses));
        System.out.println(sets);
        System.out.println(maps);
        System.out.println(list);
    }
   }
   ```

   - 在 Spring xml 配置文件中进行配置

   ```xml
   <!--集合类型数据配置-->
    <bean id="stu2" class="com.aggregatearr.Stu">
        <!--数组类型属性注入-->
        <property name="courses">
            <array>
                <value>java</value>
                <value>php</value>
            </array>
        </property>
        <!--list类型属性注入-->
        <property name="list">
            <list>
                <value>javaScript</value>
                <value>数据结构算法</value>
            </list>
        </property>
        <!--map类型属性注入-->
        <property name="maps">
            <map>
                <entry key="c" value="C"></entry>
                <entry key="c++" value="C++"></entry>
                <entry key="java" value="JAVA"></entry>
            </map>
        </property>
        <!--set类型属性注入-->
        <property name="sets">
            <set>
                <value>MySql</value>
                <value>Reads</value>
            </set>
        </property>
    </bean>
   ```

   - 编写测试用例

   ```java
   @Test
    public void testStu() {
        //1.加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("aggregate-list.xml");
        //2.获取配置创建的对象(参数1：xml设置id,参数2：对应类名)
        Stu stu = context.getBean("stu2", Stu.class);
        stu.test();
    }
   ```

4. 在集合里面设置对象类型值

```xml
 <!--集合类型数据配置-->
<bean id="stu2" class="com.aggregatearr.Stu">
   <!--2.注入list集合类型，值是对象-->
   <property name="couresList">
      <list>
            <ref bean="course1"></ref>
            <ref bean="course2"></ref>
      </list>
   </property>
</bean>

<!--1.创建多个 courses 对象-->
<bean id="course1" class="com.aggregatearr.Coures">
   <property name="cname" value="JAVA"></property>
</bean>
<bean id="course2" class="com.aggregatearr.Coures">
   <property name="cname" value="Spring5"></property>
</bean>
```

5. 把集合注入部分提取出来

   - 在 Spring 配置文件中引入命名空间 util

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:util="http://www.springframework.org/schema/util"
         xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                              http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd">

   </beans>
   ```

   - 使用 util 标签完成 list 集合注入提取

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:util="http://www.springframework.org/schema/util"
         xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                              http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd">
      <!--1.提取list集合类型属性配置-->
      <util:list id="booksList">
         <value>Spring</value>
         <value>Java</value>
         <value>MySQL</value>
      </util:list>
      <!--2.提取list集合类型属性注入使用-->
      <bean id="books" class="com.aggregatearr.Books">
         <property name="list" ref="booksList"></property>
      </bean>
   </beans>
   ```

### IOC 操作 Bean 管理（FactoryBean）

Spring 有两种类型的 bean 一种普通 bean，一种工厂 bean（FactoryBean）

#### 普通 Bean:定义什么类型就是返回类型

1. 创建两个类 service 类和 dao 类

```java
public interface UserDao {
   public void update();
}

```

```java
public class UserDaoImpl implements UserDao {

    @Override
    public void update() {
        System.out.println("dao update");
    }
}
```

2. 在 service 调用 dao 里面的方法

```java
public class UserService {
    /**
     * 创建UserDao 类型属性，生成 set方法
     * 属性：可以是方法、类、字符串等
     */
    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void add() {
        System.out.println("service add..........");
        /*
         * 原始方式：创建 UserDao 对象
         * */
//        UserDao user_dao = new UserDaoImpl();
//        user_dao.update();
    }
}
```

3. 在 Spring 配置文件中进行配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--1. service 和 dao 对象创建-->
    <bean id="userService" class="com.service.UserService">
        <!--注入 userDao 对象
            name属性值：类里面属性名称
            ref属性：创建 userDao 对象 bean 标签 id值
        -->
        <property name="userDao" ref="userDaoImpl"></property>
    </bean>
    <bean id="userDaoImpl" class="com.dao.UserDaoImpl"></bean>
</beans>
```

4. 编写测试用例

```java
@Test
public void testAddUser() {
   //1.加载spring配置文件
   ApplicationContext context = new ClassPathXmlApplicationContext("external-bean.xml");
   //2.获取配置创建的对象
   UserService userService = context.getBean("userService", UserService.class);
   System.out.println(userService);

   userService.add();
}
```

#### 工厂 Bean （FactoryBean）：在配置文件定义 bean 类型，可以和返回类型不一致

第一步：创建类，让这个类作为工厂 bean 实现接口 FactoryBean

第二步：实现接口里面的方法，在实现方法中定义返回 bean 类型

```java
public class MyBean implements FactoryBean<Coures> {
    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }

    /**
     * 定义返回bean
     */
    @Override
    public Coures getObject() throws Exception {
        Coures coures = new Coures();
        coures.setCname("张三");
        return coures;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }
}
```

```java
@Test
public void TestMyBean() {
   ApplicationContext context = new ClassPathXmlApplicationContext("mybean.xml");
   Coures coures = context.getBean("mybean", Coures.class);
   System.out.println(coures);
}
```

### IOC 操作 Bean 管理（bean 作用域）

在 Spring 里面设置创建 bean 实例是单实例还是多实例

1. 在 Spring 里面，默认情况下，bean 是单实例对象

```java
@Test
public void testBooks() {
   //1.加载spring配置文件
   ApplicationContext context = new ClassPathXmlApplicationContext("aggregate-list2.xml");
   //2.获取配置创建的对象(参数1：xml设置id,参数2：对应类名)
   Books books = context.getBean("books", Books.class);
   Books books1 = context.getBean("books", Books.class);
   //默认单实例对象，两次输出地址一样
   System.out.println(books);
   System.out.println(books1);
   books.test();
}
// 输出结果
// com.aggregatearr.Books@3b94d659
// com.aggregatearr.Books@3b94d659
```

2. 如何设置单实例还是多实例
   - 在 Spring 配置文件 bean 标签里面由属性（scope）用于设置单实例还是多实例
   - scope 属性值：a、默认值 singleton 表示单实例对象 b、prototype 表示多实例对象
   ```xml
   <bean id="books" class="com.aggregatearr.Books" scope="prototype">
      <property name="list" ref="booksList"></property>
    </bean>
   ```
   ```java
   @Test
   public void testBooks() {
      //1.加载spring配置文件
      ApplicationContext context = new ClassPathXmlApplicationContext("aggregate-list2.xml");
      //2.获取配置创建的对象(参数1：xml设置id,参数2：对应类名)
      Books books = context.getBean("books", Books.class);
      Books books1 = context.getBean("books", Books.class);
      //默认单实例对象，两次输出地址一样
      System.out.println(books);
      System.out.println(books1);
      books.test();
   }
   // 输出结果
   // com.aggregatearr.Books@4f2b503c
   // com.aggregatearr.Books@bae7dc0
   ```
   - singleton 和 prototype 区别
     - singleton 单实例，prototype 多实例
     - 设置 scope 值是 singleton 时候，加载 spring 配置文件的时候就会创建单实例对象
     - 设置 scope 值是 prototype 时候，不是在加载 spring 配置文件的时候创建对象，在调用 getBean 方法的时候创建多实例对象

### IOC 操作 Bean 管理（bean 生命周期）

生命周期：从对象创建到对象销毁的过程

#### bean 生命周期

1. 通过构造器创建 bean 实例（无参构造）
2. 为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）
3. 调用 bean 的初始化的方法（需要进行配置）
4. bean 可以使用了（对象获取到了）
5. 当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）

##### 生命周期演示代码

1. 创建类

```java
public class LifeCycleOrders {
   //无参构造
   public LifeCycleOrders() {
      System.out.println("1. 通过构造器创建 bean 实例（无参构造）");
   }

   private String oname;

   public void setOname(String oname) {
      this.oname = oname;
      System.out.println("2. 为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）");
   }

   //创建执行初始化方法
   public void initMethod() {
      System.out.println("3. 调用 bean 的初始化的方法（需要进行配置）");
   }

   // 创建执行的销毁方法
   public void destroyMethod() {
      System.out.println("5. 当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）");
   }
}
```

2. 在 Spring xml 配置文件中配置

```xml
<bean id="lifeCycleOrders" class="com.lifecycle.LifeCycleOrders" init-method="initMethod" destroy-method="destroyMethod">
   <property name="oname" value="张三"></property>
</bean>
```

3. 编写测试类

```java
@Test
public void TestLifeCycleOrders() {
ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("life-cycle-orders.xml");
   //        ApplicationContext context = new ClassPathXmlApplicationContext("life-cycle-orders.xml");
   //获取配置创建的对象(参数1：xml设置id,参数2：对应类名)
   LifeCycleOrders lco = context.getBean("lifeCycleOrders", LifeCycleOrders.class);
   System.out.println("4. bean 可以使用了（对象获取到了）");
   System.out.println(lco);

   // 手动让bean实例销毁:自动强制转换
   //        ((ClassPathXmlApplicationContext) context).close();
   context.close();
}
```

#### bean 的后置处理器（七步）

1. 通过构造器创建 bean 实例（无参构造）
2. 为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）
3. 把 bean 实例传递 bean 后置处理器的方法 postProcessBeforeInitialization
4. 调用 bean 的初始化的方法（需要进行配置）
5. 把 bean 实例传递 bean 后置处理器的方法 postProcessAfterInitialization
6. bean 可以使用了（对象获取到了）
7. 当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）

##### 演示添加后置处理器效果

1. 创建类，实现接口 BeanPostProcessor 创建后置处理器

```java
public class MyBeanPost implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("在初始化之前执行该方法");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("在初始化之后执行该方法");
        return bean;
    }
}
```

2. 在 Spring 配置文件中进行配置

```xml
<bean id="lifeCycleOrders" class="com.lifecycle.LifeCycleOrders" init-method="initMethod" destroy-method="destroyMethod">
   <property name="oname" value="张三"></property>
</bean>

<!--配置后置处理器：会将当前配置文件的所有配置都添加后置处理器-->
<bean id="myBeanPost" class="com.lifecycle.MyBeanPost"></bean>
```

### IOC 操作 Bean 管理（xml 自动装配）

自动装配：根据指定装配规则（属性名称或者属性类型）,Spring 自动将匹配的属性进行注入

#### 演示自动装配

1. 根据属性名称自动装配

```xml
<!--实现自动装配
    bean 标签属性 autowire 配置自动装配
    autowire 属性常用两个值：
        byName 根据属性名称注入，注入值 bean的id值和类属性名称一样
        byType 根据属性类型注入
    -->
    <bean id="emp" class="com.autowire.Emp" autowire="byName">
<!--        <property name="dept" ref="dept"></property>-->
    </bean>
    <bean id="dept" class="com.autowire.Dept"></bean>
```

2. 根据属性类型自动装配

```xml
<!--实现自动装配
    bean 标签属性 autowire 配置自动装配
    autowire 属性常用两个值：
        byName 根据属性名称注入，注入值 bean的id值和类属性名称一样
        byType 根据属性类型注入
    -->
    <bean id="emp" class="com.autowire.Emp" autowire="byType">
<!--        <property name="dept" ref="dept"></property>-->
    </bean>
    <bean id="dept" class="com.autowire.Dept"></bean>
```

### IOC 操作 Bean 管理（xml 外部属性文件）

1. 直接配置数据库信息
   - 配置德鲁伊连接池
   - 引入德鲁伊 jar 包依赖
   - 在配置文件中配置
   ```xml
   <!--配置直接连接池-->
   <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
      <!-- 直接配置 -->
      <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
      <property name="url" value="jdbc:mysql://localhost:3306/userDb"></property>
      <property name="username" value="root"></property>
      <property name="password" value="root"></property>
   </bean>
   ```
2. 引入外部属性文件配置数据库连接池

   - 创建外部属性文件，properties 格式文件（jdbc.properties），写数据库信息

   ```
   prop.driverClass=com.mysql.jdbc.Driver
   prop.url=jdbc:mysql://localhost:3306/userDb
   prop.userName=root
   prop.password=root
   ```

   - 把外部 properties 属性文件引入到 spring 配置文件中
     - 引入 context 命名空间

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
      <beans xmlns="http://www.springframework.org/schema/beans"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:context="http://www.springframework.org/schema/context"
      xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
   ```

   - 在 Spring 配置文件使用标签引入外部属性文件

   ```xml
   <!--引入外部属性文件-->
    <context:property-placeholder location="jdbc.properties"/>
    <!--配置连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="${prop.driverClass}"></property>
        <property name="url" value="${prop.url}"></property>
        <property name="username" value="${prop.userName}"></property>
        <property name="password" value="${prop.password}"></property>
    </bean>
   ```

### IOC 操作 Bean 管理（注解方式）

**注解：**

1. 注解是代码里特殊的标记，格式（@注解名称(属性名称=属性值，属性名称=属性值...)）
2. 使用注解，注解作用在类、方法、属性上面
3. 使用注解目的：简化 xml 配置

#### IOC 操作 Bean 管理（注解方式:创建对象）

1. @Component 普通对象
2. @Service 业务逻辑层
3. @Controller WEB 层
4. @Repository 持久层

**上面四个注解功能是一样的，都可以用来创建 bean 实例**

##### 基于注解方式实现对象创建

1. 引入 spring-aop-5.2.6.RELEASE.jar 包依赖到项目中
2. 开启组件扫描

```xml
<!--开启组件扫描
1. 扫描多个包，多个包使用逗号分割
2. 扫描多个包，写需要扫描多个包对应的上层包名
-->
<!--<context:component-scan base-package="com.annotation.dao,com.annotation.service"></context:component-scan>-->
<context:component-scan base-package="com.annotation"></context:component-scan>
```

3. 创建类，在类上面添加创建对象注解

```java
/**
 * 在注解里面 value 属性值可以省略不屑，默认值是类名称，首字母小写
 */
@Service(value = "userService")
public class UserService {
   public void add(){
      System.out.println("user-service-add...");
   }
}
```

4. 编写测试类

```java
@Test
public void TestUserService() {
   ApplicationContext context = new ClassPathXmlApplicationContext("annotation.xml");
   UserService user_service = context.getBean("userService", UserService.class);
   System.out.println(user_service);
   user_service.add();
}
```

#### IOC 操作 Bean 管理（注解方式:组件扫描配置）

1. 示例 1：扫描包含对应注解

```xml
<!--组件扫描配置：示例1
   use-default-filters="false" 表示现在不使用默认 filter,自己配置 filter
   context:include-filter 设置扫描那些内容（下面代码表示只扫描annotation包中，带有 @Controller 注解的方法或对象）
-->
<context:component-scan base-package="com.annotation" use-default-filters="false">
   <context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
</context:component-scan>
```

2. 示例 2：扫描不包含对应注解

```xml
<!--组件扫描配置：示例2
   下面配置扫描包所有内容
   context:exclude-filter 设置哪些类或方法不进行扫描（下面代码表示不扫描annotation包中，带有 @Component 注解的方法或对象）
-->
<context:component-scan base-package="com.annotation">
   <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Component"/>
</context:component-scan>
```

#### IOC 操作 Bean 管理（注解方式:注入属性）

1. @AutoWired 根据属性类型自动注入
2. @Qualifier 根据属性名称进行注入
3. @Resource 可以根据属性类型注入也可以根据属性名称进行注入
4. @Value 注入普通类型属性

##### @AutoWired 根据属性类型自动注入

1. 把 service 和 dao 对象创建，在 service 和 dao 类添加创建对象注解

```java
@Repository
public class UserDaoImpl implements UserDao {

    @Override
    public void add() {
        System.out.println("add dao....");
    }
}


/**
 * 在注解里面 value 属性值可以省略不屑，默认值是类名称，首字母小写
 */
@Service(value = "userService")
public class UserService {
    /**
     *定义dao类型属性，不需要添加 set 方法，添加注入属性的注解
     */
    @Autowired//根据类型进行注入
    private UserDao userDao;

    public void add(){
        System.out.println("user-service-add...");
        userDao.add();
    }
}
```

2. 在 service 中注入 dao 对象，在 service 类添加 dao 类型属性，在属性上面使用注解

```java
/**
 * 在注解里面 value 属性值可以省略不屑，默认值是类名称，首字母小写
 */
@Service(value = "userService")
public class UserService {
    /**
     *定义dao类型属性，不需要添加 set 方法，添加注入属性的注解
     */
    @Autowired
    private UserDao userDao;

    public void add(){
        System.out.println("user-service-add...");
        userDao.add();
    }
}
```

##### @Qualifier 根据属性名称进行注入

`@Qualifier` 注解使用要和 `@Autowired` 注解一起使用

```java

// UserDaoImpl
@Repository(value = "UserDaoImplOne")
public class UserDaoImpl implements UserDao {

    @Override
    public void add() {
        System.out.println("add dao....");
    }
}

// UserService
/**
 * 在注解里面 value 属性值可以省略不屑，默认值是类名称，首字母小写
 */
@Service(value = "userService")
public class UserService {
    /**
     *定义dao类型属性，不需要添加 set 方法，添加注入属性的注解
     */
    @Autowired //根据类型进行注入
    @Qualifier(value = "UserDaoImplOne") //根据名称进行注入
    private UserDao userDao;

    public void add(){
        System.out.println("user-service-add...");
        userDao.add();
    }
}
```

##### @Resource 可以根据属性类型注入也可以根据属性名称进行注入

```java

@Service
public class StudentService {
    //@Resource //根据类型进行注入
    @Resource(name ="UserDaoImplOne") // 根据名称进行注入
    private UserDao userDao;
    public void add() {
        System.out.println("student service...");
        userDao.add();
    }
}

```

##### @Value 注入普通类型属性

```java
@Service
public class StudentService {
    @Value(value = "张三") //注入普通类型
    private String name;

    //@Resource //根据类型进行注入
    @Resource(name ="UserDaoImplOne") // 根据名称进行注入
    private UserDao userDao;
    public void add() {
        System.out.println("student service...");
        userDao.add();
        System.out.println(name);
    }
}

```

#### IOC 操作 Bean 管理（注解方式：完全注解开发，不写 xml 文件）

1. 创建配置类，替代 xml 配置文件

```java
package com.annotation.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;

@Configurable // 作为配置类，替代xml配置文件
@ComponentScan(basePackages = {"com.annotation"})
public class SpringConfig {

}

```

2. 编写测试用例

```java
public class SpringConfigTest {
    @Test
    public void testConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        UserService userService = context.getBean("userService", UserService.class);
        System.out.println(userService);
        userService.add();
    }
}
```

# Aop

## Aop 概念

1. AOP 为 Aspect Oriented Programming 的缩写，意为：**面向切面编程**，利用 AOP 可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率。
2. 通俗描述：不通过修改源代码方式，在主干功能里面添加新的功能
3. 登录示例说明
   ![Snipaste_2022-09-27_18-01-09](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-09-27_18-01-09.png)

## Aop 底层原理

### Aop 底层使用动态代理

#### 有接口情况，使用 JDK 动态代理

1. 定义接口和接口实现类

```java
// 接口
interface UserDao {
   pubilc void login()
}
// 接口实现类
class UserDaoImpl IMPLEMENTS userDao {
   pubilc void login() {
      // 登录实现过程
   }
}
```

2. JDK 动态代理
   - 创建接口实现类代理对象，增强类的方法
     ![Snipaste_2022-09-27_18-16-27](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-09-27_18-16-27.png)

#### 没有接口情况，使用 CGLIB 动态代理

创建子类代理对象，增强类的方法
![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-09-27_20-14-20.png)

#### Aop （JDK 动态代理）代码演示

1. 使用 JDK 动态代理，使用 Proxy 类里面的方法创建代理对象，在`java.lang.reflect.Proxy`
   - 调用 newProxyInstance 方法，这个方法有三个参数，
     - 类加载器
     - 增强方法所在的类，这个类实现的接口，支持多个接口
     - 实现这个接口 InvocationHandler 创建代理对象，写增强的方法
2. 编写 JDK 动态代理代码

   - 创建接口，定义方法

   ```java
   public interface OrdersDao {
      public int add(int price, int number);

      public String upDate(String id);
   }

   ```

   - 创建接口实现类，实现方法

   ```java
   public class OrdersDaoImpl implements OrdersDao{
      @Override
      public int add(int price, int number) {
         System.out.println(price * number);
         return price;
      }
      @Override
      public String upDate(String id) {
         return id;
      }
   }
   ```

   - 使用 Proxy 类创建接口代理对象

   ```java
   import java.lang.reflect.InvocationHandler;
   import java.lang.reflect.Method;
   import java.lang.reflect.Proxy;
   import java.util.Arrays;

   public class JDKProxy {
      public static void main(String[] args) {
         // 创建接口实现类代理对象
         Class[] interfaces = {OrdersDao.class};
         // 匿名内部类实现方式
   //        Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
   //            @Override
   //            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
   //                return null;
   //            }
   //        });
         // 外部类实现方式
         OrdersDaoImpl orderDao = new OrdersDaoImpl();
         OrdersDao dao = (OrdersDao) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new OrderDaoProxy(orderDao));
         int result = dao.add(80, 120);
         System.out.println(result);
      }
   }
   // 创建代理对象代码
   class OrderDaoProxy implements InvocationHandler{
      //1 把创建的是谁的代理对象，把谁传递过来
      //有参构造传递
      private Object obj;
      public OrderDaoProxy(Object obj) {
         this.obj = obj;
      }

      // 增强的逻辑
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
         // 方法之前
         System.out.println("方法之前执行...." + method.getName() + "传递的参数...." + Arrays.toString(args));
         //被增强的方法执行
         Object result = method.invoke(obj, args);
         // 方法之后
         System.out.println("方法之后执行...." + obj);
         return result;
      }
   }
   ```

## Aop 术语

### 连接点

类里面哪些方法可以被增强，这些方法称为连接点

### 切入点

实际被真正增强的方法，称为切入点

### 通知（增强）

1. 实际增强的逻辑部分称为通知（增强）
2. 通知有多种类型
   - 前置通知：增强前执行
   - 后置通知：增强后执行
   - 环绕通知：增强前后都执行
   - 异常通知：增强方法异常执行
   - 最终通知：finally 增强方法无论，异常还是正常执行都会执行

### 切面

# JdbcTemplate

# 事务管理

# Spring 5 新特性

# 相关资源

- Spring 官网 https://spring.io/projects/spring-framework#learn

- Spring 相关包 https://repo.spring.io/ui/repos/tree/General/

- Spring 历史版本下载 https://repo.spring.io/ui/native/libs-release/org/springframework/spring/
- 日志包 https://commons.apache.org/proper/commons-logging/download_logging.cgi
- 选择下载文件

  ![image-20220527094342675](https://gitee.com/zmmlet/study-note/raw/master/images/image-20220527094342675.png)

  Spring 5 模块

  ![](https://gitee.com/zmmlet/study-note/raw/master/images/Spring5模块.png)
