//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
//
//// 一、阅读图像、视频和网络摄像头
//// 1.加载图形
//
//void main() {
//	string path = "Resources/test.png";
//	// Mat 基于 opencv 引用的矩阵数据类型
//	Mat img = imread(path);
//
//	
//	// imshow 用于显示图像
//	imshow("Image", img);
//	// 由于显示图像后会立刻关闭，所以waitKey(0) 表示无穷大 这样打开了不会关闭
//	waitKey(0);
//};



// 2.加载视频:视频播放完成后，会出现一个错误，错误原因是没有可播放的图片了

//void main() {
//	string path = "Resources/test_video.mp4";
//	// 视频捕获
//	VideoCapture cap(path);
//	Mat img;
//	// 由于视频由一系列的图片组成，所以使用循环
//	while (true) {
//		// 视频每一帧图片的渲染
//		cap.read(img);
//		// imshow 用于显示图像
//		imshow("Image", img);
//		// 由于显示图像后会立刻关闭，所以waitKey(0) 表示无穷大 这样打开了不会关闭
//		// waitKey() 数值越大播放越慢
//		waitKey(20);
//	}
//
//};


// 3.加载网络摄像头

//void mainVoid() {
//	// cap(1) 标识摄像头id
//	VideoCapture cap(0);
//	Mat img;
	// 由于视频由一系列的图片组成，所以使用循环
//	while (true) {
//		// 视频每一帧图片的渲染
//		cap.read(img);
//		// imshow 用于显示图像
//		imshow("Image", img);
//		// waitKey() 数值越大播放越慢
//		waitKey(1);
//	}
//};