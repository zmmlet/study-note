//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
//// 六、颜色检测
//Mat imgHSV, mask;
//int hmin = 0, smin = 110, vmin = 153;
//int hmax = 19, smax = 240, vmax = 255; // 如何确定这6个值，每次都更改所有这些再次运行很痛苦 -->创建跟踪栏（使我们可以实时更改这些值）
//
//int main() {
//	string path = "Resources/shapes.png";
//	Mat img = imread(path);
//	// 转换图像到HSV空间，在其中查找颜色更加容易
//	cvtColor(img, imgHSV, COLOR_BGR2HSV);
//
//	// 创建控制窗口
//	namedWindow("Trackbars", (640, 200));
//
//	// 控制图现象显示 运行时，把3个min的都移到最小值，把3个max的都移到最大值，然后移动使其保持为白色
//	createTrackbar("Hue Min", "Trackbars", &hmin, 179); // 对于hue色相饱和度最大180,对于另外两个色相饱和度最大255
//	createTrackbar("Hue Max", "Trackbars", &hmax, 179);
//	createTrackbar("Sat Min", "Trackbars", &smin, 255);
//	createTrackbar("Sat Max", "Trackbars", &smax, 255);
//	createTrackbar("Val Min", "Trackbars", &vmin, 255);
//	createTrackbar("Val Max", "Trackbars", &vmax, 255);
//
//	while (true)
//	{
//		//检查数组元素是否位于其他两个数组的元素之间。
//		Scalar lower(hmin, smin, vmin);
//		Scalar upper(hmax, smax, vmax);
//		// imgHSV为输入图像，mask为输出图像
//		inRange(imgHSV, lower, upper, mask);
//		imshow("Image", img);
//		imshow("Image HSV", imgHSV);
//		imshow("Image HSV", imgHSV);
//		imshow("Image Mask", mask);
//		waitKey(1);
//	}
//
//	return 0;
//};