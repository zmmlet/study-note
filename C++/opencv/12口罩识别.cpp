//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <opencv2/objdetect.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
//void main() {
//	// 创建摄像头连接 如果是只连接一个摄像头则默认0（包括笔记本电脑自带的摄像头），其余的外接可能需要更换数字
//	VideoCapture cap(0);
//	Mat frame; // 摄像头对象读取数据;
//
//	CascadeClassifier face_cascade, nose_cascade, mask_detector;
//	// 加载人脸检测训练模型 
//	face_cascade.load("Resources/haarcascade_frontalface_alt2.xml");
//	// 加载人脸鼻子模型
//	nose_cascade.load("Resources/haarcascade_mcs_nose.xml");
//	// 加载口罩模型
//	mask_detector.load("Resources/mask.xml");
//	// 判断是否为空
//	if (face_cascade.empty()){cout << "人脸模型XML文件未加载" << endl;}
//	if (nose_cascade.empty()){cout << "人脸鼻子模型XML文件未加载" << endl;}
//	if (mask_detector.empty()){cout << "口罩模型XML文件未加载" << endl;}
//	
//	/* 
//	detectMultiScale函数。它可以检测出图片中所有的人脸，并将人脸用vector保存各个人脸的坐标、大小（用矩形表示），函数由分类器对象调用：
//	void detectMultiScale(  
//		const Mat& image,  
//		CV_OUT vector<Rect>& objects,  
//		double scaleFactor = 1.1,  
//		int minNeighbors = 3,   
//		int flags = 0,  
//		Size minSize = Size(),  
//		Size maxSize = Size()  
//	);  
//	函数介绍：
//	参数1：image--待检测图片，一般为灰度图像加快检测速度；
//	参数2：objects--被检测物体的矩形框向量组；
//	参数3：scaleFactor--表示在前后两次相继的扫描中，搜索窗口的比例系数。默认为1.1即每次搜索窗口依次扩大10%;
//	参数4：minNeighbors--表示构成检测目标的相邻矩形的最小个数(默认为3个)。
//			如果组成检测目标的小矩形的个数和小于 min_neighbors - 1 都会被排除。
//			如果min_neighbors 为 0, 则函数不做任何操作就返回所有的被检候选矩形框，
//			这种设定值一般用在用户自定义对检测结果的组合程序上；
//	参数5：flags--要么使用默认值，要么使用CV_HAAR_DO_CANNY_PRUNING，如果设置为
//			CV_HAAR_DO_CANNY_PRUNING，那么函数将会使用Canny边缘检测来排除边缘过多或过少的区域，
//			因此这些区域通常不会是人脸所在区域；
//	参数6、7：minSize和maxSize用来限制得到的目标区域的范围。
//	*/
//	// 创建矩形并用于标识人脸
//	vector<Rect> faces, masks, noses;
//	
//	while (true) {
//		// 获取图片每一帧的数据
//		cap.read(frame);
//		// 转换图像颜色
//		/*cvtColor(img, frame, COLOR_BGR2GRAY);*/
//		// 检测人脸，并转换为人脸参数
//		face_cascade.detectMultiScale(frame, faces, 1.2, 10);
//		mask_detector.detectMultiScale(frame, masks, 1.03, 5);
//		nose_cascade.detectMultiScale(frame, noses, 1.2, 10);
//		// 搜索人脸
//		for (int i = 0; i < faces.size(); i++)
//		{
//			/*
//			rectangle 绘制边界矩形
//				tl()：topleft矩形左上角坐标
//				br()：bottom right矩形右下角坐标
//			*/
//			rectangle(frame, faces[i].tl(), faces[i].br(), Scalar(0, 0, 255), 2);
//			// 存放文本 TODO:Point() 坐标应该修改为动态
//			putText(frame, "no_mask", Point(137, 262), FONT_HERSHEY_DUPLEX, 3, Scalar(0, 0, 255), 2);
//		}
//
//		// 搜索人脸鼻子
//		for (int i = 0; i < noses.size(); i++)
//		{
//			rectangle(frame, noses[i].tl(), noses[i].br(), Scalar(10, 10, 255), 2);
//			putText(frame, "no_mask", Point(137, 262), FONT_HERSHEY_DUPLEX, 3, Scalar(0, 0, 255), 2);
//		}
//		// 搜索口罩
//		for (int i = 0; i < masks.size(); i++)
//		{
//			rectangle(frame, masks[i].tl(), masks[i].br(), Scalar(0, 255, 0), 2);
//			putText(frame, "have_mask", Point(137, 262), FONT_HERSHEY_DUPLEX, 3, Scalar(0, 255, 0), 2);
//		}
//		
//		// imshow 用于显示图像
//		imshow("Image", frame);
//		// waitKey() 数值越大播放越慢
//		int c = waitKey(5);
//	}
//}