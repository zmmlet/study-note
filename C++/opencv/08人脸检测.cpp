//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <opencv2/objdetect.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
//// 八、人脸检测
//void main() {
//	string path = "Resources/test.png";
//	Mat img = imread(path);
//
//	// 加载人脸检测训练模型 
//	CascadeClassifier faceCascade;
//	faceCascade.load("Resources/haarcascade_frontalface_default.xml");
//	// 判断是否为空
//	if (faceCascade.empty()){cout << "XML文件未加载" << endl;}
//	// 创建矩形变量，用于标识人脸
//	vector<Rect> faces;
//	// 使用人脸级连点检测多尺度方法
//	faceCascade.detectMultiScale(img, faces, 1.1, 10);
//	// 遍历人脸
//	for (int i = 0; i < faces.size(); i++) {
//		/*
//		rectangle 绘制边界矩形
//			tl()：topleft矩形左上角坐标
//			br()：bottom right矩形右下角坐标
//		*/
//		rectangle(img, faces[i].tl(), faces[i].br(), Scalar(0, 255, 0), 5);
//	}
//	imshow("Image", img);
//	waitKey(0);
//};