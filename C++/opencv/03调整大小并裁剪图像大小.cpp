//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
//// 三、调整大小并裁剪图像大小
//void main() {
//	string path = "Resources/test.png";
//	Mat img = imread(path);
//	Mat imgResize, imgCrop;
//
//	// 打印图像大小
//	cout << img.size() << endl;
//
//	// 1.调整图像大小 -- 指定大小缩放
//	// resize(img, imgResize, Size(301,306));
//	// 调整图像大小 -- 按照百分比缩放，原图像为1
//	 resize(img, imgResize, Size(), 0.5,0.5);
//
//	 //2.裁剪图像
//	 Rect roi(100,100,300,250);
//	 imgCrop = img(roi);
//
//
//	// 原图像大小
//	imshow("Image", img);
//	// 调整后的图像大小
//	imshow("Image Resize", imgResize);
//	// 裁剪图像
//	imshow("Image Crop", imgCrop);
//	waitKey(0);
//
//};