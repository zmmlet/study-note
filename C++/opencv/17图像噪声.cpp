//#include <cstdlib>
//#include <iostream>  
//#include <opencv2\core\core.hpp>  
//#include <opencv2\highgui\highgui.hpp>  
//#include <opencv2\imgproc\imgproc.hpp>  
//using namespace cv;
//using namespace std;
//Mat addSaltNoise(const Mat srcImage, int n);
//int main()
//{   
//    string path = "Resources/Noise.webp";
//    Mat srcImage = imread(path);
//    if (!srcImage.data)
//    {
//        cout << "读入图像有误！" << endl;
//        system("pause");
//        return -1;
//    }
//    imshow("原图像", srcImage);
//    Mat dstImage = addSaltNoise(srcImage, 3000);
//    imshow("添加椒盐噪声的图像", dstImage);
//    //存储图像  
//    //imwrite("salt_pepper_Image.jpg", dstImage);
//    waitKey();
//    return 0;
//}
//Mat addSaltNoise(const Mat srcImage, int n)
//{
//    //克隆一张一摸一样的图
//    Mat dstImage = srcImage.clone();
//    for (int k = 0; k < n; k++)
//    {
//        //随机取值行
//        //rand()是伪随机数生成函数，%是模运算，这个表达式的作用是随机生成012三个数字中的一个。
//        int i = rand() % dstImage.rows;
//        int j = rand() % dstImage.cols;
//        //图像通道判定  
//        if (dstImage.channels() == 1)
//        {
//            // at类中的at方法对于获取图像矩阵某点的RGB值或者改变某点的值很方便
//            //对于单通道的图像，则可以使用：
//            //    如：XXX.at<uchar>(i, j) = 255（因为单通道（灰度图），由黑到白）
//            //    （坐标(i, j)上，像素点的值为白色）
//            //    对于多通道的图像（BGR），如
//
//            //    img.at<Vec3b>(14, 25)[0] = 25;//B    
//            //img.at< Vec3b >(14, 25)[1] = 25;//G    
//            //img.at< Vec3b >(14, 25[2] = 25;//R 
//            dstImage.at<uchar>(i, j) = 255;       //盐噪声  
//        }
//        else
//        {
//            dstImage.at<Vec3b>(i, j)[0] = 255;
//            dstImage.at<Vec3b>(i, j)[1] = 255;
//            dstImage.at<Vec3b>(i, j)[2] = 255;
//        }
//    }
//    for (int k = 0; k < n; k++)
//    {
//        //随机取值行列  
//        int i = rand() % dstImage.rows;
//        int j = rand() % dstImage.cols;
//        //图像通道判定  
//        if (dstImage.channels() == 1)
//        {
//            dstImage.at<uchar>(i, j) = 0;     //椒噪声  
//        }
//        else
//        {
//            dstImage.at<Vec3b>(i, j)[0] = 0;
//            dstImage.at<Vec3b>(i, j)[1] = 0;
//            dstImage.at<Vec3b>(i, j)[2] = 0;
//        }
//    }
//    return dstImage;
//}