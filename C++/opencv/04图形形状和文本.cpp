//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
////
//// 四、图形形状和文本
//void main() {
//	
//	// 创建空白图片
//	Mat img(521,521, CV_8UC3, Scalar(255,255,255));
//	// 创建一个圆-边框厚度为10
//	//circle(img, Point(256, 256), 155, Scalar(255, 98, 0), 10);
//
//
//	// 创建一个圆-完全填充颜色
//	circle(img, Point(256, 256), 155, Scalar(255, 98, 0), FILLED);
//	// 长方形
//	rectangle(img, Point(130,226), Point(283,286),Scalar(255,255,255), FILLED);
//	// 创建一根线
//	line(img, Point(130, 296), Point(382, 296), Scalar(255, 255, 255), 2);
//	// 存放文本
//	putText(img, "workShopText", Point(137, 262), FONT_HERSHEY_DUPLEX, 3,Scalar(255, 255, 255), 2);
//	
//	imshow("Image", img);
//
//	waitKey(0);
//
//};