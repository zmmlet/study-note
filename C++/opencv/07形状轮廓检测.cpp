//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace std;
//using namespace cv;
//
////七、 形状/轮廓检测
//// 接收图像 Mat 数据类型在 opencv 中是矩阵数据
//Mat imgGray, imgBlur, imgCanny, imgDil, imgErode;
//
//
////imgDil是传入的扩张边缘的图像用来查找轮廓，img是要在其上绘制轮廓的图像
//void getContours(Mat imgDil, Mat img) {
//
//	/*{ 
//		{Point(20,30),Point(50,60)},
//		{},
//		{}
//	}*/
//	// 轮廓检测到的轮廓。每个轮廓线存储为一个点的向量
//	vector<vector<Point>> contours;
//	// 包含关于映像拓扑的信息  typedef Vec<int, 4> Vec4i;具有4个整数值
//	vector<Vec4i> hierarchy;
//	// 在二值图像中查找轮廓。该函数利用该算法从二值图像中提取轮廓
//	findContours(imgDil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
//	// img：要绘制轮廓在什么图片上，contours：要绘制的轮廓，-1定义要绘制的轮廓号（-1表示所有轮廓），Saclar表示轮廓颜色，2表示厚度
//	// drawContours(img, contours, -1, Scalar(255, 0, 255), 4);
//
//	vector<vector<Point>> conPoly(contours.size());//conploy的数量应小于contours
//	vector<Rect> boundRect(contours.size());
//	//过滤器：通过轮廓面积来过滤噪声
//	for (int i = 0; i < contours.size(); i++) {//遍历检测到的轮廓
//		int area = contourArea(contours[i]);
//
//		cout << "area:" << area << endl;
//
//		string objectType;
//		// 轮廓面积＞1000才绘制
//		if (area > 1000) {
//			//计算轮廓周长或凹坑长度。该函数计算了曲线长度和封闭的周长。
//			float peri = arcLength(contours[i], true);//计算封闭轮廓周长
//			// 以指定的精度近似多边形曲线。第二个参数conPloy[i]存储近似的结果，是输出。
//
//			/* 
//				approxPolyDP()函数是opencv中对指定的点集进行多边形逼近的函数，其逼近的精度可通过参数设置。
//				第三个参数double epsilon：指定的精度，也即是原始曲线与近似曲线之间的最大距离。
//				第四个参数bool closed：若为true，则说明近似曲线是闭合的；反之，若为false，则断开
//			*/
//			approxPolyDP(contours[i], conPoly[i], 0.02 * peri, true);
//			//计算边界矩形
//			boundRect[i] = boundingRect(conPoly[i]);
//			//找近似多边形的角点,三角形有3个角点，矩形/正方形有4个角点，圆形>4个角点
//			int objCor = (int)conPoly[i].size();
//
//			cout << "图形存在几个顶点：" << objCor << endl;
//			// 为不同顶点的图形添加类型
//			if (objCor == 3) { objectType = "Tri"; }
//			else if (objCor == 4) {
//				//宽高比
//				float aspRatio = (float)boundRect[i].width / (float)boundRect[i].height;
//				//矩形的宽高比不会正好等于1
//				if (aspRatio > 0.95 && aspRatio < 1.05) { 
//					objectType = "Square"; 
//				}else objectType = "Rect";
//			}
//			else if (objCor > 4) { objectType = "Circle"; }
//
//			drawContours(img, conPoly, i, Scalar(255, 0, 255), 2);
//			rectangle/*绘制边界矩形*/(img, boundRect[i].tl()/*tl()：topleft矩形左上角坐标*/, boundRect[i].br()/*br()：bottom right矩形右下角坐标*/, Scalar(0, 255, 0), 5);
//			putText(img, objectType, { boundRect[i].x,boundRect[i].y - 5 }/*文字坐标*/, FONT_HERSHEY_PLAIN, 1, Scalar(0, 69, 255), 2);
//		}
//	}
//}
//
//void main() {
//	string path = "Resources/shapes.png";
//	Mat img = imread(path);
//	/* 图像经过处理称为：预处理 */
//	// 转换图像颜色
//	cvtColor(img, imgGray, COLOR_BGR2GRAY);
//	// 添加图像模糊-高斯模糊
//	GaussianBlur(imgGray, imgBlur, Size(7, 7), 5, 0);
//	// canny 图像边缘检测器
//	Canny(img, imgCanny, 50, 150);
//	// 扩大图像（加粗图像检查测边缘线条）创建一个核，增加Size（只能是奇数）会扩张/侵蚀更多
//	Mat kernel = getStructuringElement(MORPH_RECT, Size(5, 5));
//	dilate(imgCanny, imgDil, kernel);
//
//	//img是在其上绘轮廓的图片
//	getContours(imgDil, img);
//	
//
//	imshow("Image", img);
//	//imshow("Image Gray", imgGray);
//	//imshow("Image Blur", imgBlur);
//	//imshow("Image Canny", imgCanny);
//	//imshow("Image Dil", imgDil);
//	waitKey(0);
//};