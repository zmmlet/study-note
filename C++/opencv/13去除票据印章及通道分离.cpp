//#include <opencv2/core/core.hpp>  
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <iostream>
//using namespace cv;
//using namespace std;
//int main() {
//	//定义相关变量
//	Mat srcImage, newImage;
//	Mat srcImage_B, srcImage_G, srcImage_R;
//	//存放Mat的数组vector
//	vector<Mat> channels_BGR;
//	//读取原始图像并检查图像是否读取成功 
//	string path = "Resources/voucher.jfif";
//	srcImage = imread(path);
//	if (srcImage.empty())
//	{
//		cout << "读取图像有误，请重新输入正确路径！\n";
//		getchar();
//		return -1;
//	}
//	imshow("src原图像", srcImage);  //在窗口显示原图像
//   //对加载的原图像进行通道分离，即把一个3通道图像转换成为3个单通道图像  
//	split(srcImage, channels_BGR);
//	//从数组中取出3种颜色,0通道为B分量，1通道为G分量，2通道为R分量。
//	srcImage_B = channels_BGR.at(0);
//	srcImage_G = channels_BGR.at(1);
//	srcImage_R = channels_BGR.at(2);
//	imshow("srcImage_B通道", srcImage_B); //分别显示R，G，B各个通道图像  
//	imshow("srcImage_G通道", srcImage_G);
//	imshow("srcImage_R通道", srcImage_R);
//	// 全局二值化
//	Mat gray;
//	cvtColor(srcImage, gray, COLOR_BGR2GRAY);
//	int th = 170; //阈值要根据实际情况调整
//	Mat binary;
//	//CV_THRESH_BINARY代表阈值其中一种模式，下期会讲起阈值
//	threshold(gray, binary, th, 255, THRESH_BINARY);
//	Mat red_binary;
//	threshold(srcImage_R, red_binary, th, 255, THRESH_BINARY);
//	imshow("灰色图 + 阈值处理 ", binary);
//	imshow("R通道+阈值处理", red_binary);
//	// 将3个单通道重新合并成一个三通道图像  
//	merge(channels_BGR, newImage);
//	imshow("将R，G，B通道合并后的图像", newImage);
//	waitKey(0);
//
//	return 0;
//};
