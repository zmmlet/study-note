////---------------------------------【头文件、命名空间包含部分】-----------------------------
////      描述：包含程序所使用的头文件和命名空间
////-------------------------------------------------------------------------------------------------
//#include "opencv2/core/core.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include <iostream>
//using namespace cv;
//using namespace std;
////--------------------------------------【main( )函数】-----------------------------------------
////          描述：控制台应用程序的入口函数，我们的程序从这里开始执行
////-------------------------------------------------------------------------------------------------
//int main()
//{
//    //【1】以灰度模式读取原始图像并显示
//    string path = "Resources/test.png";
//    Mat srcImage = imread(path, 0);
//    if (!srcImage.data) { 
//        cout << "读取图片错误，请确定目录下是否有imread函数指定图片存在~！" << endl;
//        getchar(); 
//        return false; 
//    }
//    imshow("原始图像", srcImage);
//    //【2】将输入图像延扩到最佳的尺寸，边界用0补充
//    //离散傅里叶变换的运行速度与图片的尺寸有很大关系。
//    //当图像尺寸是2，3，5整数倍时，计算速度最快。getOptimalDFTSize就是获取最佳尺寸
//    int m = getOptimalDFTSize(srcImage.rows);
//    int n = getOptimalDFTSize(srcImage.cols);
//    //将添加的像素初始化为0.
//    Mat padded;
//    //copyMakeBorder（）函数的作用是扩充图像边界
//    copyMakeBorder(srcImage, padded, 0, m - srcImage.rows, 0, n - srcImage.cols, BORDER_CONSTANT, Scalar::all(0));
//    //【3】为傅立叶变换的结果（复数）(实部和虚部)分配存储空间。
//    //将planes数组组合合并成一个多通道的数组complexI
//    //1.傅里叶结果是复数，这就是说对于每个原图像值，结果会有两个图像值
//    //2.频域值范围远远超过空间值范围，因此要将频域存储再float格式中。并多加一个通道储存复数部分
//    //3.什么是通道？请翻开第四篇文章谢谢，关注一下小嗷的公众号：aoxiaoji
//    Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
//    Mat complexI;
//    merge(planes, 2, complexI);
//    //【4】进行就地离散傅里叶变换
//    dft(complexI, complexI);
//    //【5】将复数转换为幅值，即=> log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
//    //请翻开第四篇文章有讲通道分离split。谢谢，关注一下小嗷的公众号：aoxiaoji
//    split(complexI, planes); // 将多通道数组complexI分离成几个单通道数组，planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
//    //计算二维矢量的幅值
//    magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude  
//    Mat magnitudeImage = planes[0];
//    //【6】进行对数尺度(logarithmic scale)缩放
//    //将复数转换为幅值范围太大。高值显示为白点，而低值为黑点，高低值的变化无法有效分辨。为了在屏幕上凹显出高低变化的连续性，用对数尺度替代线性尺度
//    magnitudeImage += Scalar::all(1);
//    log(magnitudeImage, magnitudeImage);//求自然对数
//    //【7】剪切和重分布幅度图象限
//    //若有奇数行或奇数列，进行频谱裁剪
//    //第二步，为了提高处理速度延扩了图像，现在是剔除添加的像素。
//    //为了方便显示，也可以重新分布幅度图像象限位置，
//    //（注：将第五步得到的幅度图从中间划开，得到4张1/4子图象，将每张子图堪称幅度图的一个象限，重新分布，即将4个角点重叠到图片中心）
//    //这样的话原点（0，0）就位移到图像中心了
//    magnitudeImage = magnitudeImage(Rect(0, 0, magnitudeImage.cols & -2, magnitudeImage.rows & -2));
//    //重新排列傅立叶图像中的象限，使得原点位于图像中心  
//    int cx = magnitudeImage.cols / 2;
//    int cy = magnitudeImage.rows / 2;
//    Mat q0(magnitudeImage, Rect(0, 0, cx, cy));   // ROI区域的左上
//    Mat q1(magnitudeImage, Rect(cx, 0, cx, cy));  // ROI区域的右上
//    Mat q2(magnitudeImage, Rect(0, cy, cx, cy));  // ROI区域的左下
//    Mat q3(magnitudeImage, Rect(cx, cy, cx, cy)); // ROI区域的右下
//                                                  //交换象限（左上与右下进行交换）
//    Mat tmp;
//    q0.copyTo(tmp);
//    q3.copyTo(q0);
//    tmp.copyTo(q3);
//    //交换象限（右上与左下进行交换）
//    q1.copyTo(tmp);
//    q2.copyTo(q1);
//    tmp.copyTo(q2);
//    //【8】归一化，用0到1之间的浮点值将矩阵变换为可视的图像格式（部分幅度值仍然超过可显示范围[0,1].幅度归一化到可显示范围）
//    //此句代码的OpenCV2版为：
//    //normalize(magnitudeImage, magnitudeImage, 0, 1, CV_MINMAX); 
//    //此句代码的OpenCV3版为:
//    normalize(magnitudeImage, magnitudeImage, 0, 1, NORM_MINMAX);
//    //【9】显示效果图
//    imshow("频谱幅值", magnitudeImage);
//    waitKey();
//    return 0;
//}