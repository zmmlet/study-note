# 基础语法

## C++ 初识

### 基础输出

- `#include <iostream>` 头文件
- std::cout 输出函数 ,面向对象的变成
- printf("输出内容"); c 语言中的函数，也可以执行原因时 c++ 时 c 的超集，c 的多数语法 c++都支持
- /n 作用换行符
- std::endl; 也是换行符的作用
- 在编译器中\只能输入 2 的倍数列入\\ 编译后输入只有一个\不能单个输入，除非是\n
- 关键字严格区分大小写
- std::cout moi

```c++
//#为预处理执行
//<iostream> 库函数
#include <iostream>
using namespace std;

int main() {
  std::cout << "Hello World\\C++\n";

    printf("大家伙\n我的名字叫\"张三\"");
    system 系统关机命令
    system("shutdown /s");
    system 系统关机取消命令
   system("shutdown /a");
    //暂停
  system("pause");
    printf("pause已被成功完成");

   system("cls");

   system("color 12");
}
```

### 注释

1. 单行注释 //
2. 多行注释 /\* \*/

### 变量

**变量的创建语法：数据类型 变量名 = 变量初始值**

```c++
//#为预处理执行
//<iostream> 库函数
#include <iostream>
using namespace std;
int main() {
  int a = 20;

    cout << "a=" << a << endl;
    system("pause");
  return 0;
}
```

### 常量

**作用：用于记录程序中不可更改的数据，C++ 定义常量的两种方式**

1. #define 宏常量： #define 常量名 常量值
   通常在文件上方定义，表示一个常量
2. const 修饰的变量 const 数据类型 常量名 = 常量值
   通常在变量定义前加关键字 const 修饰该变量为常量，不可修改

```c++
//#为预处理执行
//<iostream> 库函数
#include <iostream>
using namespace std;
// 1. #define 宏常量
#define Day 7

int main() {
  cout << "一周共有： " << Day << "天" << endl;
  //2. const 修饰的变量
  const int month = 12;
  //month = 80 // const 修饰的变量也是常量不可修改
  cout << "一年共有： " << month << "天" << endl;
  return 0;
}
```

### 关键字

**作用：关键字是 C++中预留的单词**
[关键字](https://www.runoob.com/w3cnote/cpp-keyword-intro.html)

### 标识符命名规则

**作用：C++规定给标识符（变量、常量）命名时，有一套规则**

- 标识符不能是关键字
- 标识符只能由字母、数字、下划线组成
- 第一个字符必须为字母或下划线
- 标识符中字母区分大小写
  建议：给标识符命名时，争取做到见名知意的效果

```c++
#include <iostream>
using namespace std;

int main() {
    int month = 12;
    cout << "一年共有： " << month << "天" << endl;

    const int _abs = 120;
    int num2 = 60;
    int c = _abs + num2;
    cout << "contunt:" << c << endl;
}
```

## 数据类型

C++规定在创建一个变量或常量时，必须要指定出响应的数据类型，否则无法给变量分配内存

### 整型

**作用：**整型变量表示的是整数类型的数据，**数据类型存在的意义就是给变量分配一个合适的内存空间**
C++中能表示整型的类型有以下几种方式，**区别在于所占内存空间不同**
|数据类型|占用空间|取值范围|
|--|--|--|
|short(短整型)|2 字节|(-2^15~2^12-1)|
|int(整型)|4 字节|(-2^31~2^31-1)|
|long(长整型)|Windows 为 4 字节、Linux 为 4 字节（32 位）、8 字节（64 位）|(-2^31~2^31-1)|
|long long(长长整型)|8 字节|(-2^63~2^63-1)|

```c++
#include <iostream>
using namespace std;

int main() {
    //1.短整型(-32768~32767) 超出范围则显示最小数
    short age = 32768;
    cout << "short_age=" << age << endl;

    //2.整型 最常用
    int a = 32768;
    cout << "int_a=" << a << endl;

    //3.长整型
    long num3 = 10;
    cout << "long_num3=" << num3 << endl;

    //4.长长整型
    long long num4 = 10;
    cout << "long_long_num4=" << num4 << endl;
}
```

### sizeof 关键字

作用：利用 sizeof 关键字可以统计数据类型所占内存大小
语法：`sizeof(数据类型、或者变量)`

```c++
#include <iostream>
using namespace std;

int main() {
  //1.短整型
  short age = 32768;
  cout << "short类型所占内存空间为：" << sizeof(age) << endl;

  cout << "int类型所占内存空间为：" << sizeof(int) << endl;

  cout << "long类型所占内存空间为：" << sizeof(long) << endl;

  cout << "long_long_类型所占内存空间为：" << sizeof(long long) << endl;
}
```

### 实型（浮点型）

作用：用于表示小数

**浮点型变量分为两种, 两者的区别在于表示的有效数字范围不同**

1. 单精度 float
2. 双精度 double

| 数据类型 | 占用空间 | 有效数字范围     |
| -------- | -------- | ---------------- |
| float    | 4 字节   | 7 位有效数字     |
| double   | 8 字节   | 15~16 位有效数字 |

默认情况下输出一个小数会显示 6 位有效数字

```c++
#include <iostream>
using namespace std;

int main() {
  //1. 单精度
  float f1 = 3.14f;
  //2. 双精度
  double f2 = 3.14;

  cout << f1 << endl;
  cout << f2 << endl;
  //float占用内存空间为：4
  //double占用内存空间为：8
  cout << "float占用内存空间为：" << sizeof(f1) << endl;
  cout << "double占用内存空间为：" << sizeof(f2) << endl;

  //科学计数法
  float f3 = 3e2; // 3 * 10 ^ 2
  cout << "科学计数法" << f3 << endl;

  float f4 = 3e-2; // 3*0.1
  cout << f4 << endl;
}
```

### 字符型

作用：字符型变量用于显示单个字符
语法：`char ch = 'a';`
注意：

1. 在显示字符型变量时，用单引号将字符括起来，不要用双引号
2. 单引号内只能有一个字符，不可以是字符串

- C 和 C++中字符型变量只占用 1 个字节
- 字符型变量并不是包字符本身放到内存中存储，而是将对应的 [ASCII 编码表](http://ascii.911cha.com/)放到存储单元

```c++

#include <iostream>
using namespace std;
int main() {
  //1.字符型变量创建方式
  char ch = 'c';
  cout << ch << endl;
  //2.字符型变量所占的内存大小
  cout << "字符型占用字节长度为：" << sizeof(ch) << endl;
  //3.字符型变量常见错误
  //char ch2 = "n"; // 创建字符型变量必须为单引号
  //char ch2 = "naaaa"; // 创建字符型变量，单引号内只能有一个字符

  //4.字符型变量对应ASCII编码
  //a - 97
  //A - 65
  cout << (int)ch << endl;
}
```

### 转移字符

作用：用于标识一些**不能显示出来的 ASCII 字符**
现阶段常用转义字符有：`\n \\ \t`
| 转义字符 |含义 | ASCII 码值（十进制） |
| -------- | -------- | ---------------- |
| \a | 警报 | 007 |
| \b | 退格（BS），当前位置移到前一列 | 008 |
| \f | 换页（FF），当前位置移到下页开头 | 012 |
| \n | 换行(LF) |010|
| \r | 回车(CR) | 013|
| \t | 水平制表(HT) | 009|
| \v | 垂直制表(VT) |011|
| \\ | 反斜杠| 092|
| \? | 问号字符 |063|
| \' | 单引号字符 | 039|
| \" | 双引号字符 | 034|
| \0 | 空字符(NULL) |000|
| \ddd | 任意字符 三位八进制 ||
| \xhh | 任意字符 二位十六进制 ||

```c++
#include <iostream>
using namespace std;
int main() {
	//换行符 \n
	cout << "白日依山尽\n" << endl;
	/*制表符 /t*/
	cout << "黄河入海流\t。" << endl;
	// 反斜杠
	cout << "\\" << endl;
}
```

### 字符串型

作用：用于表示一串字符
两种风格

1. C 风格字符串 `char 变量名[] = "字符串值"`

```c++
#include <iostream>
using namespace std;
int main() {
	char str1[] = "C风格 字符串";

	cout << str1 << endl;

	system("pause");
}
```

注意：C 风格字符串要用双引号括起来

2. C++风格字符串 `string 变量名 = "字符串值"`

```c++
#include <iostream>
#include <string>
using namespace std;
int main() {
	//包含一个头文件
	string str1 = "C++风格 字符串";

	cout << str1 << endl;

	system("pause");
}
```

### 布尔类型 bool

作用：布尔数据类型代表真或假的值

- true -- 真（本质 1）
- false -- 假（本质 0）

**bool 类型占 1 字节大小**

```c++

#include <iostream>
using namespace std;
int main() {

	bool flag = true;
	cout << flag << endl;

	flag = false;
	cout << flag << endl;

	cout << "flag of size:" << sizeof(flag) << endl;

	system("pause");
}
```

### 数据的输入

作用：用于从键盘获取数据
关键字：`cin` 语法:`cin >> 变量`

```c++
#include <iostream>
using namespace std;

#include <string>
int main() {

	//整型输入
	int a = 0;
	cout << "请输入整型变量：" << endl;
	cin >> a;
	cout << a << endl;

	//浮点型输入
	double d = 0;
	cout << "请输入浮点型变量：" << endl;
	cin >> d;
	cout << d << endl;

	//字符型输入
	char ch = 'a';
	cout << "请输入字符型变量：" << endl;
	cin >> ch;
	cout << ch << endl;

	// 字符串型
	string str = "hello world";
	cin >> str;
	cout << str << endl;

	//布尔型 只要非0的值都为真
	bool isTrue = false;
	cin >> isTrue;
	cout << isTrue << endl;

	system("pause");

	return 0;
}
```

## 运算符

作用：用于执行代码的运算
|运算符类型|作用|
|--|--|
|算术运算符|用于处理四则运算|
|赋值运算符|用于将表达式的值赋值给变量|
|比较运算符|用于表达式比较，并返回一个真值或假值|
|逻辑运算符|用于根据表达式的值返回真值或假值|

### 算术运算符

作用：用于处理四则运算
|运算符|术语|示例|
|--|--|--|
| + |把两个操作数相加| A + B 将得到 30|
| - |从第一个操作数中减去第二个操作数| A - B 将得到 -10|
| _ |把两个操作数相乘| A _ B 将得到 200|
| / |分子除以分母| B / A 将得到 2|
| % |取模运算符，整除后的余数| B % A 将得到 0|
| ++ | 自增运算符，整数值增加| 1 A++ 将得到 11|
| -- | 自减运算符，整数值减少| 1 A-- 将得到 9|

```c++
#include <iostream>
using namespace std;

#include <string>
int main() {
	//整型输入
	int a1 = 10;
	int b1 = 3;

	cout << a1 + b1 << endl;
	cout << a1 - b1 << endl;
	cout << a1 * b1 << endl;
	//两个整数相除，C++中自动舍弃小数，返回为整数
	cout << a1 / b1 << endl;
	cout << a1 % b1 << endl;


	system("pause");

	return 0;
}
```

递增++

```c++
#include <iostream>

using namespace std;
int main() {
	// 1.前置递增
	int aa = 10;
	cout << "aa=" << ++aa << endl;
	// 2.后置递增
	int bb = 10;
	cout << "bb=" << bb++ << endl;
	// 3.前置和后置的区别
	//前置递增先让变量 +1 然后进行表达式运算
	int a2 = 10;
	int b2 = ++a2 * 10;
	cout << "a2=" << a2 << endl;
	cout << "b2=" << b2 << endl;

	//后置递增先进行表达式运算，后让变量+1
	int a3 = 10;
	int b3 = a3++ * 10;
	cout << "a3=" << a3 << endl;
	cout << "b3=" << b3 << endl;
}
```

### 赋值运算符

作用：用于将表达式的值赋值给变量
|运算符| 描述 |实例|
|--|--|--|
|= |简单的赋值运算符，把右边操作数的值赋给左边操作数| C = A + B 将把 A + B 的值赋给 C|
|+=| 加且赋值运算符，把右边操作数加上左边操作数的结果赋值给左边操作数| C += A 相当于 C = C + A|
|-=| 减且赋值运算符，把左边操作数减去右边操作数的结果赋值给左边操作数| C -= A 相当于 C = C -A|
|_=| 乘且赋值运算符，把右边操作数乘以左边操作数的结果赋值给左边操作数| C _= A 相当于 C = C \* A|
|/= |除且赋值运算符，把左边操作数除以右边操作数的结果赋值给左边操作数| C /= A 相当于 C = C / A|
|%= |求模且赋值运算符，求两个操作数的模赋值给左边操作数| C %= A 相当于 C = C % A|
|<<= |左移且赋值运算符| C <<= 2 等同于 C = C << 2|
|>>= |右移且赋值运算符| C >>= 2 等同于 C = C >> 2|
|&= |按位与且赋值运算符| C &= 2 等同于 C = C & 2|
|^= |按位异或且赋值运算符| C ^= 2 等同于 C = C ^ 2|
||= |按位或且赋值运算符| C |= 2 等同于 C = C | 2|

```c++
#include <iostream>

using namespace std;
int main() {
	// 1. =
	int a = 10;
	a = 100;
	cout << "a=" << a << endl;
	// 2. +=
	a = 10;
	a += 2; // a = a + 2
	cout << "a=" << a << endl;
	// 3. -=
	a = 10;
	a -= 2; // a = a - 2
	cout << "a=" << a << endl;
	// 4. *=
	a = 10;
	a *= 2; // a = a * 2
	cout << "a=" << a << endl;
	// 5. /=
	a = 10;
	a /= 2; // a = a / 2
	cout << "a=" << a << endl;
}
```

### 关系运算符

作用：用于表达式比较，并返回一个真值或假值
假设变量 A 的值为 10，变量 B 的值为 20，则：
|运算符| 描述| 实例|
|--|--|--|
|==| 检查两个操作数的值是否相等，如果相等则条件为真。| (A == B) 不为真。|
|!=| 检查两个操作数的值是否相等，如果不相等则条件为真。| (A != B) 为真。|
|>| 检查左操作数的值是否大于右操作数的值，如果是则条件为真。| (A > B) 不为真。|
|< |检查左操作数的值是否小于右操作数的值，如果是则条件为真。| (A < B) 为真。|
|>=| 检查左操作数的值是否大于或等于右操作数的值，如果是则条件为真。| (A >= B) 不为真。|
|<=| 检查左操作数的值是否小于或等于右操作数的值，如果是则条件为真。| (A <= B) 为真。|

```c++
#include <iostream>

using namespace std;
int main() {
	int a = 10;
	int b = 10;
	int c = 20;
	if (a == b) {
		cout << "相等" << endl;
	} else {
		cout << "不相等" << endl;
	}

	if (b <= c) {
		cout << "<=成立" << endl;
	}
}
```

### 逻辑运算符

作用：用于根据表达式的值返回真值或假值
假设变量 A 的值为 1，变量 B 的值为 0，则：
|运算符| 描述 |实例|
|--|--|--|
| && |称为逻辑与运算符。如果两个操作数都 true，则条件为 true| (A && B) 为 false|
| || | 称为逻辑或运算符。如果两个操作数中有任意一个 true，则条件为 true。| (A || B) 为 true|
| ! |称为逻辑非运算符。用来逆转操作数的逻辑状态，如果条件为 true 则逻辑非运算符将使其为 false|!(A && B) 为 true|

```c++
#include <iostream>

using namespace std;
int main() {
  int a = 5;
  int b = 20;
  int c;

  if (a && b)
  {
    cout << "Line 1 - 条件为真" << endl;
  }
  if (a || b)
  {
    cout << "Line 2 - 条件为真" << endl;
  }
  /* 改变 a 和 b 的值 */
  a = 0;
  b = 10;
  if (a && b)
  {
    cout << "Line 3 - 条件为真" << endl;
  }
  else
  {
    cout << "Line 4 - 条件不为真" << endl;
  }
  if (!(a && b))
  {
    cout << "Line 5 - 条件为真" << endl;
  }
  return 0;
}
```

## 程序流程结构

C/C++支持最基础的三种程序结构：顺序结构、选择结构、循环结构

- 顺序结构：按照顺序执行，不发生跳转
- 选择结构：根据条件是否满足，有选择的执行相应的程序
- 循环结构：根据条件是否满足，循环多次执行某段代码

### 选择结构

#### if 语句

作用执行：满足条件的语句
if 语句三种形式：

1. 单行格式 if 语句
2. 多行格式 if 语句
3. 多条件的 if 语句
4. 嵌套 if 语句

**1. 单行格式 if 语句：`if(条件){条件满足执行的语句}`**

```c++
#include <iostream>

using namespace std;
int main() {
  /*
  选择结构-单行if语句
  输入一个分数、分数大于60分视为及格
  */
  cout << "提示：输入一个分数、分数大于60分视为及格" << endl;
  int score = 0;
  cout << "请输入一个分数" << endl;
  cin >> score;
  cout << "您输入的分数为" << endl;
  if (score >= 60) {
    cout << "恭喜考试及格，考试成绩为：" << score << endl;
  }

  return 0;
}
```

**2. 多行格式 if 语句：`if(条件){条件满足执行的语句} else {条件不满足的语句}`**

```c++
#include <iostream>

using namespace std;
int main() {
  /*
  选择结构-单行if语句
  输入一个分数、分数大于60分视为及格
  */
  cout << "提示：输入一个分数、分数大于60分视为及格" << endl;
  int score = 0;
  cout << "请输入一个分数" << endl;
  cin >> score;
  cout << "您输入的分数为" << endl;
  if (score >= 60) {
    cout << "恭喜考试及格，考试成绩为：" << score << endl;
  }
  else {
    cout << "考试不及格，考试成绩为：" << score << endl;
  }

  return 0;
}
```

**3. 多条件的 if 语句：`if(条件1){条件1满足执行的语句} else if (条件2) {条件2满足执行的语句} else {条件不满足的语句}`**

```c++
#include <iostream>

using namespace std;
int main() {
  /*
  选择结构-单行if语句
  输入一个分数、分数大于60分视为及格
  */
  cout << "提示：输入一个分数、分数大于60分视为及格" << endl;
  int score = 0;
  cout << "请输入一个分数" << endl;
  cin >> score;
  cout << "您输入的分数为" << endl;
  if (score >= 85) {
    cout << "恭喜考试及格,评定为优秀，考试成绩为：" << score << endl;
  }
  else if (score >= 60) {
    cout << "恭喜考试及格，考试成绩为：" << score << endl;
  }
  else {
    cout << "考试不及格，考试成绩为：" << score << endl;
  }

  return 0;
}
```

**4. 嵌套 if 语句：**
语法：

```c++
if( boolean_expression 1)
{
   // 当布尔表达式 1 为真时执行
   if(boolean_expression 2)
   {
      // 当布尔表达式 2 为真时执行
   }
}
```

示例：

```c++
#include <iostream>
using namespace std;

int main ()
{
   // 局部变量声明
   int a = 100;
   int b = 200;

   // 检查布尔条件
   if( a == 100 )
   {
       // 如果条件为真，则检查下面的条件
       if( b == 200 )
       {
          // 如果条件为真，则输出下面的语句
          cout << "a 的值是 100，且 b 的值是 200" << endl;
       }
   }
   cout << "a 的准确值是 " << a << endl;
   cout << "b 的准确值是 " << b << endl;

   return 0;
}
```

#### 三目运算符

作用：通过三目运算符实现简单的判断
语法：`表达式1 ? 表达式2 : 表达式3`
表达式 1 为真、执行表达式 2，并返回表达式 2 的结果
表达式 1 为假、执行表达式 3，并返回表达式 3 的结果

```c++
#include <iostream>

using namespace std;
int main() {
  int a = 10;
  int b = 20;
  int c = 0;
  c = (a > b ? a : b);
  cout << c << endl;
  //C++中三目运算符返回的是变量可以继续赋值
  (a > b ? a : b) = 100;
  cout << "a=" << a << endl;
  cout << "b=" << b << endl;
  return 0;
}
```

#### switch 语句

作用：执行多条件分支语句

**if 和 switch 区别？**
switch 缺点、判断时只能是整型或者字符串，不可以是一个区间
switch 优点、结构清晰，执行效率高

**注意:**`case` 中必须写`break`
**语法：**

```c++
switch(expression){
    case constant-expression  :
       statement(s);
       break; // 可选的
    case constant-expression  :
       statement(s);
       break; // 可选的

    // 您可以有任意数量的 case 语句
    default : // 可选的
       statement(s);
}
```

```c++
#include <iostream>

using namespace std;
int main() {
	char grade = 'A';
	cout << "请输入电影《功夫》评分，默认评价为A" << endl;
	//cin 用户输入
	cin >> grade;
	cout << "您对电影《功夫》的评分为：" << grade << endl;

	switch (grade){
	case 'A':
		cout << "经典的电影" << endl;
		break; // 退出当前分支
	case 'B':
		cout << "很好的电影" << endl;
		break;
	case 'C':
		cout << "还行的电影" << endl;
		break;
	case 'D':
		cout << "凑合的电影" << endl;
		break;
	default:
		cout << "烂片" << endl;
		break;
	}
  return 0;
}
```

### 循环结构

#### while 循环语句

作用：满足循环条件、执行循环语句
语法：`while(循环条件){循环语句}`
解释：只要循环条件的结果为真，就执行循环语句

```c++
#include <iostream>

using namespace std;
int main() {
	// 在屏幕中打印 0~9这是个数字
	int num = 0;
	cout << num << endl;
	while (num < 10)
	{
		cout << "循环数据为：" << num << endl;
		num++;
	}

  return 0;
}
```

**while 循环练习案例：猜数字**
案例描述：系统随机生成一个 1~100 之间的数字，玩家进行猜测，如果猜错提示玩家过大或过小，如果猜对恭喜玩家胜利，并退出游戏

```c++
#include <iostream>
#include <ctime>
using namespace std;
int main() {
	//添加随机数种子，利用当前系统时间生成随机数，防止每次随机数一样
	srand((unsigned int)time(NULL));
	//1. 系统生成随机数
	int num = rand() % 100 + 1; // rand()%100 + 1 生成 0~99 + 1的随机数
	//2. 玩家进行猜测
	int val = 0; // 玩家输入的数据
	cout << "请输入数字进行1~100随机数，猜数游戏" << endl;
	while (true)
	{
		cin >> val;
		//3. 判断玩家进行猜测
		// 猜错 提示猜错的结果，过大或者过小 重新返回第2步
		if (val > num) {
			cout << "您输入的数字为：" << val << "_______猜测过大" << endl;
		}
		else if (val < num) {
			cout << "您输入的数字为：" << val << "_______猜测过小" << endl;
		}
		else {
			cout << "您输入的数字为：" << val << "_______猜测对了" << endl;
			break; // // 猜对 退出循环
		}
	}
  return 0;
}
```

#### do...while 循环语句

作用：满足循环条件、执行循环语句
语法：`do{循环语句}while(循环条件)`
注意：与 while 的区别在于 do...while 会先执行一次循环语句，再判断循环条件

```c++
#include <ctime>
using namespace std;
int main() {
	int num = 0;
	do
	{
		cout << num << endl;
		num++;
	} while (num < 10);
  return 0;
}
```

**do...while 循环练习案例：水仙花数**
案例描述：水仙花数是指一个 3 位数，它的每个位上的数字的 3 次幂之和等于它本身,求出所有 3 位数中的水仙花数
例如：1^3+5^3+3^3 = 153

```c++
#include <iostream>
using namespace std;
int main() {
	//所有三位数 100~999
	/*
	153
	1*1*1 = 1
	5*5*5 = 125
	3*3*3 = 27
	在所有三位数中找水仙花数
	获取个位 153%10 = 3
	获取十位 153/10 = 15    15%10 = 5 先整除10得到两位数，再取模于10得到十位
	获取百位 153/100 = 1		直接整除100，获取百位
	*/
	//判断个位^3 + 十位^3 + 百位^3 == 本身

	int num = 100;
	do
	{
		// 如果是水仙花数才打印
		int a = 0; // 个位
		int b = 0; // 十位
		int c = 0; // 百位
		a = num % 10; // 获取数字个位
		b = num / 10 % 10; // 获取数字十位
		c = num / 100; // 获取数字的百位
		// 看是不是水仙花数
		if (a * a * a + b * b * b + c * c * c == num) {
			cout << "水仙花数:" << num << endl;
		}

		num++;
	} while (num < 1000);
}
```

#### for 循环语句

作用：满足循环条件，执行循环语句
语法：`for(起始表达式;条件表达式;末尾循环体) {循环语句}`

```c++
#include <iostream>
using namespace std;
int main() {

	for (int i = 0; i < 10; i++)
	{
		cout << i << endl;
	}

	system("pause");
	return 0;
}
```

**练习案例：敲桌子**
案例描述：从 1 开始数到数字 100，如果数字各位含有 7，或者数字十位含有 7，或者说该数字为 7 的倍数，打印敲桌子，其他直接打印数字。

```c++
#include <iostream>
using namespace std;
int main() {

	for (int i = 1; i < 100; i++) {
		//int a = i % 7; // 7的倍数
		//int b = i % 10; // 个位有7
		//int c = i / 10; // 十位有7
		if (i%7 == 0 || i%10 == 7 || i/10 == 7) {
			cout << "7的倍数_敲桌子" << i << endl;
		} else {
			cout << i << endl;
		}
	}

	system("pause");
	return 0;
}
```

#### 嵌套循环

**利用嵌套循环打印 10\*10 的星云图形**

```c++
#include <iostream>
using namespace std;
int main()
{
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      cout << "* ";
    }
    cout << endl;
  }
}
```

**利用嵌套循环九九乘法表**

```c++

#include <iostream>
using namespace std;
int main()
{
  for (int i = 1; i <= 9; i++) {
    for (int j = 1; j <= i; j++) {
      cout << i << "x" << j << "=" << i * j << "\t";
    }
    cout << endl;
  }
}

```

### 跳转语句

#### break 语句

作用：用于跳出选择结构或者循环结构
break 使用的时机：

- 出现再 switch 条件语句中，作用是中止 case 并跳出 switch
- 出现在循环语句中，作用是跳出当前的循环语句
- 出现在嵌套循环中，跳出最近的内层循环语句

**1. 出现在 switch 语句中**

```c++
#include <iostream>
using namespace std;
int main()
{
  // 出现在 switch 语句中
  cout << "请选择副本难度" << endl;
  cout << "1.普通" << endl;
  cout << "2.中等" << endl;
  cout << "3.困难" << endl;

  int select = 0; // 创建选择结果变量
  cin >> select; // 等待用户输入

  switch (select)
  {
  case 1:
    cout << "您选择的难度是普通" << endl;
    break;
  case 2:
    cout << "您选择的难度是中等" << endl;
    break;
  default:
    cout << "您选择的难度是困难" << endl;
    break;
  }
}
```

**2. 出现在循环语句中**

```c++
#include <iostream>
using namespace std;
int main()
{

  for (int i = 0; i < 10; i++)
  {
    if (i == 5) break; // 到5退出循环
    cout << i << endl;
  }
}
```

**3. 出现再嵌套循环语句中**

```c++

#include <iostream>
using namespace std;
int main()
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (j == 5) break; // 退出内层循环
			cout << "* ";
		}
		cout << endl;
	}
}
```

#### continue 语句

作用：在循环语句中，跳过本次循环中余下尚未执行的语句，继续执行下一次循环
**continue 跳过本次循环，break 会直接跳出循环**

```c++
#include <iostream>
using namespace std;
int main()
{
	for (int i = 0; i < 100; i++)
	{
		if (i % 2 == 0) continue; // 如果是偶数则跳过循环，只打印奇数
		cout << i << endl;
	}
}
```

#### goto 语句

作用：可以无条件跳转语句
语法：`goto 标记;`
解释：如果标记名称存在，执行到`goto`语句时，会跳转到标记的位置

```c++
#include <iostream>
using namespace std;
int main()
{
	cout << "1.xxxxxxxxx" << endl;
	goto FLAG;
	cout << "2.xxxxxxxxx" << endl;
	cout << "3.xxxxxxxxx" << endl;
	FLAG:
	cout << "4.xxxxxxxxx" << endl;
}
```

## 数组

### 概述

所谓的数组，就是一个集合，里面存放了相同类型的数组元素

- 特点 1：数组中的每个**数组元素都是相同的数据类型**
- 特点 2：数组是由**连续的内存位置**组成的

### 一维数组

#### 一维数组定义方式

一维数组定义的三种方式：

1. `数据类型 数组名[数组长度];`
2. `数据类型 数组名[数组长度] = {值1,值2,...};`
3. `数据类型 数组名[] = {值1,值2,...};`

```c++

#include <iostream>
using namespace std;
int main()
{
	//1. 数据类型 数组名[数组长度];
	int score[10];
	// 根据下标进行赋值
	score[0] = 100;
	score[1] = 200;
	// 访问数组元素
	cout << score[1] << endl;

	//2. 数据类型 数组名[数组长度] = { 值1,值2,... };
	int balance[5] = { 1000, 2, 3, 7, 50 };
	for (int i = 0; i < 5; i++)
	{
		cout << balance[i] << endl;
	}


	//3. 数据类型 数组名[] = { 值1,值2,... };
	char strarr[] = { '张三', '李四'};
}
```

#### 一维数组数组名

用途：

1. 统计整个数组再内存中的长度
2. 获取数组再内存中的首地址

**数组名是一个常量，不可以重新赋值操作**

```c++

#include <iostream>
using namespace std;
int main()
{
	//1. 可以获取整个数组占用内存大小
	int arr[5] = { 1000, 2, 3, 7, 50 };

	cout << "整个数组所占内存空间为" << sizeof(arr) << endl;
	cout << "每个元素所占内存空间为" << sizeof(arr[0]) << endl;
	cout << "数组元素个数为" << sizeof(arr) / sizeof(arr[0]) << endl;

	//2. 可以通过数组名获取到数组首地址
	cout << "数组首地址为" << (int)arr << endl;

	cout << "数组中第一个元素的地址" << (int)&arr[0] << endl;
	cout << "数组中第二个元素的地址" << (int)&arr[1] << endl;

}

```

**练习案例 1：五只 🐖 称体重**
案例描述：在一个数组中记录了五只小猪的体重，如 int arr[5] = {300,350,200,400,260}; 打印并找出最重的小猪体重

```c++

#include <iostream>
using namespace std;
int main()
{
	int arr[5] = { 300,350,200,400,260 };
	int max = 0;
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		// 最大值大于默认最大值，跟新最大体重
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	cout << "最重的猪体重为：" << max << endl;
}

```

**练习案例 2：数组元素逆置**
案例描述：清明一个 5 个元素的数组，并且将元素逆置。（如原数组元素为：1,3,5,2,4 逆置后的结果为 4,2,5,3,1）

```c++

#include <iostream>
using namespace std;
int main()
{
	int arr[5] = { 1,3,5,2,4 };
	int start = 0; // 起始下标
	int end = sizeof(arr) / sizeof(arr[0]) - 1; // 末尾元素下标

	// sizeof(arr) / sizeof(arr[0]) 获取了数组长度
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		cout << "数组元素逆置前：" << arr[i] << endl;
	}


	cout << "\n" << endl;

	while (start < end)
	{
		// 实现元素互换
		int temp = arr[start]; // 临时数据
		arr[start] = arr[end];
		arr[end] = temp;

		// 下标更新
		start++;
		end--;
	}

	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		cout << "数组元素逆置后：" << arr[i] << endl;
	}

}
```

#### 冒泡排序

作用：最常用的排序算法，对数组内元素进行排序

1. 比较相邻的元素，如果第一个比第二个大，就交换他们两个
2. 每一对相邻元素做同样的工作，执行完毕后，找到第一个最大值
3. 重复以上的步骤，每次比较次数=1，直到不需要比较

示例：将数组{4,2,5,8,6,7,89,45,11,52} 进行升序排序

```c++

#include <iostream>
using namespace std;
int main()
{
	int arr[] = { 4,2,5,8,6,7,89,45,11,52 };
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		cout << "排序前" << arr[i] << endl;
	}


	cout << "\n" << endl;

	// 开始冒泡排序
	// 总共排序轮数为 元素个数 - 1
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		// 内层循环对比次数 = 元素个数-当前轮数-1
		for (int j = 0; j < sizeof(arr) / sizeof(arr[0]) - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1]) {
				// 交换位置
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}


	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		cout << "排序后" << arr[i] << endl;
	}

}
```

### 二维数组

多维数组最简单的形式是二维数组。一个二维数组，在本质上，是一个一维数组的列表。声明一个 x 行 y 列的二维整型数组，形式如下：
`type arrayName [ x ][ y ];`

#### 二维数组定义方式

二维数组定义的四种方式：

1. `数据类型 数组名[行数][列数];`
2. `数据类型 数组名[行数][列数] = {{数据1, 数据2}, {数据3, 数据4}};`
3. `数据类型 数组名[行数][列数] = {数据1, 数据2, 数据3, 数据4};`
4. `数据类型 数组名[][列数] = {数据1, 数据2, 数据3, 数据4};`

建议：以上 4 四种定义方式，利用第二种更加直观，提高代码可读性

```c++

#include <iostream>
using namespace std;
int main()
{
	//1.数据类型 数组名[行数][列数];
	int arr[2][3];
	arr[0][0] = 1;
	arr[0][1] = 2;
	arr[0][2] = 3;
	arr[1][0] = 4;
	arr[1][1] = 5;
	arr[1][2] = 6;

	cout << arr[0][0] << endl;
	//外层循环打印行数，内层循环打印列数
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << arr[i][j] << endl;
		}
	}



	//2.数据类型 数组名[行数][列数] = { {数据1, 数据2}, {数据3, 数据4} };
	int arr2[2][3] = {
		{1,2,3},
		{4,5,6}
	};

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << arr2[i][j] << " ";
		}
		cout << endl;
	}

	//3.数据类型 数组名[行数][列数] = { 数据1, 数据2, 数据3, 数据4 };
	int arr3[2][3] = { 1,5,6,8,5,9 };
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << arr3[i][j] << " ";
		}
		cout << endl;
	}
	//4.数据类型 数组名[][列数] = { 数据1, 数据2, 数据3, 数据4 };
	int arr4[][3] = { 1,5,6,8,5,9 };

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << arr4[i][j] << " ";
		}
		cout << endl;
	}
}

```

#### 二维数组数组名

- 查看二维数组所占内存空间
- 获取二维数组首地址

```c++

#include <iostream>
using namespace std;
int main()
{
	// 定义一个两行三列的二维数组
	int arr2[2][3] = {
		{1,2,3},
		{4,5,6}
	};

	//查看二维数组所占内存空间
	cout << "二维数组占用内存空间" << sizeof(arr2) << endl;
	cout << "二维数组第一行占用内存空间" << sizeof(arr2[0]) << endl;
	cout << "二维数组第一个元素占用内存空间" << sizeof(arr2[0][0]) << endl;

	cout << "二维数组行数为" << sizeof(arr2) / sizeof(arr2[0]) << endl;
	cout << "二维数组列数为" << sizeof(arr2) / sizeof(arr2[0][0]) << endl;


	//获取二维数组首地址
	cout << "二维数组的首地址" << (int)arr2 << endl;
	cout << "二维数组中第一行的首地址" << (int)arr2[0] << endl;
	cout << "二维数组中第二行的首地址" << (int)arr2[1] << endl;


	cout << "二维数组中第一个元素的首地址" << (int)&arr2[0][0] << endl;
	cout << "二维数组中第二个元素首地址" << (int)&arr2[0][1] << endl;

}

```

#### 二维数组应用案例

**考试成绩统计：**
案例描述：有三名同学（张三、李四、王五）再一次考试中成绩分表如下表，请分别输出三位同学的总成绩
||语文|数学|英语|
|--|--|--|--|
|张三|100|100|100|
|李四|90|50|100|
|王五|60|70|80|

```c++

#include <iostream>
using namespace std;
#include <string>
int main()
{
	// 成绩数组
	int scores[3][3] = {
		{100, 100, 100},
		{90, 50, 100},
		{60, 70, 80}
	};

	string names[3] = { "张三", "李四", "王五" };

	// 统计分数
	for (int i = 0; i < 3; i++)
	{
		int sum = 0; // 统计
		for (int j = 0; j < 3; j++)
		{
			sum += scores[i][j];
			//cout << scores[i][j] << " ";
		}
		cout << names[i] << "个人的总分为" << sum << endl;
	}

}

```

## 函数

### 概述

作用：将一段经常使用的代码封装起来，减少重复代码
一个较大的程序，一般分为若干个程序块，每个模块实现特定的功能。

### 函数的定义

函数的定义一般主要有五个步骤：

1. 返回值类型
2. 函数名
3. 参数表列
4. 函数体语句
5. return 表达式

**语法：**

```c++
返回值类型 函数名(参数列表) {
  函数体语句
  return 表达式
}
```

**定义加法函数，实现两数相加**

```c++

#include <iostream>
using namespace std;

//函数的定义
//语法：返回值类型 函数名(列表阐述) {函数体语句 return表达式}

// 定义加法函数，实现两数相加
int add(int num1 , int num2) {
	int sum = num1 + num2;
	return sum;
}

void main()
{

}

```

### 函数的调用

功能：使用定义好的函数
语法：函数名（参数）

```c++

#include <iostream>
using namespace std;

//函数的定义
//语法：返回值类型 函数名(列表阐述) {函数体语句 return表达式}

// 定义加法函数，实现两数相加
// 初始化 num2 的默认值，调用时可以选择性传递
int add(int num1 , int num2 = 20) {
	int sum = num1 + num2;
	return sum;
}

void main()
{
	int a = 10;
	int b = 30;
	// main 函数中调用函数
	int result = add(a,b);
	cout << result << endl;
}

```

### 值传递

- 所谓值传递，就是函数调用时实参将数值传入给形参
- 值传递时，如果形参发生改变，并不会影响实参

```c++

#include <iostream>
using namespace std;

// 值传递
// 定义函数，实现两数字进行交换
void swap(int num1 , int num2) {
	cout << "交换前num1" << num1 << endl;
	cout << "交换前num2" << num2 << endl;

	int temp = num1;
	num1 = num2;
	num2 = temp;

	cout << "交换后num1" << num1 << endl;
	cout << "交换后num2" << num2 << endl;
}

void main()
{
	int a = 10;
	int b = 30;
	// 值传递时，如果形参发生改变，并不会影响实参
	swap(a,b);

}

```

### 函数的常见样式

1. 无参无返
2. 有参无反
3. 无参有返
4. 有参有返

```c++

#include <iostream>
using namespace std;

//1. 无参无返
void test1() {
	cout << "无参无返" << endl;
}

//2. 有参无反
void test2(int num) {
	cout << "有参无反" << num << endl;
}

//3. 无参有返
int test3() {
	int num = 20;
	return num;
}


//4. 有参有返
int test4(int num1) {
	return num1;
}

void main()
{
	int a = 10;
	int b = 30;
	test1();
	test2(a);
	test3();
	test4(b);
}

```

### 函数的声明

作用：告诉编译器函数名称及如何调用函数，函数的实际主体可以单独定义

- 函数的**声明可以多次**，但是函数的**定义只能有一次**

```c++

#include <iostream>
using namespace std;

// 函数声明，提前告诉编译器函数的存在，可以利用函数的声明
// 函数声明可以写多次，函数定义只能有一次
int max(int a, int b);

void main()
{
	int a = 10;
	int b = 30;
	// 函数调用打印
	cout << "最大值为" << max(a, b) << endl;
}


// 函数定义-实现返回最大值
int max(int num1, int num2) {
	return num1 > num2 ? num1 : num2;
}
```

### 函数的分文件编写

作用：让代码结构更清晰
函数分文件编写一般有 4 个步骤：

1. 创建后缀名为`.h` 的头文件
2. 创建后缀名为`.cpp` 的源文件
3. 在头文件中写函数的声明
4. 在源文件中写函数的定义

**一、定义文件**
**1. 创建后缀名为`.h` 的头文件**

```c++
#pragma once
#include <iostream>
using namespace std;
//函数的声明
void swap(int a, int b);
```

**2. 创建后缀名为`.cpp` 的源文件**

```c++
#include "swap.h"

// 函数的定义-实现两数字交换函数
void swap(int a, int b) {
	int temp = a;
	a = b;
	b = temp;

	cout << "a=" << a << endl;
	cout << "b=" << b << endl;
}
```

**一、引入头文件，调用函数**

```c++

#include <iostream>
using namespace std;

// 引用头文件
#include "swap.h"

void main()
{
	int a = 10;
	int b = 30;
	// 函数的调用
	swap(a, b);
}

```

## 指针

### 指针的基本概念

指针的作用：**可以通过指针间接访问内存**

- 内存编号是从 0 开始记录的，一般用 16 进制数字表示
- 可以利用指针变量保存地址

### 指针变量的定义和使用

指针变量定义语法：`数据类型 * 变量名;`

```c++

#include <iostream>
using namespace std;

void main()
{

	// 1.定义一个指针
	int a = 10;
	// 指针变量定义语法：`数据类型 * 变量名;`
	int* p;
	// 让指针记录变量a地址
	p = &a;

	cout << "a的地址为：" << &a << endl;
	cout << "指针p为：" << p << endl;

	// 2.使用指针
	//可以通过解引用的方式来找到指针只想的内存，指针前添加 * 号代表解引用，找到指针指向的内存中的数据
	*p = 1000;

	cout << "a=" << &a << endl;
	cout << "*p=" << *p << endl;
}
```

### 指针所占内存空间

提问：指针也是种数据类型，那么这种数据类型占用多少内存空间？

```c++

#include <iostream>
using namespace std;

int main()
{

   /* 在32位操作系统下：指针占用4个字节空间，和数据类型无关
    在64位操作系统下：指针占用8个字节空间，和数据类型无关*/
    int a = 10;
   /* int* p;
    p = &a;*/
    // 创建一个指针并且指定值
    int* p = &a;

    cout << "sizeof(int*) = " << sizeof(int*) << endl;
    cout << "sizeof(float*) = " << sizeof(float*) << endl;
    cout << "sizeof(char*) = " << sizeof(char*) << endl;
    cout << "sizeof(double*) = " << sizeof(double*) << endl;

    return 0;
}

```

### 空指针和野指针

空指针：指针变量指向内存编号为 0 的空间
用途：初始化指针变量
注意：空指针指向的内存是不可以访问的 </br>
**示例 1：空指针**

```c++
#include <iostream>
using namespace std;

int main() {
	// 指针变量 p 指向内存地址编号为0的空间
	int*p = NULL;
	// 访问空指针报错
	// 内存编号0~255为系统占用内存，不允许用户访问
	cout << *p << endl;

	system("pause");

	return 0;
}
```

**示例 2：野指针**<br>
野指针：指针变量指向非法的内存空间

```c++

#include <iostream>
using namespace std;

int main()
{
    // 指针变量p指向内存地址编号为0x1100的空间,尽量在程序中出现野指针
    int* p = (int*)0x1100;
    // 访问野指针报错
    cout << *p << endl;

    system("pause");

    return 0;
}

```

### const 修饰指针

const 修饰指针的三种情况：

- const 修饰指针---常量指针
- const 修饰产量---指针常量
- const 即修饰指针，又修饰常量

```c++

#include <iostream>
using namespace std;

void main()
{
    int a = 10;
    int b = 20;

    // 1.const 修饰的是指针-- 常量指针，特点：指针的指向可以修改，但是指针指向的值不可以改
    const int* p = &a;

    cout << *p << endl;
    p = &b;

    cout << *p << endl;

    // 2.cosnt修饰常量 ---指针常量：特点：指针的指向不可以修改，指针的值可以修改
    int* const p2 = &a;
    *p2 = 30;
    cout << *p2 << endl;

    // 3.const即修饰指针，又修饰指针--特点:指向和值都不可以修改
    const int* const p3 = &a;
    cout << *p3 << endl;


    system("pause");
}

```

### 指针和数组

作用：利用指针访问数组中的元素

```c++

#include <iostream>
using namespace std;

void main()
{
    int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
    cout << "第一个元素为：" << arr[0] << endl;

    int* p = arr; // arr就是数组首地址
    cout << "利用指针访问第一个元素：" << *p << endl;

    p++; // 向后移动4个字节，整形类型占用4个字节
    cout << "利用指针访问第二个元素：" << *p << endl;

    cout << "利用指针遍历数组" << endl;
    int* p2 = arr;
    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
    {
        //cout << arr[i] << endl;
        cout << *p2 << endl;
        p2++;
    }

    system("pause");
}

```

### 指针和函数

```c++

#include <iostream>
using namespace std;

// 值传递
void swap(int num1, int num2) {
	cout << "交换前num1" << num1 << endl;
	cout << "交换前num2" << num2 << endl;

	int temp = num1;
	num1 = num2;
	num2 = temp;

	cout << "交换后num1" << num1 << endl;
	cout << "交换后num2" << num2 << endl;
}

// 地址传递
void swap02(int *p1, int *p2) {
	int temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}

void main()
{
    // 1.值传递，值传递不可以修饰实参，也就是实参不会改变
	int a = 10;
	int b = 20;
	swap(a, b);
	// 2. 地址传递,如果是地址传递，可以修饰实参，实参会改变
	swap02(&a, &b);
    system("pause");
}

```

### 指针、数组、函数

案例描述：封装一个而函数，利用冒泡排序，实现对整型数组的升序排序
数组：`int arr[10] = {3,5,8,12,2,9,7,4,6,1}`

```c++

#include <iostream>
using namespace std;


// 冒泡排序函数
void bubbleSort(int* arr, int len) {
	for (int i = 0; i < len - 1; i++)
	{
		for (int j = 0; j < len - 1 -i; j++)
		{
			if (arr[j] > arr[j + 1]) {
				// 交换位置
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

// 打印数组
void printArray(int *arr, int len) {
	for (int i = 0; i < len; i++)
	{
		cout << arr[i] << endl;
	}
}

void main()
{
	// 创建一个数组
    int arr[10] = { 3,5,8,12,2,9,7,4,6,1 };
	// 数组长度
	int len = sizeof(arr) / sizeof(arr[0]);
	// 传入排序函数
	bubbleSort(arr, len);

	// 打印数组
	printArray(arr, len);

    system("pause");
}

```

## 结构体

### 结构体基本概念

结构体属于用户自定的数据类型，允许用户存储不同的数据类型

### 结构体定义和使用

语法：`struce 结构体名 {结构体成员列表};`<br>
通过结构体创建变量的三种方式：

- struce 结构体名 变量名
- struce 结构体名 变量名 = {成员 1 值,成员 2 值,....}
- 定义结构体时顺便创建变量

```c++

#include <iostream>
using namespace std;
#include <string>

// 结构体定义---出昂见学生类型
struct Student
{
	// 成员列表
	string name; // 姓名
	int age; // 年龄
	int score; // 分数
};


// /3.在定义结构体时顺便创建结构体变量
struct Student_s3
{
	// 成员列表
	string name; // 姓名
	int age; // 年龄
	int score; // 分数
}s3;



void main()
{
	// 通过学生类型创建具体学生
	//1. struct Student s1
	struct Student s1;
	// 给s1属性赋值，通过.访问结构体变量中的属性
	s1.name = "张三";
	s1.age = 24;
	s1.score = 100;
	cout << "姓名：" << s1.name << "年龄：" << s1.age << "分数：" << s1.score << endl;

	//2. struct Student s2 = {....}
	struct Student s2 = {
		"李四",
		19,
		80
	};
	cout << "姓名：" << s2.name << "年龄：" << s2.age << "分数：" << s2.score << endl;

	//3.在定义结构体时顺便创建结构体变量
	s3.name = "王五";
	s3.age = 18;
	s3.score = 90;
	cout << "姓名：" << s3.name << "年龄：" << s3.age << "分数：" << s3.score << endl;
    system("pause");
}

```

### 结构体数组

作用:将自定义的结构体放到数组中方便维护<br>
语法：`struct 结构体名 数组名[元素个数] = {{}, {}, ......{}}`

```c++

#include <iostream>
using namespace std;
#include <string>

// 结构体定义
struct student {
	string name;
	int age;
	int score;
};

void main()
{
	// 结构体数组
	struct student arr[3] = {
		{"张三",18,60},
		{"李四", 20, 100},
		{"王五", 19, 85}
	};
	// 修改结构体中的值
	arr[1].name = "麻子";
	// 遍历结构体
	int len = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < len; i++)
	{
		cout << "姓名：" << arr[i].name << "年龄：" << arr[i].age << "分数：" << arr[i].score << endl;
	}

    system("pause");
}

```

### 结构体指针

作用：通过指针访问结构体中的成员

- 利用操作符`->`可以通过结构体指针访问结构体属性

```c++

#include <iostream>
using namespace std;
#include <string>

// 结构体定义
struct student {
	string name;
	int age;
	int score;
};

void main()
{
	// 创建结构体变量
	struct student stu = {"张三",18,60};
	// 通过指针指向结构体变量
	struct student* p = &stu;
	// 通过指针访问结构体变量中的数据
	cout << "姓名：" << p->name << endl;
	cout << "年龄：" << p->age << endl;
	cout << "分数：" << p->score << endl;

    system("pause");
}

```

### 结构体嵌套结构体

作用：结构体中成员可以是另一个结构体<br>
例如：每个老师辅导一个学员，一个老师的结构体中，记录一个学生的结构体

```c++

#include <iostream>
using namespace std;
#include <string>

// 学生结构体定义
struct student {
	string name;
	int age;
	int score;
};

// 教师结构体定义
struct teacher {
	int id;
	string name;
	int age;
	struct student stu; // 子结构体学生
};

void main()
{
	// 创建结构体变量
	teacher t;
	t.id = 1000;
	t.name = "张勋";
	t.age = 45;
	t.stu.name = "张三";
	t.stu.age = 18;
	t.stu.score = 90;

	cout << "教师姓名：" << t.name << "教师编号：" << t.id << "教师年龄：" << t.age
		<< "学生姓名：" << t.stu.name << "学生年龄：" << t.stu.age
		<< "学生分数：" << t.stu.score << endl;

    system("pause");
}

```

### 结构体做函数参数

作用：将结构体作为参数想函数中传递<br>
传递方式有两种：值传递、地址传递

```c++

#include <iostream>
#include <string>

using namespace std;


//定义学生结构体
struct student {
	string name;
	int age;
	int score;
};

// 1.值传递
void printStudent1(struct student s) {
	cout << "子函数姓名：" << s.name << endl;
}

// 2.地址传递
void printStudent2(struct student *p) {
	cout << "子函数2姓名" << p->name << endl;
}

void main()
{

	// 结构体作为函数参数

	// 创建结构体变量
	struct student s;
	s.name = "张三";
	s.age = 20;
	s.score = 90;

	cout << "学生信息：姓名 " << s.name << "年龄：" << s.age << endl;
	// 值传递
	printStudent1(s);
	// 地址传递
	printStudent2(&s);

    system("pause");
}

```

### 结构体中 const 使用场景

作用：用 const 来防止误操作

```c++

#include <iostream>
#include <string>

using namespace std;


//定义学生结构体
struct student {
	string name;
	int age;
	int score;
};

//const 使用场景, 将函数中形参改为指针，可以减少内存空间，而且不会复制出来新的副本
void printStudent(const student *s) {
	// s->age = 150; 加入const之后，一旦有修改操作就会报错，可以防止我们的误操作
 	cout << "姓名：" << s->name << " 年龄：" << s->age << " 分数：" << s->score << endl;
}

void main()
{
	struct student s = { "张三", 20, 90 };
	// 通过函数体打印结构体变量信息
	printStudent(&s);

    system("pause");
}
```

### 结构体案例

案例描述 1：学校毕设项目，每个老师带 5 个学生，总共有 3 个老师，需求如下，设计学生和老师结构体。其中在老师结构体中，
有老师姓名和一个存放 5 名血色和功能的数组作为成员。学生成员有姓名、分数、创建数组存放 3 名老师，通过函数给每个老师
及所带的学生赋值，最终打印出老师数据及老师所带学生数据。

```c++

#include <iostream>
#include <string>
#include <ctime>
using namespace std;


// 按照指定范围生成随机分数
int Random(int start, int end) {

	int dis = end - start;
	return rand() % dis + start;

}

// 定义学生结构体
struct Student {
	string name;
	int score;
};
// 定义教师结构体
struct Teacher {
	string tName;
	// 学生数据
	struct Student sArray[5];
};

// 给教师和学生赋值的函数
void allocateSpace(struct Teacher tArray[], int len){

	string nameSeed = "ABCDE";
	// 给教师赋值
	for (int i = 0; i < len; i++)
	{
		tArray[i].tName = "Teacher_";
		tArray[i].tName += nameSeed[i];
		// 给学生赋值
		for (int j = 0; j < 5; j++)
		{
			tArray[i].sArray[j].name = "Student_";
			tArray[i].sArray[j].name += nameSeed[j];

			tArray[i].sArray[j].score = Random(60,100);
		}
	}
}

// 打印所有信息
void printAll(struct Teacher tArray[], int len) {
	for (int i = 0; i < len; i++)
	{
		cout << "教师姓名：" << tArray[i].tName << endl;
		for (int j = 0; j < 5; j++)
		{
			cout << "学生姓名：" << tArray[i].sArray[j].name << "学生分数" << tArray[i].sArray[j].score << endl;
		}
	}
}

void main()
{

	// 添加随机数种子
	srand((unsigned int)time(NULL));

	//创建3名教师数组
	struct Teacher tArray[3];
	//通过函数给3名教师的信息赋值，并给教师带的学生信息赋值
	int len = sizeof(tArray) / sizeof(tArray[0]);
	allocateSpace(tArray, len);
	//打印所有教师和学生信息
	printAll(tArray, len);


    system("pause");
}

```

案例描述 2：设计一个英雄的结构体，包含成员姓名、年龄、性别；创建结构体数组，数组中存放 5 名英雄。
通过冒泡排序算法，将数组中的英雄按照年龄进行升序排序，最终打印排序结果。

```c++

#include <iostream>
#include <string>
using namespace std;

// 定义英雄结构体
struct Hero {
	string name;
	int age;
	string sex;
};

// 冒泡升序
void sortBubbling(struct Hero heroArray[], int len) {
	for (int i = 0; i < len - 1; i++)
	{
		for (int j = 0; j < len - 1 - i; j++)
		{
			if (heroArray[j].age > heroArray[j + 1].age) {
				int temp = heroArray[j].age;
				heroArray[j].age = heroArray[j + 1].age;
				heroArray[j + 1].age = temp;
			}
		}
	}
}

// 打印所有信息
void printinfo(struct Hero heroArray[], int len) {
	for (int i = 0; i < len; i++)
	{
		cout << "英雄："<< heroArray[i].name << "年龄："<< heroArray[i].age << "性别："<< heroArray[i].sex  << endl;
	}
}

void main()
{
	// 设计英雄结构体
	struct Hero heroArray[5] = {
		{"李应", 30, "男"},
		{"卢俊义", 26, "男"},
		{"朱仝",32,"男"},
		{"穆弘",25,"男"},
		{"穆春",20,"男"},
	};

	int len = sizeof(heroArray) / sizeof(heroArray[0]);
	// 冒泡排序：英雄按照年龄进行升序排序
	sortBubbling(heroArray, len);
	// 打印结果
	printinfo(heroArray, len);

    system("pause");
}

```

# 通讯录管理系统

- 添加联系人：向通讯录中添加新人，信息包括（姓名、性别、年龄、联系电话、家庭住址）最多添加 1000 人
- 显示联系人：显示通讯录中所有联系人
- 删除联系人：按照姓名进行删除指定联系人
- 查找联系人：按照姓名查看指定联系人信息
- 修改联系人：按照姓名重新修改指定联系人
- 清空联系人：清空通讯录中所有信息
- 退出联系人：退出当前使用的通讯录

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 显示提示
void shwoMenu() {
	cout << "*************通讯录系统**************" << endl;
	cout << "***********   1、添加   *************" << endl;
	cout << "***********   2、显示   *************" << endl;
	cout << "***********   3、删除   *************" << endl;
	cout << "***********   4、查找   *************" << endl;
	cout << "***********   5、修改   *************" << endl;
	cout << "***********   6、清空   *************" << endl;
	cout << "***********   0、退出   *************" << endl;
	cout << "*************************************" << endl;
}

// 联系人结构体
struct Person
{
	string name;
	int sex;
	int age;
	string phone;
	string address;
};

// 通讯录结构体
#define MAX 1000 // 最大人数
struct Addressbooks {
	struct Person personArray[MAX]; // 通讯录中保存的联系人数组
	int m_Size; // 通讯录中人员个数
};

// 添加通讯录:向通讯录中添加新人，信息包括（姓名、性别、年龄、联系电话、家庭住址）最多添加 1000 人
void addPerson(Addressbooks *abs) {
	// 判断通讯录是否已满，满了提示用户不添加
	if (abs->m_Size == MAX) {
		cout << "通讯录已满，无法添加" << endl;
		return;
	}
	else {
		// 添加具体联系人
		// 名字
		string in_name;
		cout << "请输入姓名:" << endl;
		cin >> in_name;
		abs->personArray[abs->m_Size].name = in_name;
		// 性别
		cout << "请输入性别：" << endl;
		cout << "1----男" << endl;
		cout << "2----女" << endl;
		int in_sex = 0;
		while (true)
		{
			cin >> in_sex;
			if (in_sex == 1 || in_sex == 2) {
				abs->personArray[abs->m_Size].sex = in_sex;
				break;
			}
			cout << "输入有误，请重新输入" << endl;
		}

		// 年龄
		int in_age;
		cout << "请输入年龄：" << endl;
		cin >> in_age;
		abs->personArray[abs->m_Size].age = in_age;
		// 手机号
		string in_phone;
		cout << "请输入手机号：" << endl;
		cin >> in_phone;
		abs->personArray[abs->m_Size].phone = in_phone;
		// 地址
		string in_address;
		cout << "请输入地址：" << endl;
		cin >> in_address;
		abs->personArray[abs->m_Size].address = in_address;

		// 更新通讯录人数
		abs->m_Size++;
		cout << "添加成功！！" << endl;

		system("pause"); // 请按任意键继续
		system("cls"); // 请屏操作
	}
}

// 查看联系人:显示通讯录中所有联系人
void showPerson(Addressbooks* abs) {
	if (abs->m_Size == 0) {
		cout << "当前记录为空" << endl;
	} else {
		for (int i = 0; i < abs->m_Size; i++)
		{
			cout << "姓名：" << abs->personArray[i].name << "\t";
			cout << "性别：" << (abs->personArray[i].sex == 1 ? "男" : "女") << "\t";
			cout << "年龄：" << abs->personArray[i].age << "\t";
			cout << "手机号：" << abs->personArray[i].phone << "\t";
			cout << "地址：" << abs->personArray[i].address << endl;
		}
	}
	system("pause"); // 请按任意键继续
	system("cls"); // 请屏操作
}

// 检测联系人是否存在:如果存在返回联系人所在位置，不存在返回-1
int isExist(Addressbooks * abs, string name) {
	return 0;
	for (int i = 0; i < abs->m_Size; i++)
	{
		if (abs->personArray[i].name == name) {
			return i;
		}
		else {
			return -1;
		}
	}
}

// 删除:按照姓名进行删除指定联系人
void deletePerson(Addressbooks *abs) {

	if (abs->m_Size == 0) {
		cout << "当前记录为空,没有删除项别" << endl;
	}
	else {
		string user_input_name;
		cout << "请输入删除联系人姓名：" << endl;
		cin >> user_input_name;
		int result = isExist(abs, user_input_name);
		if (result != -1) {
			// 删除
			for (int i = result; i < abs->m_Size; i++)
			{
				// 数据前移
				abs->personArray[i] = abs->personArray[i + 1];
			}
			abs->m_Size--; // 更新通讯录中人员数量
			// 提示删除成功并更新通讯录总数
			cout << "删除成功！！！" << endl;
		}
		else {
			cout << "删除人员不存在，请确认输入联系人姓名是否正确" << endl;
		}

	}
	system("pause"); // 请按任意键继续
	system("cls"); // 请屏操作
}

//查找：按照姓名查看指定联系人信息
void lookupPerson(Addressbooks* abs) {
	if (abs->m_Size == 0) {
		cout << "通讯录为空，暂不支持查找" << endl;
	}
	else {
		string look_user;
		cout << "请输入需要查找的联系人姓名" << endl;
		cin >> look_user;
		int result = isExist(abs, look_user);
		if (result != -1) {
			cout << "-------------------查询人员信息-------------------" << endl;
			cout << "姓名：" << abs->personArray[result].name << "\t";
			cout << "性别：" << (abs->personArray[result].sex == 1 ? "男" : "女") << "\t";
			cout << "年龄：" << abs->personArray[result].age << "\t";
			cout << "手机号：" << abs->personArray[result].phone << "\t";
			cout << "地址：" << abs->personArray[result].address << endl;
		}
		else {
			cout << "查询人员不存在，请确认输入联系人姓名是否正确" << endl;
		}
	}
	system("pause"); // 请按任意键继续
	system("cls"); // 请屏操作

}

// 修改：按照姓名重新修改指定联系人
void modifyPerson(Addressbooks *abs) {
	if (abs->m_Size == 0) {
		cout << "通讯录为空，暂不支持修改" << endl;
	}
	else {
		string modify_user;
		cout << "请输入需要重新修改的联系人姓名" << endl;
		cin >> modify_user;
		int result = isExist(abs, modify_user);
		if (result != -1) {

			string in_name;
			cout << "请输入姓名:" << endl;
			cin >> in_name;
			abs->personArray[result].name = in_name;

			// 性别
			cout << "请输入性别：" << endl;
			cout << "1----男" << endl;
			cout << "2----女" << endl;
			int in_sex = 0;
			while (true)
			{
				cin >> in_sex;
				if (in_sex == 1 || in_sex == 2) {
					abs->personArray[result].sex = in_sex;
					break;
				}
				cout << "输入有误，请重新输入" << endl;
			}

			// 年龄
			int in_age;
			cout << "请输入年龄：" << endl;
			cin >> in_age;
			abs->personArray[result].age = in_age;
			// 手机号
			string in_phone;
			cout << "请输入手机号：" << endl;
			cin >> in_phone;
			abs->personArray[result].phone = in_phone;
			// 地址
			string in_address;
			cout << "请输入地址：" << endl;
			cin >> in_address;
			abs->personArray[result].address = in_address;

			cout << "联系人信息修改成功" << endl;
		}
		else {
			cout << "修改人员信息不存在，请确认输入联系人姓名是否正确" << endl;
		}
	}

	system("pause"); // 请按任意键继续
	system("cls"); // 请屏操作
}

//清空:清空通讯录中所有信息
void emptyAllPerson(Addressbooks* abs) {
	abs->m_Size = 0;
	cout << "通讯录已清空" << endl;
	system("pause"); // 请按任意键继续
	system("cls"); // 请屏操作
}

int main()
{
	// 创建通讯录结构体变量
	Addressbooks abs;
	// 初始化通讯录中当前人员个数
	abs.m_Size = 0;

	int select = 0; // 创建用户选择输入变量
	while (true)
	{
		// 显示提示
		shwoMenu();

		cin >> select;
		switch (select)
		{
		case 1: // 添加
			addPerson(&abs);
			break;
		case 2: // 显示
			showPerson(&abs);
			break;
		case 3: // 删除
			deletePerson(&abs);
			break;
		case 4: // 查找
			lookupPerson(&abs);
			break;
		case 5: // 修改
			modifyPerson(&abs);
			break;
		case 6: // 清空
			emptyAllPerson(&abs);
			break;
		case 0: // 退出
			cout << "欢迎下次使用" << endl;
			system("pause");
			return 0;
			break;
		default:
			break;
		}
	}
	return 0;
    system("pause");
}

```

# 核心编程

针对 C++面向对象

## 内存分区模型

C++程序执行时，将内存大致划分为 4 个区域：

- 代码区：存放函数体的二进制代码，由操作系统进行管理的
- 全局区：存放全局变量和静态变量以及常量
- 栈区：由编译器自动分配释放，存放函数的参数值，局部变量等
- 堆区：由程序员分配和释放，若程序员不释放，程序结束时由操作系统回收

内存四区意义：<br>
不同区域存放的数据，赋予不同的生命周期，给哦我们最大的灵活编程

### 程序运行前

程序编译后，生成**exe**可执行程序，未执行该程序前分为两个区域<br>
**代码区：**

- 存放 CPU 执行的机器命令
- 代码区是**共享**的，共享的目的是对繁琐被执行的程序，只需要在内存中有一份代码即可
- 代码区是**只读**的，使其只读的原因是防止程序意外的修改它的指令

  **全局区：**

- 全局变量和静态变量存放在此
- 全局区还包含了常量区，字符串常量和其他常量也存放在此
- 该区域的数据在程序结束后由操作系统释放

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 全局变量
int g_a = 20;
int g_b = 20;

// const 修饰的全局变量，全局常量
const int g_c = 30;

int main()
{
	//全局区：全局变量、静态变量、常量
	// 创建普通局部变量
	int a = 10;
	int b = 10;

	cout << "局部变量a的地址为：" << (int)&a << endl;
	cout << "局部变量a的地址为：" << (int)&b << endl;

	cout << "全局变量a的地址为：" << (int)&g_a << endl;
	cout << "全局变量a的地址为：" << (int)&g_b << endl;

	// 静态变量 在普通变量前面加 static 属于静态变量
	static int s_a = 10;
	static int s_b = 10;
	cout << "静态变量s_a的地址为：" << (int)&s_a << endl;
	cout << "静态变量s_b的地址为：" << (int)&s_b << endl;

	// 常量
	// 字符串常量
	cout << "字符串常量地址：" << (int)&"hello world" << endl;
	//cosnt 修饰的变量
	//const 修饰的全局变量，const 修饰的局部变量
	cout << "const 修饰的全局变量，全局常量" << (int)&g_c << endl;



	return 0;
    system("pause");
}

```

总结：

- C++中在程序运行前分为全局区和代码区
- 代码区特点是共享和只读
- 全局区中存放全局变量、静态变量、常量
- 常量区中存放 cosnt 修饰的全局变量和字符串常量

### 程序运行后

**栈区：**
由编译器自动分配释放，存放函数的参数值，局部变量等<br>
注意事项：不要返回局部变量的地址，栈区开辟的数据由编译器自动释放

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 栈区数据注意事项----不要返回局部变量的地址
// 栈区数据由编译器管理开辟和释放
int* func(int b) { // 形参数据也会放在栈上
	b = 100;
	int a = 10; // 局部变量 存放在栈区，栈区的数据在函数执行完成后自动释放
	return &a; // 返回局部变量地址
}

int main()
{

	int *p = func(1);
	cout << *p << endl;
	cout << *p << endl;

    system("pause");
	return 0;
}

```

**堆区：**

- 由程序员分配和释放，若程序员不释放，程序结束时由操作系统回收
- 在 C++中主要利用 new 在堆区开辟内存

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int* functio() {
	//利用new关键字，可以将数据开辟到堆区
	int* a = new int(10);
	return a;
}

int main()
{
	int *p = functio();
	cout << *p << endl;
    system("pause");
	return 0;
}

```

### new 操作符

- C++中利用 `new` 操作符在堆区开辟数据
- 堆区开辟的数据，由程序员手动开辟，手动释放，释放利用操作符 `delete`
- 语法：`new 数据类型`
- 利用 new 创建的数据，会返回该数据对应的类型的指针

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int* functio() {
	// new 返回是该数据类型的指针
	int* a = new int(10);
	return a;
}

// 1.new 的基本语法
void test01() {
	int* p = functio();
	cout << *p << endl;
	//堆区开辟的数据，由程序员手动开辟，手动释放，释放利用操作符 `delete`
	delete p;

}
// 2.利用new关键字开辟数组
void test02() {
	// 创建10整形数组，在堆区
	int * arr = new int[10];
	for (int i = 0; i < 10; i++)
	{
		arr[i] = i + 100;
	}

	// 打印数据
	for (int i = 0; i < 10; i++)
	{
		cout << arr[i] << endl;
	}
	// 释放堆区数组,释放数组时需要添加[]
	delete[] arr;
}

int main()
{
	test01();

	test02();

    system("pause");
	return 0;
}

```

## 引用

### 引用的基本使用

作用：给变量起别名<br>
语法：`数据类型 &别名 = 原名`

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;


int main()
{
	int a = 10;
	int& b = a;

	cout << "a=" << a << endl;
	cout << "b=" << b << endl;

	b = 100;
	cout << "a=" << a << endl;
	cout << "b=" << b << endl;

    system("pause");
	return 0;
}

```

### 引用注意事项

- 引用必须初始化
- 引用初始化后，不可以改变

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;


int main()
{
	//1.引用必须初始化

	int a = 10;
	int& b = a;
	//2.引用在初始化后，不可以修改
	int c = 20;
	b = c; // c的值并没有赋值给b

	cout << "a=" << a << endl;
	cout << "b=" << b << endl;

	b = 100;
	cout << "a=" << a << endl;
	cout << "b=" << b << endl;

    system("pause");
	return 0;
}

```

### 引用做函数参数

作用：函数传参时，可以利用引用的技术让形参修饰实参<br>
优点：可以简化指针修改实参

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 1.值传递
void mySwap01(int a, int b) {
	int temp = a;
	a = b;
	b = temp;

	cout << "mySwap01 a= " << a << endl;
	cout << "mySwap01 b= " << b << endl;
}

// 2.地址传递
void mySwap02(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

// 3.引用传递
void mySwap03(int &a, int &b) {
	int temp = a;
	a = b;
	b = temp;
}

int main()
{

	int a = 10;
	int b = 20;
	// 值传递, 形参不会修饰实参
	mySwap01(a,b);
	cout << "a= " << a << endl;
	cout << "b= " << b << endl;

	// 地址传递,形参可以修饰实参
	mySwap02(&a,&b);

	// 引用传递,形参也可以修饰实参
	mySwap03(a,b);


    system("pause");
	return 0;
}

```

### 引用做函数返回值

- 作用：引用是可以作为函数的返回值存在的
- 注意：不要返回局部变量引用
- 用法：函数调用作为左值

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 1.不用返回局部变量的应用
int& test01() {
	int a = 10; // 局部变量存放在四区中的栈区
	return a;
}

// 2.函数的调用可以作为左值
int& test02() {
	static int a = 10; // 静态变量，存放在全局区，全局区在程序结束后系统释放
	return a;
}

int main()
{

	int& ref = test01();
	cout << "ref=" << ref << endl; // 第一次结果正确，因为编译器做了保留
	cout << "ref=" << ref << endl; // 第二次结果错误，a的内存已经释放


	int& ref2 = test02();
	cout << "ref2=" << ref2 << endl;
	cout << "ref2=" << ref2 << endl;
	//函数的调用可以作为左值
	test02() = 1000; // 如果函数的返回值是引用，这个函数调用可以作为左值

	cout << "ref2=" << ref2 << endl;
	cout << "ref2=" << ref2 << endl;

    system("pause");
	return 0;
}

```

### 引用的本质

本质：引用的本质在 C++内部实现是一个指针常量，引用一旦初始化就不可以发生改变

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 发现是引用，转换为 int* const ref = &a;
void func(int& ref) {
	ref = 100; // ref是引用，转换为*ref = 100
}

int main()
{
	int a = 10;
	// 自动转换为 int* const ref = &a; 指针常量是指针指向不可改，也说明为什么引用不可更改
	int& ref = a;
	ref = 20;// 内部发现 ref 是引用，自动帮我们转换为： *ref = 20;

	cout << "a:" << a << endl;
	cout << "ref:" << ref << endl;

	func(a);

  system("pause");
	return 0;
}

```

### 常量引用

作用：常量引用主要用来修饰形参，防止误操作。在函数形参列表中，可以加 const 修饰形参，防止形参改变实参

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 打印函数数据 const 修饰形参，不允许修改
void showValue(const int& val) {
	//val = 100;
	cout << "val=" << val << endl;
}

int main()
{
	// 引用使用的场景，通常用来修饰形参，防止误操作
	int a = 10;
	// 加上 cosnt 之后，编译器将代码修改 int temp = 10; const int &ref = temp;
	const int& ref = 10; // 引用必须引一块合法的内存空间

	int b = 10;
	showValue(b);
	cout << "b=" << b << endl;

    system("pause");
	return 0;
}

```

## 函数提高

### 函数默认参数

在 C++中，函数的形参列表中的参数可以设置默认值。语法`返回值类型 函数名(参数 = 默认值) {}`<br>
**注意事项**

- 1.如果某个位置已经存在了默认参数，那么从这个位置往后，从左到右都必须有默认值
- 2.如果函数声明有默认参数，函数实现就不能够有默认参数

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 如果某个位置已经存在了默认参数，那么从这个位置往后，从左到右都必须有默认值
int pushFunction(int a, int b = 20, int c = 10) {
	return a + b + c;
}

// 如果函数声明有默认参数，函数实现就不能够有默认参数
// 声明和实现只能有一个设置默认参数，否则编译器报错
int functTwo(int a = 10, int b = 10);
int functTwo(int a, int b) {
	return a + b;
}

int main()
{
	int a = 20;
	int result = pushFunction(a);
	cout << result << endl;


	int reslut_a = functTwo();
	cout << reslut_a << endl;

    system("pause");
	return 0;
}

```

### 函数展位参数

- C++中函数的形参列表中可以有占位参数，用来占位，调用函数时必须填补该位置。
- 语法：`返回值类型 函数名(数据类型){}`

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 占位参数  --- 返回值类型 函数名(数据类型){}
// 目前阶段的占位参数，用不到，占位参数还可以设置默认值
void func(int a, int = 10) {
	cout << "this is function" << endl;
}

int main()
{
	func(20, 10);

    system("pause");
	return 0;
}

```

### 函数重载

#### 函数重载描述

作用：函数名可以相同，提高复用<br>
**函数重载满足条件：**

- 同一个作用域下
- 函数名称相同
- 函数**类型不同**，或者**个数不同**或者**顺序不同**

注意：函数的返回值不可以作为函数重载的条件

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 函数重载，提高复用。函数重载需要函数都在一个作用域下

//函数重载满足条件：
//同一个作用域下
//函数名称相同
//函数 类型不同，或者 个数不同  或者  顺序不同
//存在返回值的函数，不允许进行函数重载
void func() {
	cout << "func 的调用" << endl;
}

void func(int a) {
	cout << "func（int a）的调用" << endl;
}

// 传递参数顺序不同
void func(int a, double b) {
	cout << "func（int a, double b）的调用" << endl;
}

void func(double b, int a) {
	cout << "func（int a, double b）的调用" << endl;
}




int main()
{
	func();
	func(10);

    system("pause");
	return 0;
}

```

#### 函数重载注意事项

- 引用作为函数重载条件
- 函数重载碰到函数默认参数

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

///////////////引用作为函数重载条件/////////////////
void func(int &a) {
	cout << "func（int &a）的调用=" << &a << endl;
}
void func(const int& a) {
	cout << "func（const int& a）的调用" << endl;
}

///////////////函数重载碰到函数默认参数/////////////////
void func2(int a, int b = 10) {
	cout << "func2(int a)的调用" << endl;
}

void func2(int a) {
	cout << "func2(int a)的调用" << endl;
}

int main()
{


	// 调用 void func(int &a)
	/*int a = 10;
	func(a);*/

	// 调用 void func(const int& a)
	//func(10);

	//func2(10); // 当函数重载遇到默认参数，出现二义行，报错，尽量避免

    system("pause");
	return 0;
}

```

## 类和对象

C++面向对象的三大特性为：封装、多态、继承<br>
C++认为万事万物皆为对象，对象存在其属性和行为<br>
例如：人可以作为对象，属性有姓名、年龄、身高、体重，...行为有走、跑、跳、吃饭等
具有相同性质的对象，我们可以抽象为类

### 封装

#### 封装的意义

封装是 C++面向对象三大特性之一<br>
封装的意义：

- 将属性和行为作为一个整体，表现生活中的事务
- 将属性和行为加以权限控制

**封装第一层含义：**
在设计类的时候，属性和行为写在一起，表现事务
**语法：**`class 类名{访问权限:属性/行为};`<br>
**示例 1：**设置一个圆类，求圆的周长

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 设置一个圆类，求圆周长
// 圆周长公式：2 * PI * 半径

//圆周率
const double PI = 3.1415926;

//class 代表设计一个类，类后面跟着类名称
class Circle {
	// 访问权限
	// 公共权限
public:
	// 属性：半径
	int m_r;

	// 行为：获取圆的周长
	double calculateZC() {
		return 2 * PI * m_r;
	}
};

int main()
{
	// 同通过圆类 创建具体的圆（对象）
	Circle c1;
	// 给圆对象的属性进行赋值
	c1.m_r = 10;

	cout << "圆周长=====" << c1.calculateZC() << endl;

    system("pause");
	return 0;
}

```

**示例 2：**设置一个学生类，属性有姓名、学号、可以给姓名和学号赋值。可以显示学生姓名和学号

```c++

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

// 设置一个学生类，属性有姓名、学号、可以给姓名和学号赋值。可以显示学生姓名和学号

// 类中属性和行为我们统一称为成员
//属性 成员属性 成员变量
//行为 成员函数 成员方法

//class 代表设计一个类，类后面跟着类名称
class Student {
	// 访问权限:公共权限
	public:
	// 属性：姓名、学号
	string m_Name;
	int m_Id;

	// 行为：获取学生学号和姓名
	void showStudent() {
		cout << "名字:" << m_Name << " 学号：" << m_Id << endl;;
	}

	// 给姓名赋值
	void setName(string name) {
		m_Name = name;
	}
	// 给学号赋值
	void setId(int id) {
		m_Id = id;
	}
};

int main()
{
	// 同通过学生类 创建具体的学生（对象）
	Student s1;
	// 给学生对象的属性进行赋值
	s1.setName("李四");
	int id = 2016084512;
	s1.setId(id);

	s1.showStudent();

    system("pause");
	return 0;
}

```

**封装第二层含义：**<br>
类在设计时，可以八属性和行为放在不同权限下，加以控制<br>
访问权限有三种：

- pubilc 公共权限 类内可访问 类外可访问
- protected 保护权限 类内可访问 类外不可以访问
- private 私有权限 类内可访问 类外不可访问

```c++
#include <iostream>
using namespace std;

/*
	pubilc 公共权限 类内可访问 类外可访问
	protected 保护权限 类内可访问 类外不可以访问 子类可以访问父类中的保护内容
	private 私有权限 类内可访问 类外不可访问 子类不可以访问父类中的私有内容
*/

class Person {
public:
	// 公共权限
	string m_Name; // 姓名

protected:
	// 保护权限
	string m_Car; // 汽车
private:
	// 私有权限
	int m_Password; // 银行卡密码
public:
	void fuinc() {
		m_Name = "张三";
		m_Car = "奔驰";
		m_Password = 457787;
	}
};

int main()
{
	// 实例化具体对象
	Person p1;
	p1.m_Name = "姓名";
	//p1.m_Car = "44545" // 保护权限内容，在类外不可访问
	//p1.m_Password = 451274 // 私有权限，在类外不可访问

	cout << "=======" << endl;
}
```

#### struct 和 class 区别

C++中`struct`和`class`唯一区别就在于默认的访问权限不同<br>
区别：

- struct 默认权限为公共
- class 默认权限为私有

```c++
#include <iostream>
using namespace std;

struct C1 {
	int m_A; // 默认是公有权限
};

class C2 {
	int m_A; // 默认是私有权限
};

int main()
{
	C1 c1;
	c1.m_A = 100;

	C2 c2;
	//c2.m_A = 100; // c2 为 class 创建类默认为私有类外不可访问
}

```

#### 成员属性设置为私有

优点：

- 将所有成员属性设置为私有，可以自己控制读写权限
- 对于写权限，我们可以检测数据的有效性

```c++
#include <iostream>
using namespace std;
#include <string>
// 成员属性设置为私有
//1.可以自己控制读写权限
//2.对于写可以检测数据的有效性

class Person {
public:
	// 写姓名
	void setName(string name) {
		m_Name = name;
	}
	// 读姓名
	string getName() {
		return m_Name;
	}


	// 设置年龄 可读可写，如果修改年龄范围必须在0~150之间
	void setAge(int age) {
		if (age < 0 || age > 150) {
			return;
		}
		m_Age = age;
	}

	// 获取年龄
	int getAge() {
		//m_Age = 0; // 初始化为0岁
		return m_Age;
	}

	// 设置喜好
	void setLover(string lover) {
		m_Lover = lover;
	}

private:
	string m_Name; // 可读可写
	int m_Age; // 只读
	string m_Lover; // 只写
};

int main()
{

	Person p;
	p.setName("张三");
	cout << "姓名为：" << p.getName() << endl;

	p.setAge(1100);
	cout << "年龄为：" << p.getAge() << endl;

	// 只写，只能在class内进行访问到
	p.setLover("喜欢汽车");


	//p.m_Name = "张三";

	return 0;
}

```

练习案例 1：设计立方体类（Cube）求出立方体面积和体积，分别用全局函数和成员函数判断两个立方体是否相等

```c++
#include <iostream>
using namespace std;
#include <string>

// 设计立方体类（Cube）求出立方体面积和体积，分别用全局函数和成员函数判断两个立方体是否相等
class Cube {
	// 行为 获取里立方体面积和体积
public:
	// 设置长
	void setL(int l) {
		m_L = l;
	}
	// 获取长
	int getL() {
		return m_L;
	}

	// 设置宽
	void setW(int w) {
		m_W = w;
	}
	// 获取宽
	int getW() {
		return m_W;
	}

	// 设置高
	void setH(int h) {
		m_H = h;
	}

	// 获取高
	int getH() {
		return m_H;
	}

	// 获取立方体面积
	int calculateS() {
		return 2 * m_L * m_W + 2 * m_W * m_H + 2 * m_L * m_H;
	}

	// 获取立方体体积
	int calculateV() {
		return m_L* m_W* m_H;
	}

	// 利用成员函数判断两个立方体是否相等
	bool isSameByClass(Cube &c) {
		if (getL() == c.getL() && getW() == c.getW() && getH() == c.getH()) {
			return true;
		}
		return false;
	}

	// 属性
private:
	int m_L;
	int m_W;
	int m_H;


};


// 利用全局函数判断两个立方体是否相等 ,采用值传递方式传递参数
bool isSame(Cube &c1, Cube &c2) {
	if (c1.getL() == c2.getL() && c1.getW() == c2.getW() && c1.getH() == c2.getH()) {
		return true;
	}
	return false;
}

int main()
{

	// 创建立方体对象
	Cube c1;
	c1.setL(10);
	c1.setW(10);
	c1.setH(10);

	cout << "C1的面积为：" << c1.calculateS() << endl;
	cout << "C1的体积为：" << c1.calculateV() << endl;


	Cube c2;
	c2.setL(10);
	c2.setW(10);
	c2.setH(11);

	cout << "C2的面积为：" << c2.calculateS() << endl;
	cout << "C2的体积为：" << c2.calculateV() << endl;
	// 利用全局函数判断立方体是否相等
	bool result = isSame(c1, c2);
	if (result) {
		cout << "全局函数==c1和c2是相等的" << endl;
	}
	else {
		cout << "全局函数==c1和c2不相等" << endl;
	}

	// 利用成员函数判断立方体是否相等
	bool ret = c1.isSameByClass(c2);
	if (ret) {
		cout << "类成员函数==c1和c2是相等的" << endl;
	}
	else {
		cout << "类成员函数==c1和c2不相等" << endl;
	}


	system("pause");
	return 0;
}

```

练习案例 2：点和圆的关系，设计一个圆形类（Circle）和一个点类（Point）计算点和圆的关系。

```c++

```

### 对象的初始化和清理

```c++

```

### C++对象模型和 this 指针

```c++

```

### 友元

```c++

```

### 运算符重载

```c++

```

### 继承

```c++

```

### 多态

```c++

```

## 文件操作

```c++

```
