:::tip
注意所有 C++ 源文件使用 Visual Studio 2019 编辑器打开否则会出现中文注释乱码
:::

## 环境搭建

### 环境介绍

- 操作系统 windows11
- visualStudio 2019 下载
- [opencv4.5](https://github.com/opencv/opencv/releases/tag/4.5.4) 和 对应文档下载

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_11-23-57.png)

下载后安装,安装完成共有两个文件夹，一个是编译后的一个是源码

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_11-29-36.png)

### 环境配置

`D:\localProgram\opencv\opencv\build\x64\vc15\bin` 安装目录文件路径，配置到电脑环境变量，然后重启项目

### 使用 vs 创建一个空项目和项目配置

#### 创建完成项目如果编译器是 x86 改成如下

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-12-02.png)
然后为项目添加目录和 opencv 库做为依赖，点击项目，选择属性

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-12-53.png)

#### 分别添加 添加生成目录，添加库目录 如下：

- 添加生成目录 D:\localProgram\opencv\opencv\build\include

- 添加库目录 D:\localProgram\opencv\opencv\build\x64\vc15\lib

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-13-44.png)

#### 配置附加依赖项：

在 `D:\localProgram\opencv\opencv\build\x64\vc15\lib` 下的文件夹中 `opencv_world454d.lib`文件用于调试。 `opencv_world454.lib`文件用于正式发布，在属性如图中进行配置。
**注：两个文件的区别在于是否带 d，带 d 的用于调试环境，不带 d 用于发布环境。vs 编辑器 2017 以前使用 vc14。vs2017 以后版本编辑器使用 vc15**

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-15-01.png)

### opencv_contrib 模块

为人脸模型训练模块在 opencv_contrib 模块中，因此想要训练人脸模型必须使用 opencv_contrib 模块，opencv_contrib 模块版本必须与 opencv 库版本相对应
人脸模型训练模块 [https://github.com/opencv/opencv_contrib/tags](https://github.com/opencv/opencv_contrib/tags)

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-16-35.png)

Cmaker 编译工具 [https://cmake.org/download/](https://cmake.org/download/)

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-19-44.png)
Cmaker 编译工具 下载后解压找到目录 `D:\download\tool\cmake-3.22.1-windows-x86_64\bin` 运行 `.exe` 程序选择 `opencv` 源码目录，**点击 configure** 按钮

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-20-31.png)

编译报错，无法从远程下载文件，，重新点击 confifure 按钮
在 CMake 中，Where is the source code 添加 opencv 文件夹，即`/Opencv/opencv-4.5.4`（注意：如果`/Opencv/opencv-4.5.4` 下还有一层文件夹 `opencv-4.5.4`，请用`/Opencv/opencv-4.5.4/opencv-4.5.4`）；

在 CMake 中，Where to build the binaries 添加 build 文件夹，即`/Opencv/build`

点击 configure，选择你使用的 `Visual Studio` 版本，选择平台（一般是 x64）

注意：如果这里版本选错，点击 `File > DeleteCache`。再点击 `Configure` 重新选择。

点击 finish；

再次点击 configure，等待（过程中有文件下载，请先搭好梯子）

完成后，在编译选项中进行勾选。BUILD_CUDA_STUBS、OPENCV_DNN_CUDA、WITH_CUDA、OPENCV_ENABLE_NONFREE、build_opencv_world 打勾

找到编译选项 OPENCV_EXTRA_MODULES_PATH ，将 Value 设置为`/Opencv/opencv_contrib-4.5.4/modules`（即`/Opencv/opencv_contrib-4.5.4` 中的 `modules` 目录，注意中间有没有多一层文件夹）；

点击 configure，等待至出现 configuring done；

注：若报错，先查报错并修改后重试；若多次重试，仍报同一个错误，建议新建 build1。修改 Where to build the binaries 为`/Opencv/build1`；点击 `File > DeleteCache`。再点击 Configure 重新开始。

点击 generate，等待至出现 generating done；

点击 open project。

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-26-31.png)
对照报错和日志文件可以看出，日志中提示我们手动下载缺失文件

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-25-27.png)
手动下载后修改文件命名，MD5 值+'-'+下载的文件名。比如 `opencv_videoio_ffmpeg_64.dll` 这个文件下载后凡在`.cache` 下的 `ffmpeg` 下，然后改名为 `b8120c07962d591e2e9071a1bf566fd0-opencv_videoio_ffmpeg_64.dll` 替换原来 `0KB` 的那个文件

`ffmpeg_version.cmake` 文件访问后，另存为 `txt` 文本，然后修改文件名称和后缀。

![](https://gitee.com/zmmlet/study-note/raw/master/images/Snipaste_2022-10-08_15-24-42.png)
**下载失败文件搞定后，重新点击 configure，等待至出现 configuring done**

## 程序和测试

opencv 学习 Demo 代码下载 [https://gitee.com/zmmlet/study-note/tree/master/C++/opencv](https://gitee.com/zmmlet/study-note/tree/master/C++/opencv)
