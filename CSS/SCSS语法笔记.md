# SCSS 语法

[SCSS](https://www.sass.hk/) 是 CSS 的一种扩展语言，具有良好的复用性和动态性

## 变量声明

```scss
$primary-color: #f5222d;
$font-color: #ecac4a;
$primary-border: 1px solid $primary-color;
```

## 嵌套

```scss
.nav-box {
  width: 5rem;
  height: 5rem;
  padding: 1.25rem;
  border: $primary-border;

  span {
    // 属性嵌套 相当于font-weight: 700;font-size: 1.25rem;
    font: {
      weight: 700;
      size: 1.25rem;
    }

    color: $font-color;

    // &调用父选择器
    &:hover {
      color: yellow;
    }
  }

  // 调用父选择器，相当于.nav-box .nav-box-text
  & &-text {
    font-size: 1rem;
  }
}
```

## 混合

@mixin 名字 （参数 1, 参数 2）{}

```scss
// mixin声明
@mixin alert($text-color: #35e549, $background: #005140) {
  color: $text-color;
  background-color: $background;

  // darken()加深指定颜色的值
  a {
    color: darken($text-color, 10%);
  }
}

// @include调用传递参数
.alert-error {
  @include alert;
}

.alert-info {
  @include alert($background: yellow, $text-color: green);
}
```

## 继承

```scss
.alert {
  padding: 1.25rem;
}

.alert a {
  text-align: center;
  font-weight: bold;
}

// @extend继承其他类名下的css
.alert-warring {
  @extend .alert;
  background-color: red;
}
```

## partials 与@import

partials 必须以下划线开头 如 `_test.scss` 解决重复编译，请求 url 资源问题
引入文件以 `@import` 开头不需要写下划线但必须以分号结束 `@import 'test.scss';`

## type-of()判断数据类型

scss 中的数据类型 `number`,`string`, `bool`, `color`,`list`, `map`, `null` 可以结合 `@if` 进行判断来执行不同的 css 样式

### 1. number

`type-of(0)` 和 `type-of(1px)` 输出结果都是 number 类型，示例如下

```scss
@if (type-of(1px) == 'number') {
  .body {
    background: red;
  }
}
```

### 2. string

`type-of(a)` 和 `type-of('a')` 输出结果都是 string 类型，示例如下

```scss
@if (type-of(a) == 'string') {
  .body {
    background: red;
  }
}
```

### 3. bool

`type-of(true)` 和 `type-of(0<1)` 输出结果都是 bool 类型，示例如下

```scss
@if (type-of(true) == 'bool') {
  .body {
    background: red;
  }
}
```

### 4. color

`type-of(rgba(0,0,0,0.5))` 和 `type-of(rgb(0,0,0))` ,`type-of(#fff)`, `type-of(red)`, `type-of(hsla(0,0,0,0.5)`, `type-of(hsl(0,0,0)` 输出结果都是 color 类型，目前常使用的颜色表示方式都支持。示例如下

```scss
@if (type-of(rgba(0, 0, 0, 0.5)) == 'color') {
  .body {
    background: red;
  }
}
```

### 5. list(sass.list = js.array)

`type-of((1px, 2px, 3px))` 输出结果都是 list 类型,示例如下

```scss
@if (type-of((1px, 2px, 3px)) == 'list') {
  .body {
    background: red;
  }
}
```

### 5. list(sass.list = js.array)

`type-of((1px, 2px, 3px))` 输出结果是 list 类型,示例如下

```scss
@if (type-of((1px, 2px, 3px)) == 'list') {
  .body {
    background: red;
  }
}
```

### 6. map(sass.map = js.json)

`type-of((a:1px, b:2px, c:3px))` 输出结果是 map 类型,示例如下

```scss
@if (
  type-of(
      (
        a: 1px,
        b: 2px,
        c: 3px
      )
    ) ==
    'map'
) {
  .body {
    background: red;
  }
}
```

### 7. null

`type-of(null)` 输出结果是 null 类型,示例如下

```scss
@if (type-of(null) == 'null') {
  .body {
    background: red;
  }
}
```

## 数字-数字函数

```scss
// 数字
.box {
  padding: 12px + 6px;
  margin: 10 * 2px;
  border-radius: 10 - 6px;
  // 除(16/6)
  // 字号16px 行间距1.8倍
  font: 16px/1.8 serif;
}

//数字函数
.fontStyle {
  font-size: abs(-10px); // 绝对值
  padding: round(2.5); // 四舍五入
  margin: ceil(5.2); // 向上取正
  line-height: floor(5.9); // 向下取正
  border-radius: percentage(650px / 1000px); // 百分数
  width: min(1, 2, 5) + 'px';
  width: max(1, 2, 5) + 'px';
}
```

## 字符串-字符串函数

```scss
// 字符串
.box {
  font-family: 'sans' + Serif;
  font-family: sans - serif;
  font-family: sans / serif;
}

$zhao: 'Hello Word';

// 字符串函数
.fontStyle {
  content: $zhao;
  content: to-upper-case($zhao); // 转换大写
  content: to-lower-case($zhao); // 转换小写
  content: str-length($zhao); // 字符串长度
  content: str-index($zhao, 'Word'); // 字符串索引，在指定位置插入元素
  content: str-insert($zhao, '插入元素', 14); // 字符串插入
}
```

## 颜色

**颜色的表示方式**

- 16 进制 #654585,
- rgb(红 0~255, 绿 0~255, 蓝 0~255)
- rgba(红 0~255, 绿 0~255, 蓝 0~255, 透明度 0~1)
- String red, green, blue
- HSL(色相 0~360,饱和度 0~100%,明度 0~100%)
- HSLA(色相 0~360,饱和度 0~100%,明度 0~100%,透明度 0~1)

```scss
.parBackground {
  color: #654585;
  background-color: rgb(0, 255, 0);
  background-color: rgb(100%, 100, 0);
  background-color: rgba(100%, 100, 0, 0.5);
  background-color: hsl(0, 100%, 50%);
  background-color: hsla(0, 100%, 50%, 0.6);
}

// adjust-hue函数调整颜色色相
$base-color: #ff0000;
$base-color-hsl: hsl(0, 100, 50%);

.ajustHue {
  background-color: adjust-hue($base-color, 30deg);
  background-color: adjust-hue($base-color-hsl, 137deg);
}

// lighten（增加明度）与darken（减少明度）调整颜色明度
.ligDar {
  background: lighten($base-color, 30%);
  background-color: darken($base-color-hsl, 20%);
}

// saturate（增加饱和度）与desaturate（减少饱和度）
.starDes {
  background-color: saturate($base-color-hsl, 30%);
  background-color: desaturate($base-color-hsl, 30%);
}

// opacify（增加） 与 transparentize（减少） 修改颜色透明度
$base-color-op: hsla(222, 100%, 50%, 0.5);

.opacTran {
  background-color: opacify($base-color-op, 0.3);
  background-color: transparentize($base-color-op, 0.2);
}
```

## list-相关函数

**常见的 list 值**

- border: 1px solid #000;
- font-family: Courier, "Lucida ConsoLe", monospace;
- padding: 5px 10px, 5px 0; 等同于 padding: (5px 10px) (5px 0)

```scss
.box {
  padding: length(5px 0); //length函数返回list数量
  padding: nth(5px 0, 1); // nth获取list索引值，索引从1开始
  padding: index(1px solid #333, solid); // 获取某个值的索引位置
  padding: append(2px 10px, 5px); // append插入一个值
  padding: join(2px 10px, 10px 20px); //join合并list
}
```

## map

map 声明方式：`$map: (key1: value1, key2: value2, key3: value3)`

```scss
$colors: (
  light: #ffffff,
  dark: #000000
);
.boxMap {
  color: length($colors); // 返回map长度
  color: map-get($colors, dark); // 获取map中的某个属性值
  color: map-keys($colors); // 获取map中的所有key
  color: map-values($colors); // 获取map中的所有value
  color: map-has-key($colors, dark); // 判断map中是否存在指定的值，返回布尔值
}
```

## 布尔值

```scss
.Boolean {
  padding: 5px > 10px;
  padding: 5px < 10px;
  padding: (5px < 10px) and (10px > 20px);
  padding: (5px < 10px) or (10px > 20px);
  padding: not(10px > 20px); // 对布尔值取反
}
```

## interpolation 插入值

```scss
$version: '0.0.0.1';
/*! 项目当前版本是： #{$version} */

$name: 'info';
$attr: 'border';

.alert-#{$name} {
  #{$attr}-color: #ccc;
}
```

## 控制指令

控制指令包含`@if`, `@for`, `@each`, `@while`

### @if

@if 条件 {}

```scss
$theme: yellow;

body {
  @include legend; @else if $theme==light {
    background-color: white;
  } @else {
    background-color: grey;
  }
}

$use-prefixes: true;

.rounded {
  @if $use-prefixes {
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
  }

  border-radius: 5px;
}
```

### @for

- @for $var from <开始值> through <结束值> {}
- @for $var from <开始值> to <结束值> {} 不包含最后一个

```scss
$columns: 4;
@for $i from 1 through $columns {
  .col-#{$i} {
    width: 100% / $columns * $i;
  }
}
@for $i from 1 to $columns {
  .col-#{$i} {
    width: 100% / $columns * $i;
  }
}

// 示例：循环数组动态根据索引设置颜色
$icon-color: #00fdfb, #ffd200, #fb5911, #2cf698, #5da6fb, #d75efb, #9fff10;
@for $i from 1 through length($icon-color) {
  $item: nth($icon-color, $i);
  .col-#{$i} {
    color: $item;
  }
}
```

### @each

```scss
$icons: success error warning;

@each $icon in $icons {
  icon-#{$icon} {
    background-image: url(./images/#{$icon}.png);
  }
}
```

### @while

```scss
/* 
  @while 条件 {}
  */
$i: 6;

@while $i>0 {
  .item-#{$i} {
    width: 5px * $i;
  }

  $i: $i - 2;
}
```

## 函数自定义

语法：` @function 名称 (参数1, 参数2) {}`

```scss
$colors: (
  light: #ffffff,
  dark: #000000
);

@function color($key) {
  // 添加警告和报错
  @if not map-has-key($colors, $key) {
    @warn "在 $colors 里面没有找到 #{$key} 这个 key";
  }

  @return map-get($colors, $key);
}

body {
  background-color: color(light);
}
```
