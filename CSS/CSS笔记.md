# 目录

- [CSS 笔记](#CSS笔记)
  - [CSS 布局](#CSS布局)
    - [1. Grid | 网格](#Grid|网格)
    - [2. Subgrid | 子网格](#Subgrid|子网格)
    - [3. Flexbox | 弹性盒子](#Flexbox|弹性盒子)
    - [4. Multi-Column Layout | 多列布局](#Multi-ColumnLayout|多列布局)
    - [5. Writing Modes | 文本排列方向](#WritingModes|文本排列方向)

# CSS 笔记

## 布局

### Grid | 网格

```css
.grid {
  display: grid;
  --edge: 10px;
  grid-template-columns: var(--edge) 1fr var(--edge);
}
@media (min-width: 1000px) {
  .grid {
    --edge: 15%;
  }
}
```

### Subgrid | 子网格

[Subgrid](https://developer.mozilla.org/en-us/docs/web/css/css_grid_layout/subgrid)

### Flexbox | 弹性盒子

[Flexbox](https://developer.mozilla.org/zh-CN/docs/Glossary/Flexbox)

### Multi-Column Layout | 多列布局

[Multi-Column Layout](https://cloud.tencent.com/developer/section/1072295)

### Writing Modes | 文本排列方向

writing-mode 属性定义了文本水平或垂直排布以及在块级元素中文本的行进方向。为整个文档设置书时，应在根元素上设置它（对于 HTML 文档应该在 html 元素上设置）
[Writing Modes](https://developer.mozilla.org/zh-CN/docs/Web/CSS/writing-mode)

## 图形图像

### Shapes

### object-fit

### clip-path

### Masking

### blend-mode

### Filters & Effects

### backdrop-filter

## 交互

### Scroll Snap

### overscroll-behavior

### overflow-anchor

### touch-action

### pointer-events

## 排版

### @font-face

### Line Breaking Properties

### font-variant

### initial_letter

### font-variant-numeric

### font-display

### line-clamp

`-webkit-line-clamp` CSS 属性可以设置块容器内内容显示的行数，只有在`display`属性设置成`-webkit-box`或者`-webkit-inline-box` 并且`-webkit-box-orient`属性设置成`vertical`时才有效果。多数情况序号设置 `overflow` 属性为 `hidden` 否则里面的内容不会被裁剪。并且在指定行数后还会显示省略号。
其应用场景：多行文本省略显示。可以结合 标签的 `title` 属性或者结合`ant desing vue `组件库中的 Tooltip 做文字悬停展示更多，超出隐藏

```html
<div class="box">
  <p>
    多行文本省略显示。可以结合 标签的 title 属性或者结合ant desing vue
    组件库中的Tooltip 做文字悬停展示更多，超出隐藏多行文本省略显示。可以结合
    标签的 title 属性或者结合ant desing vue 组件库中的Tooltip
    做文字悬停展示更多，超出隐藏多行文本省略显示。可以结合 标签的 title
    属性或者结合ant desing vue 组件库中的Tooltip 做文字悬停展示更多，超出隐藏
  </p>
</div>
```

```css
.box {
  width: 200px;
  height: 300px;
  border: 1px solid black;
}
p {
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2; /*设置p元素最大2行，父元素需设置宽度*/
  text-overflow: ellipsis;
  overflow: hidden;
  -webkit-box-orient: vertical;
}
```

## 动画与过度

### Transitions

### Transforms

### Animations

## 媒体查询

### @media

```css
/* 1200-1366 */
@media (min-width: 1200px) and (max-width: 1366px) {
}

/* 1367-1920 */
@media (min-width: 1367px) and (max-width: 1920px) {
}

@media screen and (max-width: 960px) {
  body {
    background: #000;
  }
}
```

### prefers-reduced-motion

### prefers-color-scheme

### color-gamut

## 其他特性

### Variables

### @supports

@supports 用于检测浏览器是否支持该属性
支持逻辑运算符；not, and, or

```css
@supports (display: grid) {
  div {
    background-color: rgb(255 124 0 / 85%);
  }
}
```

### Contain

### will-change

### calc()

### Css Houdini

### Css Comparison Functions

## 自定义属性

1. 自定义属性

```css
:root {
  --color: #42ea66;
  --main-bg: rgba(30, 6, 238, 0.651);
}
```

2. 在 html 结构中使用变量

```html
<div style="--color: yellow">style</div>
```

3. 在 CSS 中使用变量

```css
.box {
  color: var(--color);
}
```

## 字符串拼接

```css
.string {
  --value: 12;
  font-size: calc(var(--value) _ 2px);
  --pixel_converter: 1px;
  font-size: calc(var(--value) _ var(--pixel_converter));
}
```

## @property

@ property CSS at-rule 是 CSS Houdini api 的一部分，它允许开发人员显式定义 CSS 自定义属性，允许属性类型检查，设置默认值，并定义属性是否可以继承值
该规则直接在样式表中表示自定义属性的注册，无需运行任何 JS。有效的规则会生成一个已注册的自定义属性，就好像使用等效的 parameters.@property@property 调用了 CSS.registerProperty 一样
syntax：语法
@property --my-color：声明一个自定义属性 --mycolor
inherits：是否允许继承
initial-value：初始值

```css
@property --houdini-colorA {
  syntax: '<color>';
  inherits: false;
  initial-value: #fff;
}
@property --houdini-colorB {
  syntax: '<color>';
  inherits: false;
  initial-value: #000;
}
```

## 提高页面渲染速度

```css
.card {
  content-visibility: auto;
  contain-intrinsic-size: 200px;
}
```

## CSS 单位

### px

### %

### vh,vw

### em

### rem

### vmin,vmax

### pt

### ch

### cm

### mm

### in

### ex

## 选择器

### 伪类与伪元素

[伪元素](CSS%E4%BC%AA%E7%B1%BB%E4%B8%8E%E4%BC%AA%E5%85%83%E7%B4%A0.md)

### 关系选择器（Combinators）

#### div span （后代选择器）

#### div > span （子选择器）

#### div + div （相邻选择器）

#### div ~ span （兄弟选择器）

### 属性选择器

#### div[foo="bar"]

#### div[foo]

#### div[foo^="bar"]

#### div[foo*="bar"]

#### div[foo~="bar"]

#### div[foo$="bar"]

# 预/后处理

## PostCSS

## [Sass](./SCSS%E8%AF%AD%E6%B3%95%E7%AC%94%E8%AE%B0.md)

## Stylus

## Less

# CSS 框架

## Tailwind CSS

## Bulma

## PureCSS

## Tachyons

## Semantic UI

## Ant Design

## Materialize CSS

## UIKit

## Bootstrap

## Foundation

## Primer

# CSS-in-JS

## Emotion

## CSS Modules

## Styled Components

## Styled JSX

## JSS

## Radium

# CSS 工具库

## Stylelint

## PurgeCSS

在 vue 项目中使用
删除未使用的 CSS 代码

- [1. PurgeCSS 删除未使用的 CSS 代码](#PurgeCSS删除未使用的CSS代码)

在提高 web 性能的方法中，可以通过移除不需要的 js 和 css 达到性能提高。
PurgeCSS 是一个用来删除未使用的 CSS 代码的工具
[PurgeCSS](https://www.purgecss.cn/)

1. 安装

```
vue add @fullhuman/purgecss
```

安装成功后会在项目创建`postcss.config.js`文件

2. postcss.config.js 文件修改

- content 字段，来告诉 PurgeCSS 去哪里查找将要对应匹配的 class
- whitelist 对于一些你不想要移除的 class 或者某些标签上对应的样式名称，你可以它们加到白名单字段中。你至少需要添加 html 和 body 标签以及任意的动态 class 样式名称到白名单配置字段中

```javascript
const IN_PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
  plugins: [
    IN_PRODUCTION &&
      require('@fullhuman/postcss-purgecss')({
        content: [
          `./public/**/*.html`,
          `./src/**/*.vue`,
          `./layouts/**/*.vue`,
          `./components/**/*.vue`,
          `./pages/**/*.vue`
        ],
        whitelist: ['html', 'body'],
        defaultExtractor(content) {
          const contentWithoutStyleBlocks = content.replace(
            /<style[^]+?<\/style>/gi,
            ''
          )
          return (
            contentWithoutStyleBlocks.match(
              /[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g
            ) || []
          )
        },
        safelist: [
          /-(leave|enter|appear)(|-(to|from|active))$/,
          /^(?!(|.*?:)cursor-move).+-move$/,
          /^router-link(|-exact)-active$/,
          /data-v-.*/
        ]
      })
  ]
}
```

## PurifyCSS
