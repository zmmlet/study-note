# ES6 学习笔记

## 变量解构赋值

### 字符串解构赋值

```javaScript
let [a, b, c] = [1, 2, 3]
console.log(a, b, c)
```

### 对象解构赋值

```javaScript
let { foo, bar } = { foo: "aaa", bar: "bbb" };
console.log(foo, bar)
```

### 数值解构赋值

```javaScript
let { toString: s } = 123;
console.log(s === Number.prototype.toString) // true
```

### 布尔值解构赋值

```javaScript
let { toString: k } = true;
console.log(k === Boolean.prototype.toString) // true
```

### 解构赋值-交换变量值

```javaScript
let x = 6;
let y = 8;
console.log([x, y] = [y, x]) // 8, 6
```

### 解构赋值-函数中返回多个值

**返回数组**

```javaScript
function example () {
  return [1, 2, 3];
}
let [a, x, z] = example();
console.log(a, x, z) // 1, 2,3
```

**返回对象**

```javaScript
function example() {
  return {
    foo: 1,
    bar: 2
  };
}
let { foo, bar } = example();
console.log(foo, bar) // 1, 2
```

### 参数与变量名对应

```javaScript
function f({x, y, z}) {
  console.log(x, y, z) // 1, 2, 3
}
f({z: 3, y: 2, x: 1});
```

### 提取 JSON 对象

```javaScript
let jsonData = {
  id: 42,
  status: "OK",
  data: [867, 5309]
};
let { id, status, data: number } = jsonData;
console.log(id, status, number); // 42, "OK", [867, 5309]
```

### 输入模块的指定方法

```javaScript
import { Row, Col, List, Icon } from 'antd'
```

## 字符串的扩展

### 字符的 Unicode 表示法

```javaScript
console.log("\u0061", "\u{41}\u{42}\u{43}") // a, "ABC"
let hello = 123;
console.log(hell\u{6F}) // 123
console.log('\u{1F680}' === '\uD83D\uDE80')// true
```

### codePointAt()

codePointAt()测试字符串由两个字节还是由四个字节组成

```javaScript
function is32Bit(c) {
  return c.codePointAt(0) > 0xFFFF;
}
is32Bit("" ) // true
is32Bit("a") // false
```

### String.fromCodePoint()

String.fromCharCode 方法，用于从码点返回对应字符，但是这个方法不能识别 32 位的 UTF-16 字符（Unicode 编号大
于 0xFFFF ）

```javaScript
String.fromCodePoint(0x78, 0x1f680, 0x79) === 'x\uD83D\uDE80y' // true
```

### 字符串的遍历器接口

```javaScript
for (let codePoint of 'foo') {
console.log(codePoint)
}
// "f"
// "o"
// "o"
```

### at()

```javaScript
//es5
'abc'.charAt(0) // "a"
// es6
'abc'.at(0) // "a"
```

### normalize()

字符串实例的 normalize() 方法，用来将字符的不同表示方法统一为同样的形式，这称为 Unicode 正规化

```javaScript
// 视觉上相等，语义上相等，javascript不能识别
'\u01D1'==='\u004F\u030C' //false
'\u01D1'.length // 1
'\u004F\u030C'.length // 2
// Unicode 正规化
'\u01D1'.normalize() === '\u004F\u030C'.normalize() // true
```

### includes(), startsWith(), endsWith()

传统上，JavaScript 只有 indexOf 方法，可以用来确定一个字符串是否包含在另一个字符串中。ES6 又提供了三种新方法。

- `includes()`：返回布尔值，表示是否找到了参数字符串。
- `startsWith()`：返回布尔值，表示参数字符串是否在原字符串的头
  部。
- `endsWith()`：返回布尔值，表示参数字符串是否在原字符串的尾部

```javaScript
let s = 'Hello world!';
s.startsWith('Hello') // true
s.endsWith('!') // true
s.includes('o') // true
```

同时支持第二个参数，表示开始搜索的位置

```javaScript
let s = 'Hello world!';
s.startsWith('world', 6) // true
s.endsWith('Hello', 5) // true
s.includes('Hello', 6) // false
```

### repeat()

`repeat()`方法返回一个新字符串，表示将原字符串重复 n 次，不支持负数，如果设置成小数则被取整

```javaScript
'x'.repeat(3) // "xxx"
```

### padStart()，padEnd()

如果某个字符串不够指定长度，会在头部或尾部补全。`padStart()` 用于头部补全，`padEnd()` 用于尾部补全。接收两个参数，参数一用于指定字符串的最小长度，参数二用来补全的字符串，如果原来字符串长度，等于或大于指定的最小长度，则返回原字符串。

```javaScript
'x'.padStart(4, 'ab') // 'abax'
'x'.padEnd(5, 'ab') // 'xabab'
```

### 模板字符串

模板字符串中嵌入变量，需要将变量名写在`${}`中

```javaScript
// 传统写法
$('#id').append(
'There are <b>' + basket.count + '</b> ' +
'items in your basket, ' +
'<em>' + basket.onSale +
'</em> are on sale!'
);

// es6
$('#result').append(`
There are <b>${basket.count}</b> items
in your basket, <em>${basket.onSale}</em>
are on sale!
`);
```
