/**
 * @desc 判断是否是数组
 * @param {Array}} arr 数组
 */
export const arrJudge = (arr) => {
  if (Array.isArray(arr)) {
    return true
  }
}

/**
 * @desc 数组去重
 * @param {Array} arr  数组
 */
export const arrRemoveRepeat = (arr) => {
  return Array.from(new Set(arr))
}

/**
 * @desc 数组对象去重
 * @param {Array} arr1 数组对象  [{ id: 1 }, { id: 2 }, { id: 3 }];
 * @param {Array} arr2 数组对象  [{ id: 3 }, { id: 4 }, { id: 5 }];
 * @retrun {Array} [ { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 } ]
 */
export const mergeArray = (arr1, arr2) => {
  // 克隆
  const cloneArr1 = arr1.slice(0)
  let v
  for (let i = 0; i < arr2.length; i++) {
    v = arr2[i]
    // 能找到相同 id 属性值的数据则进入判断
    // ~按位非 ~~双非
    if (~cloneArr1.findIndex((el) => el.id === v.id)) {
      continue
    }
    cloneArr1.push(v)
  }
  return cloneArr1
}

/**
 * @desc 数组排序
 * @param {Array} arr  数组
 * @param {Boolean} ascendFlag   升序,默认为 true
 */
export const arrOrderAscend = (arr, ascendFlag = true) => {
  return arr.sort((a, b) => {
    return ascendFlag ? a - b : b - a
  })
}

/**
 * @desc 数组最大值
 * @param {Array} arr  数组
 */
export const arrMax = (arr) => {
  return Math.max(...arr)
}

/**
 * @desc 数组求和
 * @param {Array} arr 数组
 */
export const arrSum = (arr) => {
  return arr.reduce((prev, cur) => {
    return prev + cur
  }, 0)
}

/**
 * @desc 数组对象求和
 * @param {Object} arrObj 数组对象
 * @param {String} key 数组对应的 key 值
 */
// eslint-disable-next-line no-unused-vars
export const arrObjSum = (obj, key) => {
  // eslint-disable-next-line no-undef
  return arrObj.reduce((prev, cur) => prev + cur.key, 0)
}

/**
 * @desc 数组合并,目前合并一维
 * @param {Array} arrOne 数组
 * @param {Array} arrTwo 数组
 */
export const arrConcat = (arrOne, arrTwo) => {
  return [...arrOne, ...arrTwo]
}

/**
 * @desc 数组是否包含某值
 * @param {Array} arr 数组
 * @param {}  value 值,目前只支持 String,Number,Boolean
 */
export const arrIncludeValue = (arr, value) => {
  return arr.includes(value)
}

/**
 * @desc 数组并集,只支持一维数组
 * @param {Array} arrOne
 * @param {Array} arrTwo
 */
export const arrAndSet = (arrOne, arrTwo) => {
  return arrOne.concat(arrTwo.filter((v) => !arrOne.includes(v)))
}

/**
 * @desc 数组交集,只支持一维数组
 * @param {Array} arrOne
 * @param {Array} arrTwo
 */
export const arrIntersection = (arrOne, arrTwo) => {
  return arrOne.filter((v) => arrTwo.includes(v))
}

/**
 * @desc 数组差集,只支持一维数组
 * @param {Array} arrOne
 * @param {Array} arrTwo
 * eg: [1, 2, 3] [2, 4, 5] 差集为[1,3,4,5]
 */
export const arrDifference = (arrOne, arrTwo) => {
  return arrOne
    .concat(arrTwo)
    .filter((v) => !arrOne.includes(v) || !arrTwo.includes(v))
}

/**
 * @desc 两个数组合并成一个对象数组,考虑到复杂度,所以目前支持两个一维数组
 * @param {Array} arrOne
 * @param {Array} arrTwo
 * @param {oneKey} oneKey 选填,如果两个都未传,直接以 arrOne 的值作为 key,arrTwo 作为 value
 * @param {twoKey} twoKey
 */
export const arrTwoToArrObj = (arrOne, arrTwo, oneKey, twoKey) => {
  if (!oneKey && !twoKey) {
    return arrOne.map((oneKey, i) => ({ [oneKey]: arrTwo[i] }))
  } else {
    return arrOne.map((oneKey, i) => ({ oneKey, twoKey: arrTwo[i] }))
  }
}

/**
 * @desc 将对象转化为对象数组
 * @param {Object} objData
 */
export const objToArr = (objData) => {
  var arr = []
  for (let i in objData) {
    let o = {}
    // eslint-disable-next-line no-undef
    o[i] = obj[i] //即添加了key值也赋了value值 o[i] 相当于o.name 此时i为变量
    arr.push(o)
  }
  return arr
}

/**
 * @desc 多数组相加
 * @param {arr} arr 二维数组
 * [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 4, 5, 6 ] ]
 */
export const arrTotalArray = (arr) => {
  let result = []
  for (let i = 0; i < arr.length; i++) {
    arr[i].forEach((value, index) => {
      if (result[index] == null || result[index] == '') {
        result[index] = 0
      }
      result[index] += parseInt(value)
    })
  }
  return result //[ 9, 12, 15 ]
}
