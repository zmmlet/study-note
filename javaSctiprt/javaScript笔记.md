# javaScript 笔记

## javaScript 简介

JavaScript（简称“JS”） 是一种具有函数优先的轻量级，解释型或即时编译型的编程语言。虽然它是作为开发 Web 页面的脚本语言而出名，但是它也被用到了很多非浏览器环境中，JavaScript 基于原型编程、多范式的动态脚本语言，并且支持面向对象、命令式、声明式、函数式编程范式。
javaScript 区分大小写 ，在 javaScript 中变量 test 和变量 Test 表示的是不同的变量。

## 注释方式

javaScript 支持两种注释方式：单行注释、多行注释

```javascript
// 单行注释
/*
 *
 * 多行（块级）注释
 *
 *
 * */
```

## 严格模式

```javascript
function(){
‘use strict’；
//函数体
}
```

## script 标签解析

`<script>xxx</script>` 这组标签，是用于在 html 页面中插入 js 的主要方法。它主要有以下几个属性：

1. charset：可选。表示通过 src 属性指定的字符集。由于大多数浏览器忽略它，所以很少有
   人用它。
2. defer：可选。表示脚本可以延迟到文档完全被解析和显示之后再执行。由于大多数浏览
   器不支持，故很少用。
3. language：已废弃。原来用于代码使用的脚本语言。由于大多数浏览器忽略它，所以不要
   用了。
4. src：可选。表示包含要执行代码的外部文件。
   type：可选。可以看作是 language 的替代品。表示代码使用的脚本语言的内容类型。范例：
   type="text/javascript"。
   如果你想弹出一个</script>标签的字符串，那么浏览器会误解成 JS 代码已经结束了。解决
   的方法，
   就是把字符串分成两个部分，通过连接符‘+’来连接。
   ```javascript
   <script type="text/javascript">
       alert('</scr'+'ipt>');
   </script>
   ```
   一般来说，JS 代码越来越庞大的时候，我们最好把他另存为一个.js 文件，通过 src 引入即
   可。
   它还具有维护性高、可缓存(加载一次，无需加载)、方便未来扩展的特点。
   <script type="text/javascript" src="demo1.js"></script>
   这样标签内就没有任何 JS 代码了。但，要注意的是，虽然没有任何代码，也不能用单标签：
   <script type="text/javascript" src="demo1.js" />；
   也不能在里面添加任何代码：
   <script type="text/javascript" src="demo1.js">alert('我很可怜，执行不到！')
   </script>

## 字符集

JavaScript 是使用 Unicode 字符集编写的，Unicode 是 ASCII 和 Latin-1 的超集
Unicode 允许使用多中方法对一个字符进行编码，如字符`e`可以使用 Unicode 字符`\u00E9`表示，也可以使用 ASCII 进行表示
javaScript 是区分大小写的语言，也就是，关键字、变量、函数名和说有的标识符都必须采取一致大小写的形式

## 类型、值和变量

JavaScript 的数据类型分为两类：原始类型（primitive type）和对象类型（object type）
javaScript 的数据类型一共七种分别是: Number, String, Boolean, unll, undefined, object, array

### 数据类型

**Number 类型**
JavaScript 采用 IEEE 754 标准定义 64 位浮点格式表示数字，不区分整数值和浮点数值，所有数字均用浮点数值表示
**_整数型直接量_**
用一个数字序列表示一个十进制证书，例如：
同时支持十六进制和八进制和二进制

```javascript
0
3
1000000
```

**_浮点型直接量_**
浮点型直接量可以含有小数，由两部分组成一个部分是整数，另一部分是小数部分，例如：

```javascript
3.14
0.333333333
2345.789
6.02e23 // 6.02 X 10的23次幂
```

**_算术运算_**
算术运算符进行数字元素，包括加（+）减（-）乘（\*）除（/）和求余（%），除简单运算符还支持复杂的算术运算，复杂的运算通过`Math`对象的属性定义的函数和常量来进行实现

```javascript
Math.pow(2, 53) // 9007199254740922: 2 的53次幂
Math.round(0.6) // 1.0 四舍五入
Math.ceil(0.6) // 1.0 向上取整
Math.floor(0.6) // 0.0 向下求整
Math.abs(-56) // 56 求绝对值
Math.max(x, y, z) // 返回最大值
Math.min(x, y, z) // 返回最小值
Math.PI // 圆周率
Math.sprt(3) // 3的平方根
Math.pow(3, 1 / 3) // 3的立方根
Math.sin(0) // 三角函数，还有Math.cos, Math.atan 等
Math.log(10) // 10的自然对数
Math.log(100) / Math.LN10 // 以10为底100的对数
Math.log(512) / Math.LN2 // 以2为底512的对数
Math.exp(3) // e的三次幂
```

**_日期和时间_**
`Data()`构造函数，用来创建表示日期和时间的对象，用于日期的计算，日期对象不像数字那样是基础数据类型

```javascript
var then = new Date(2020, 0, 1) // 2020年1月1日
var later = new Date(2011, 0, 1, 17, 10, 30) // 同一天当地时间 5:10:30pm
var now = new Date() // 当前时间
var elapesd = now - then // 日期减法：计算时间相隔的毫秒数
later.getFullYear() // 2020
later.getMonth() // 从0开始计数的月份
later.getDate() // 从1开始计数的天数
later.getDay() // 得到星期几 0代表星期日 5代表星期一
later.getHours() // 当地时间 16:32
later.getUTCHours() // 使用UTC表示小时的时间，基于时区
```

**String 类型**
字符串（string）是一组有 16 位值组成的不可变的有序序列，每个字符通常来自 Unicode 字符集，javaScript 通过字符串类型来表示文本，索引从 0 开始，字符长度通过`string.length`来表示
**_字符串直接量_**
由单引号或双引号括起来的字符序列，单引号和双引号可以项目包含，例如：

```javascript
'' // 空字符串，包含0个字符
'testing'
"name='myform'"
```

**_转义字符_**
|转移字符|含义|
|--|--|
|\o|NUL 字符（\u0000）|
|\b|退格符（\u0008）|
|\t|水平制表符（\u0009）|
|\n|换行符（\u000A）|
|\v|垂直制表符（\u000B）|
|\f|换页符（\u000C）|
|\r|回车符（\u000D）|
|\"|双引号（\u0022）|
|\'|撇号或单引号（\u0027）|
|\\|反斜杠（\u005C）|
**_字符串的使用_**
字符串拼接，用运算符`+`号进行连接两个字符串，例如：

```javascript
msg = 'hello,' + 'world' // 生成字符串 "hello world"
```

字符串提供的属性和调用方法

```javascript
var s = 'hello world'
s.length // 返回字符串长度，字符串的属性
s.charAt(0) // 第一个字符h
s.charAt(s.length - 1) // 最后一个字符d
s.substring(1, 4) // 第2~4个字符 ell
s.slice(1, 4) // 第2~4个字符 ell
s.slice(-3) // 最后三个字符 rld
s.indexOf('l') // 字符l首次出现的位置
s.lastIndex('l') // 字符串l最后一次出现的位置
s.indexOf('l', 3) // 在位置3及之后首次出现字符l的位置
s.split(', ') // 分割字符串
s.replace('h', 'H') // 全文字符替换 Hello, world
s.toUpperCase() // HELLO, WORLD
```

javaScript 中字符串是固定不变的类似`replace()` 和 `toUpperCase()`的方法都会返回新的字符串，原字符串本身并没有发生改变
**_模糊匹配_**
`RegExp()`构造函数，用来创建表示文本匹配模式的对象，这个模式成为**正则表达式**（regular expression）,`RegExp`不属于基本类型，两条斜线之间的文本构成了一个正则表达式直接量，第二条斜线之后可以跟随一个或多个字母，用来修饰匹配模式的含义，例如：

```javascript
;/^HTML/ / // 匹配以HTML开头的字符串
  [1 - 9][0 - 9] / //匹配一个非零数字，后面是任意个数字
  /\bjavascript\b/i // 匹配单词 javascript 忽略大小写
```

`EegExp`对象定义很多的方法，字符串同样具有可以接收`RegExp`参数的方法例如：

```javascript
var text = 'testing: 1, 2, 3' // 文本示例
var pattern = /\d+/g // 匹配所有包含一个或多个数字的实例
patrern.test(text) // 匹配成功 true
text.search(pattern) // 首次匹配成功的位置
```

**Boolean 类型**
布尔值要么为真要么为假，保留字为`true`和`false`, 布尔值包含`toString()`方法，因此可以使用这个方法将字符串转换为`true`或`false`

```javascript
if (a == 4)
  // true 情况执行
  b = b + 1
else a = a + 1 // false 情况执行
```

**null 和 undefined**
`null`是关键字，表示一个特殊值，常用来描述特殊值，对`null`执行`typeof`预算，结果返回字符串`object`,也就是说，可以将 null 认为是一个特殊的对象值，其含义是**非对象**
`undefined`也用来表示值的空缺，如果查询元素是对象属性的时候值返回为`undefined`则说明该元素或属性不存在，在未定义进行引用的时候也会返回`undefined`

### 对象和值

**全局对象**
全局对象（global object），全局对象的属性是全局定义的符号，javaScript 程序中可以直接使用
|类型|举例|
|--|--|
|全局属性|比如 undefined、Infinity 和 NaN|
|全局函数|比如 isNaN()、parseInt()、eval()|
|构造函数|比如 Date()、RegExp()、String()、Object()、Array()|
|全局对象|比如 Math 和 JSON|
不在任何函数内的 javaScript 代码，可以使用 javaScript 关键字`this`来引用全局对象

```javascript
var global = this //定义一个引用全局对象的全局变量
```

**包装对象**
JavaScript 对象是一种符合值，他是属性或已命名的几何，通过`.`符号来进行引用属性值，当属性值是一个函数的时候，称其为方法。通过`o.m()`来调用对象`o`中的方法
字符串同样也有属性和方法

```javascript
var s = 'hello world'
var word = s.substring(s.indexOf(' ') + 1, s.length) // 使用字符串的属性
```

由于字符串、数字、布尔值的属性都只是可读的，并不能给他们定义新属性，需要注意的是，可以通过 String(), Number(), Boolean()构造函数来显式创建包装对象

```javascript
var n = 'name',
  h = 1,
  t = true
var s = new String(n) // 一个字符串
var b = new Number(h) // 一个数字
var x = new Boolean(t) // 布尔值
```

**不可变的原始值和可变的对象引用**
javaScript 的原始值（undefined, null, 布尔值， 数字和字符串），原始值任何方法都无法改变
字符串所有的方法看上去返回了一个修改后的字符串实际上返回的是一个新的字符串，例如：

```javascript
var s = 'hello' // 定义字符串
s.toUpperCase() // 返回HELLO，单并没有改变s的值
s // hello 原始字符串的值并未改变
```

比较两个数组的函数是否相等

```javascript
function equalArrays(a, b) {
  if (a.length != b.length) return false // 两个长度不相同的数组不相等
  for (var i = 0; i < a.length; i++) {
    //循环所有元素
    if (a[i] !== b[i]) return false // 如果任意元素不相等则数组不相等
    return true // 否则两个数组相等
  }
}
```

**类型转换**
类型转换表
|值|字符串|数字|布尔值|对象|
|--|--|--|--|--|
|undefined|"undefined"|NaN|false|throws TypeError|
|null|"null"|0|false|throws TypeError|
|true|"true"|1||new Boolean(true)|
|false|"false"|0||new Boolean(fasle)|
![avaScript-02](https://gitee.com/zmmlet/study-note/raw/master/images/javaScript-02.png)
原始值到对象的转换可以通过调用`String()`, `Number()` 或 `Boolean()` 构造函数，转换为各自的包装对象

##### 变量

使用一个变量前应该先进行声明,变量是标识值的一个符号名称，变量是通过`var`关键字声明的，变量的声明可以是任意类型的

```javascript
var x; // 声明变量
x = 1; // 对变量进行赋值
x = 0.0.1;  // 整数和实数共用一种数据类型
x = "hello word"; // 双引号构成的字符串
x = 'javaScript'; // 单引号构成的字符串
x = true; // 布尔值 布尔值包含两个值（true和false）
x = null; // null是一个特殊的值，以为是空
x = undefined; // undefined和null非常相似
```

javaScript 中的最重要的类型就是对象
对象是键和值对的几何，或字符串到值的映射的合集，对象是由花括号`{}`括起来的，属性`topic`的值是`JavaScript`

```javascript
var book = {
  topic: 'JavaScript',
  fat: true
}
```

对象的访问通过`.`或`[]`来进行访问对象属性

```javascript
book.topic // JavaScript
boot['fat'] // true
book.author = 'flanagan' // 通过赋值创建一个新属性
book.contents = {} // {} 是一个空对象没有属性
```

javaScript 同样支持数组（以数字为索引的列表）

```javascript
var proies = [2, 4, 6, 8, 10]
proies[0] // 数组中的一个数
proies.length // 数组的长度为5
proies[proies.length - 1] // 数组的最后一个元素10
proies[4] = 12 // 通过赋值来添加新元素或通过赋值改变已有元素
var empty = [] // [] 是一个空数组，字符长度为0，具有零个元素
```

数组和对象中都可以包括零个数组或对象

```javascript
var points = [
  {x: 0, y: 1}, // 具有两个元素的数组
  {x: 1, y: 2} // 每个元素都是一个对象
]
var data = { // 一个包含两个属性的对象
  trial1 = [[1,2], [3,4]]; // 每一个属性都是数组
  trial2 = [[2,6], [4,5]]; // 数组的元素也是数组
}
```
