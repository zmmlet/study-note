/* 
Dart 数组类型 list
 */
void main() {
  // 第一种定义 List 方式
  var list = ['aaaa', 'bbbb', 'cccc'];
  print(list);
  print(list.length);
  print(list[1]);
  print(list.first);
  print(list.last);
  print(list.remove('cccc')); // remove 找不到时返回false

  // 第二种定义 List 方式
  List newList = new List();
  newList.add('张三');
  newList.addAll(['李四', '王五']);

  print(newList);

  // 定义List 指定类型
  // var li = new List<String>();
  // li.addAll(['张飞', '李斯特']);
  //  print(li);
  // li.forEach((item) => {print(item)});

  var li = new List<Map>();
  li.addAll([
    {"name": '张飞'},
    {"name": '李斯特'}
  ]);

  print(li);
  li.forEach((item) => {print(item)});
}
