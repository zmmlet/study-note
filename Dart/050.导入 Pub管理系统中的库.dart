/* 
Pub 包管理系统
1.从下面的网址找到要用的库
  https://pub.dev/packages
  https://pub.flutter-io.cn/packages
  https://pub.dartlang.org/flutter/
2.创建一个 pubspes.yaml 文件内容如下
  name: Dart
  version: 0.0.1
  description: a is dart applictin
  dependencies:
    http: ^0.12.2
    date_format: ^1.0.6
3.配置 dependencies
4.运行 put get 获取远程库
5.查看文档使用
 */
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:date_format/date_format.dart';

main() async {
  var url =
      'http://www.phonegap100.com/appapi.php?a=getPortlList&catid=20&page=1';
  var response = await http.get(url);
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    var itemCount = jsonResponse['totalItems'];
    print('Number of books about http: $itemCount.');
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }

  print(formatDate(DateTime(1989, 02, 21), [yyyy, '-', mm, '-', dd]));
}
