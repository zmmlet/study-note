/* 
Dart 中的接口 和 java接口区别
 首先dart的几口没有 interface 关键字定义接口，而普通类或抽现象类都可以作为接口被实现
 同样使用 implements 关键字进行实现
 但是 dart 的接口有点奇怪，如果实现的类是普通类，会将普通类和抽象中的属性方法全部都需要覆写一遍
 而因为抽象类可以定义抽象方法，普通类不可以，所有一般如果要实现像 java 接口那样的样式，一般会使用抽象类
 建议使用抽象类定义接口
 */

import 'lib/MySQL.dart';

void main() {
  MySQL mysql = new MySQL('localhost:3306');
  mysql.add('{"name": "张三", "age": "24", "sex": "男"}');
}
