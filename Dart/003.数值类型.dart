/* 
Dart 数据类型：数值类型
 */

void main() {
  // 1.int 类型
  int number = 4445;
  print(number); // 4445

  // 2.double 类型
  double minMumber = 0.1314;
  print(minMumber); // 0.1314

  minMumber = 25;
  print(minMumber); // 25.0

  // 3.运算符(+,-,*,/,%) 和javaScrit语法一样

  var c = minMumber + number;
  print(c);
}
