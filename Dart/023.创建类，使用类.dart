/* Dart 是一门使用类和单继承的面向对象语言，所有的对象都是类的实例，并且所有的类都是Object的子类
  一个类通常由属性和方法组成 */

// 类名首字母大写
class Person {
  String name = "张三";
  int age = 25;
  // void 标识类中的方法没有返回值
  void getInfo() {
    print("$age---$name");
    print("${this.age}--this输出--${this.name}");
  }

  void setInfo(int age) {
    this.age = age;
  }
}

void main() {
  // 实例化
  // var p = new Person();
  // print(p.name);

  // p.getInfo();

  Person p = new Person();
  print(p.name);

  p.setInfo(25);
}
