/* 
  for 基本语法
  for (int i = 1; i <= 100; i++) {
    print(i);
  }
   */
//  第一步：声明变量int i = 1
//  第二步：判断 i <= 100
//  第三步：print(i)
//  第四步：i++
//  第五步： 从第二步再来，直到判断为false

void main() {
  // 1.打印0-50所有的偶数
  int number = 50;
  for (int i = 1; i <= number; i++) {
    if (i % 2 == 0) {
      print(i);
    }
  }

  // 2. 求 1+2+3+....+100 的和
  int numMumber = 100;
  int totalNuimber = 0;
  for (int i = 1; i <= numMumber; i++) {
    totalNuimber += i;
  }
  print(totalNuimber);

  // 3.计算5的阶乘 （1*2*3*4*5 n的阶乘1*2....*n）
  int sum = 1;
  for (int j = 1; j <= 5; j++) {
    sum *= j;
  }
  print("this is numberTotal $sum");

  // 4. 打印 list
  List list = [
    {"name": '张三'},
    {"name": '李四'}
  ];

  list.forEach((item) => {print(item)});

  for (var i = 0; i < list.length; i++) {
    print(list[i]['name']);
  }

  // 5.二维数组
  List newList = [
    {
      "cate": "国内",
      "news": [
        {"title": "国内新闻1"},
        {"title": "国内新闻2"},
      ]
    },
    {
      "cate": "国外",
      "news": [
        {"title": "国外新闻1"},
        {"title": "国外新闻2"},
      ]
    }
  ];
  for (var i = 0; i < newList.length; i++) {
    print(newList[i]["cate"]);
    for (var j = 0; j < newList[i]["news"].length; j++) {
      print(newList[i]["news"][j]['title']);
    }
  }
}
