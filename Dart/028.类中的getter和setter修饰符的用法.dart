// Dart 中的 getter
class Rect {
  num height;
  num widht;
  Rect(this.height, this.widht);
  get area {
    return this.height * this.widht;
  }
}

// Dart 中的 setter
class RectSet {
  num height;
  num widht;
  RectSet(this.height, this.widht);
  get area {
    return this.height * this.widht;
  }

  set areaHeight(value) {
    this.height = value;
  }
}

/* 
Getters和Setter(也称为访问器和更改器)允许程序分别初始化和检索类字段的值。
使用get关键字定义getter或访问器。Setter或存取器是使用set关键字定义的。
默认的getter/setter与每个类相关联。
但是，可以通过显式定义setter/getter来覆盖默认值。getter没有参数并返回一个值，setter只有一个参数但不返回值
 */
void main() {
  // get 调用
  Rect r = new Rect(10, 2);
  print("面积:${r.area}"); // 直接通过访问属性的方式访问area
  // set 调用
  RectSet st = new RectSet(10, 4);
  st.areaHeight = 6;
  print(r.area);
}
