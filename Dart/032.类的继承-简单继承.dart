/* 
面向对象三大特性：封装、继承、多态
Dart 中的类的继承
  1. 子类使用 extends 关键字来继承父类
  2. 子类会继承父类里面可见的属性和方法但是不会继承构造函数
  3. 子类能覆写父类的方法 getter 和 setter
 */

class Person {
  String name = '张三';
  num age = 20;
  void printInfo() {
    print("${this.name}---${this.age}");
  }
}

// Web 类继承 Person 类
class Web extends Person {}

void main() {
  Web w = new Web();
  print(w.name);
  w.printInfo();
}
