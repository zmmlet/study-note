/* 
Dart 中的对象操作符
? 条件运算符 (了解)
as 类型转换
is 类型判断
.. 级联操作（连缀）
 */

class Person {
  String name;
  num age;
  Person(this.name, this.age);
  void printInfo() {
    print("${this.name}----${this.age}");
  }
}

void main() {
  Person p;
  // 使用 ? 判断 如果 p 对象为空 则不调用
  p?.printInfo();

  Person s = new Person('张三', 20);
  if (s is Person) {
    s.name = '李四';
  }
  s.printInfo();
  // 判断类型
  print(s is Object);

  var p1;
  p1 = '';
  p1 = new Person('张三', 40);
  // 类型转换
  (p1 as Person).printInfo();

  Person p2 = new Person('王二', 22);
  p2.printInfo();
  // p2.name = '张三454545';
  // p2.printInfo();
  // 连缀操作
  p2
    ..age = 20
    ..name = '张三';
}
