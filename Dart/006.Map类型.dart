/* 
Dart 数据类型 Map （字典）
 */

void main() {
  // 第一种定义 Map 定义方法
  var parson = {
    "name": "zhan",
    "age": 20,
    "work": ['程序员', '工程师']
  };
  print(parson["name"]);
  print(parson["work"]);

  // 第二种定义 Map 定义方法

  Map newObj = new Map();
  newObj["name"] = '张三';

  print(newObj["name"]);
}
