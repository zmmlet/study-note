/* Dart 是一门使用类和单继承的面向对象语言，所有的对象都是类的实例，并且所有的类都是Object的子类
  一个类通常由属性和方法组成 */

// 类名首字母大写
// class Person {
//   String name = "张三";
//   int age = 25;

//   // 构造函数：默认构造函数名称和类名一样
//   Person() {
//     print("构造函数里面的内容 这个方法在实例化的时候触发");
//   }

//   // void 标识类中的方法没有返回值
//   void getInfo() {
//     print("${this.age}--this输出--${this.name}");
//   }
// }

/* 1.默认构造函数普通写法 */

// class Person {
//   String name;
//   int age;

//   // 默认构造函数名称和类名一样
//   Person(String name, int age) {
//     this.name = name;
//     this.age = age;
//   }

//   // void 标识类中的方法没有返回值
//   void getInfo() {
//     print("${this.age}----${this.name}");
//   }
// }

/* 2.默认构造函数简写 */
class Person {
  String name;
  int age;

  // 默认构造函数名称和类名一样
  Person(this.name, this.age);
  // void 标识类中的方法没有返回值
  void getInfo() {
    print("${this.age}----${this.name}");
  }
}

void main() {
  Person p = new Person("张三", 25);
  p.getInfo();

  Person p2 = new Person("王五", 23);
  p2.getInfo();
}
