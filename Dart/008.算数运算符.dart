/* 
算数运算符
+(加) -(减) *(乘) /(除) %(取余) ~/(取整)
 */

void main() {
  int a = 13;
  int b = 5;

  print(a + b);
  print(a - b);
  print(a * b);
  print(a / b);
  print(a % b);
  print(a ~/ b);
}
