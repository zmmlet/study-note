/* 
mixins 的中文意思是混入，就是在类中混入其他功能
在 Dart 中可以是红mixins实现类似多继承的功能

因为 mixins 使用的条件，随着Dart版本一直在边，这里讲的Dart2.x中使用 mixins 条件
1.作为 mixins 的类只能继承自 Object ，不能够继承其他类
2.作为 mixins 的类不能有构造函数
3.一个类可以 mixins 多个 mixins 类
4.mixins 绝不是继承，也不是接口 而是一种全新的特性
 */
class Person {
  String name;
  num age;
  Person(this.name, this.age);
  printInfo() {
    print('Person 类');
    print('${this.name}---${this.age}');
  }
}

class A {
  String info = 'this is A';
  void printA() {
    print('A');
  }

  void run() {
    print('RUN A');
  }
}

class B {
  void printB() {
    print('B');
  }

  void run() {
    print('RUN B');
  }
}

// C 类首先 通过关键字 extends 继承 Person 类再使用关键字 with 混入 A,B 两个类
// 由于混入的A,B类中不可以写构造函数，所有我们可以在继承的 Person 类中进行写构造函数
// 这个时候 C类就具备了Person 和 A,B 的所有方法

// 注意：with关键字后混入的类如果方法名和前面混入的类方法名相同，则后面的覆盖前面的
class C extends Person with B, A {
  C(String name, num age) : super(name, age);
}

void main() {
  C c = new C('张三', 22);
  c.printA();
  c.printB();
  print(c.info);
  c.printInfo();
  c.run();
}
