/* 
面向对象三大特性：封装、继承、多态
Dart 中的类的继承
  1. 子类使用 extends 关键字来继承父类
  2. 子类会继承父类里面可见的属性和方法但是不会继承构造函数
  3. 子类能覆写父类的方法 getter 和 setter
 */
class Person {
  String name;
  num age;
  // 实例化动态传入
  // 匿名构造函数
  Person(this.name, this.age);
  // 命名构造函数
  Person.xxxName(this.name, this.age);
  void printInfo() {
    print("${this.name}---${this.age}");
  }

  work() {
    print("${this.name}---${this.age}");
  }
}

class Web extends Person {
  String sex;
  // 匿名构造函数调用
  // Web(String name, num age, String sex) : super(name, age) {
  //   this.sex = sex;
  // }
  // 命名构造函数调用
  Web(String name, num age, String sex) : super.xxxName(name, age) {
    this.sex = sex;
  }

  run() {
    print("${this.name}--${this.age}--${this.sex}");
  }
}

void main() {
  Person p = new Person('张三', 22);
  p.printInfo();

  Person p1 = new Person('李四', 20);
  p1.printInfo();

  Web w = new Web('王五', 23, '男');
  w.printInfo();
  w.run();
}
