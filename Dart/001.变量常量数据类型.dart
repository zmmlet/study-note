// 应用执行入口

/* 
  变量命名规则：
    1. 注意：标识符开头不能是数字
    2. 标识符不能是保留字和关键字
    3. 变量的命名区分大小写
    4. 命名见名思意
    5. 必须由字符，数字下划线不能使用$进行开头
 */

void main() {
/* 变量声明 */
  // var 声明
  var USERNAME = '人员姓名';
  USERNAME = 'GOOD';
  print(USERNAME);

  // int 类型
  int abc = 1100;

  // double 类型
  double aa = 3.012;

  print(abc + aa);
  // double 类型转 String 类型
  double c = double.parse("1.12121");
  print(c);

  // String 类型
  String aaa = 1122.toString();
  String job = 1212.1212.toStringAsFixed(2);
  print(aaa);
  print(job);

  // bool
  bool result = 1 == '1';
  print(result);

  // List 数组类型
  List list = [0, 1, 2, 3, 4, 5, 6];
  print(list);

  List newList = new List();
  newList.add(2);
  newList.addAll([4, 6, 8, 10]);
  print(newList);
  print(newList.length);
  print(newList.first);
  print(newList.last);
  print(newList[2]);
  print(newList.remove(10));

  // Map 对象类型
  Map obj = {"x": 1, "y": 2};
  Map newObj = new Map();
  newObj['z'] = 100;
  newObj['b'] = '字符';

  print(obj);
  print(newObj);

  /* 常量声明 */
  // const 常量声明
  const PI = 3.1415926;
  print(PI);

  // final 常量声明
  final PAI = 415454;
  print(PAI);

  final a = new DateTime.now();
  print(a);
  // 不可以使用 const 声明获取当前时间 final 可以进行获取
  // const b = new DateTime.now()
  /* final 和 const 的区别：
    final 可以开始不赋值，只赋值一次；而final 不仅有const的编译时常量的特性，
    最重要的它是运行时常量、贝宁且final是惰性初始换，即在运行时第一次使用前才初始换
   */
}
