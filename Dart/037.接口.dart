/* 
Dart 中的接口 和 java接口区别
 首先dart的几口没有 interface 关键字定义接口，而普通类或抽现象类都可以作为接口被实现
 同样使用 implements 关键字进行实现
 但是 dart 的接口有点奇怪，如果实现的类是普通类，会将普通类和抽象中的属性方法全部都需要覆写一遍
 而因为抽象类可以定义抽象方法，普通类不可以，所有一般如果要实现像 java 接口那样的样式，一般会使用抽象类
 建议使用抽象类定义接口
 */

// 定义 DB库 支持 MySQL, mssql,  mongodb
// MySQL, MsSql,  mongodb 三个类型里面都有同样的方法
abstract class Db {
  // 当做接口 接口：约定、规范
  String uri; // 数据库链接地址
  add(String data);
  save();
  delete();
}

// implements 关键字进行实现
class MySQL implements Db {
  @override
  add(String data) {
    return null;
  }

  @override
  delete() {
    return null;
  }

  @override
  save() {
    return null;
  }

  @override
  String uri;
}

class MsSql implements Db {
  @override
  String uri;

  MsSql(this.uri);

  @override
  add(String data) {
    print('MsSql的新增方法' + data);
  }

  @override
  delete() {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  save() {
    // TODO: implement save
    throw UnimplementedError();
  }
}

void main() {
  MsSql mssql = new MsSql('xxxx');
  mssql.add('list');
}
