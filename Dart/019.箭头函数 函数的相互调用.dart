void main() {
  /* 1.需求：适应forEach 打印下面的List 里面的数据 */
  List list = ['苹果', '香蕉'];
  list.forEach((value) => {print(value)});

  /* 2.需求：修改下面list里面的数据，让数组大于2的值乘以2 */
  List numList = [1, 2, 3, 4];
  var newList = numList.map((value) {
    if (value > 2) {
      return value * 2;
    }
    return value;
  });
  print(newList.toList());
  // 箭头函数
  var newListArrow = numList.map((value) => value > 2 ? value * 2 : value);
  print(newListArrow.toList());

  /* 3.函数相互调用
    需求：1.定义一个方法 isEvenNumber 来判断一个数是否为偶数
    2.定义一个方法打印1-n以内的所有偶数
   */
  // 1.判断是否为偶数
  bool isEvenNumber(int n) {
    if (n % 2 == 0) {
      return true;
    }
    return false;
  }

  // 2.打印偶数
  evenNumbersHandle(int n) {
    for (var i = 1; i <= n; i++) {
      if (isEvenNumber(i)) {
        print(i);
      }
    }
  }

  evenNumbersHandle(12);
}
