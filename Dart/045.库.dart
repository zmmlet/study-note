/* 
Dart 中 库的使用是通过 import 关键字引入的
library 指令可以创建一个库 ，每个 Dart 文件都是一个库 ，即使没有使用 library 指令来指定

Dart 中的库主要有三类：
  1.自定义的库
    import 'lib/xxx.dart'
  2. 系统内置库
    import 'dart:math';
    import 'dart.import';
    import 'dart.convert';
  3.Pub 包管理系统中的库
    https://pub.dev/packages
    https://pub.flutter-io.cn/packages
    https://pub.dartlang.org/flutter/

    1.需要在自己项目的根目录建一个 pubspec.yaml 
    2.在 pubspec.yaml 文件 配置名称、描述、依赖等信息
    3.然后运行 pub get 获取包下载到本地
    4.项目中引入库 import 'package:http/http.dart' as http; 看文档
 */
