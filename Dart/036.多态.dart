/* 
Dart 中的多态
 允许将子类类型的指针赋值给父类类型的指针，同一个函数调用会有不同的执行结果
 子类的实例赋值给父类的引用
 多态就是父类定义的一个方法不去实现，让其继承他的子类去实现，每个子类有不同表现
 */
abstract class Animal {
  eat(); // 抽象方法
  // 抽象类中的方法
  printInfo() {
    print('抽象类中的方法');
  }
}

// 在子类中 抽象方法必须全部实现
class Dog extends Animal {
  @override
  eat() {
    print('🐕吃骨头');
  }
}

class Cat extends Animal {
  @override
  eat() {
    print('🐱吃🐟');
  }
}

void main() {
  Dog d = new Dog();
  d.eat();
  d.printInfo();

  Cat c = new Cat();
  c.eat();

  // 抽象类无法直接进行实例化
  // Animal a = new Animal();
}
