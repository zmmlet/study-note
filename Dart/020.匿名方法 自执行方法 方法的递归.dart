void main() {
  /* 
  1.匿名函数：没有函数名称的函数
   */

  // 第一种调用方式：创建一个匿名函数，并将匿名函数赋值给变量 add，用 add来进行函数的调用
  var add = (int x, int y) {
    var z = x + y;
    print("x+y=$z");
  };
  add(1, 45);

  // 第二种调用方式：使用()将匿名函数括起来，然后后面再加一对小括号（包含参数列表）
  ((int min, int max) {
    var total = min * max;
    print(total);
  })(12, 99);

  /* 2.自执行匿名函数：即定义和调用为一体，创建了一个匿名的函数，并立即执行它，
  由于外部无法引用它内部的变量，因此在执行完后很快就会被释放，关键是这种机制不会污染全局对象
   */
  ((int n) {
    print('自执行方法$n');
  })(12);

  /* 3.方法的递归 */
  var sum = 1;
  fn(n) {
    sum * n;
    if (n == 1) {
      return;
    }
    fn(n - 1);
  }

  fn(5);
  print(sum);

  /* 通过递归方法，求1-100的和 */
  var totalNum = 0;
  functionAdd(int m) {
    totalNum += m;
    if (m == 0) {
      return;
    }
    functionAdd(m - 1);
  }

  functionAdd(100);
  print(totalNum);
}
