class Person {
  String name;
  num age;
  // 默认构造函数简写
  Person(this.name, this.age);
  Person.setInfo(String name, int age) {
    this.name = name;
    this.age = age;
  }
  void printInfo() {
    print("Person1${this.name}---${this.age}");
  }
}
