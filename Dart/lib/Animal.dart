class Animal {
  String _name; // 私有属性
  int age;
  // 默认构造函数简写
  Animal(this._name, this.age);

  void printInfo() {
    print("${this._name}-------${this.age}");
  }

  // 定义共有方法来暴露私有属性，使其能在共有下访问
  String getName() {
    return this._name;
  }

  // 定义私有方法
  void _run() {
    print("私有方法");
  }

  exceRun() {
    this._run(); // 类里面方法的相互调用
  }
}
