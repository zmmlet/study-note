// 定义 DB库 支持 MySQL, mssql
// MySQL, MsSql, 类型里面都有同样的方法
abstract class Db {
  // 当做接口 接口：约定、规范
  String uri; // 数据库链接地址
  add(String data);
  save();
  delete();
}
