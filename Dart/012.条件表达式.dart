/* 条件表达式 */

void main() {
  // 1. if else 和 switch case
  // if else
  bool falg = false;
  if (!falg) {
    print('true');
  } else {
    print('false');
  }

  // 判断一个人的成绩 > 60 及格 70 良好 90优秀
  var score = 80;
  if (score > 90) {
    print('优秀');
  } else if (score > 70) {
    print('良好');
  } else if (score > 60) {
    print('及格');
  } else {
    print('不及格');
  }

  // switch case
  var sex = '男';
  switch (sex) {
    case '男':
      print('男');
      break;
    case '女':
      print('女');
      break;
    default:
      print('传递参数错误');
      break;
  }

  // 2. 三目运算符
  var dd = true;
  String c = dd ? "this is true" : "this is false";
  print(c);

  // 3.??运算符

  var jd;
  var kl = jd ?? 10;
  print(kl);
}
