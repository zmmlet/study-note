/* 
逻辑运算符
 */

void main() {
  // !取反
  bool falg = true;
  print(!falg);

  // && 并且：全部为 true 时 值为 true 否则为 false
  bool flag1 = true;
  bool flag2 = false;
  print(flag1 && flag2);

  // ||或者： 全部为false 则为false 否则为 true
  bool flag3 = true;
  bool flag4 = false;
  print(flag3 || flag4);
}
