/* Dart 类型转换 */

void main() {
  // 1. Number 与 String 类型之间的转换
  // Number 类型转换成 String 类型 toString()
  // String 类型转换成 Number 类型 parse

  // 如果str 为空的情况则会出现报错
  // String str = "454.1";
  // var muNumber = double.parse(str);
  // print(muNumber is double);

  // try 抛出异常
  String str = "";
  try {
    var muNumber = double.parse(str);
    print(muNumber is double);
  } catch (err) {
    print(0);
  }

  // int 类型转换 String 类型
  int numberValue = 4567;
  var srtN = numberValue.toString();
  print(srtN is String);

  // 2. 其他类型转换成 Booleans 类型
  // isEmpty: 盘对岸字符串是否为空
  var sstr1 = "xxx";
  if (sstr1.isEmpty) {
    print('为空');
  } else {
    print('不为空');
  }

  // 数字转布尔
  var numm = 123;
  if (numm == 0) {
    print('0');
  } else {
    print('非0');
  }
}
