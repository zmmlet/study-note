void main() {
  /* 1.List */
  // List 里面的属性
  List myList = ['香蕉', '苹果', '橘子', '葡萄', '橙子'];
  print(myList.length);
  print(myList.isEmpty);
  print(myList.isNotEmpty);
  print(myList.reversed);
  var newMyList = myList.reversed.toList();
  print(newMyList);

  // List 里面的方法
  List mylist1 = ['奔驰', '宝马', '奥迪', '大众'];
  mylist1.add('五菱'); // 增加数据，增加一个
  print(mylist1);
  mylist1.addAll(['本田', '现代', '长城']);
  print(mylist1);
  print(mylist1.indexOf('本田')); // indexOf() 查找成功返回索引值，失败返回-1
  print(mylist1.remove('现代')); // 传入具体值
  print(mylist1.removeAt(2)); // 传入索引值删除

  /* 2.Set 
  主要功能：去除数组重复数据
  Set 是没有顺序且不能重复的集和，所以不能通过索引去获取值
  */

  var s = new Set();
  s.addAll(['苹果', '香蕉', '香蕉']);
  print(s);
  print(s.toList());

  /* 3.Map */

  var pserson = {"name": '张三', "sex": '男', "age": 22};
  Map m = new Map();
  m['sex'] = '男';
  print(pserson);
  print(m);
  // 常用属性
  print(pserson.keys.toList());
  print(pserson.values.toList());
  print(pserson.isEmpty);
  print(pserson.isNotEmpty);
  // 常用方法
  pserson.addAll({"obj": '前端工程师', "height": 180});
  print(pserson);
  pserson.remove('sex');
  print(pserson);
  // 查看对象中某个值是否存在
  pserson.containsValue('张三');

  /* 4.forEach */

  List carList = ['奔驰', '宝马', '奥迪', '大众'];
  // for (var i = 0; i < carList.length; i++) {
  //   print(carList[i]);
  // }

  // for (var item in carList) {
  //   print(item);
  // }

  carList.forEach((item) => {print("$item")});

  List numberList = [1, 2, 3, 12];
  // var newNumberList = numberList.map((value) => {value * 2});
  var newNumberList = numberList.where((value) {
    return value > 5;
  });
  // 满足一个 返回true
  var f = numberList.any((value) {
    return value > 5;
  });
  print(newNumberList);
  print(f);
}
