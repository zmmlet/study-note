/* 
Dart 判断数据类型
 is 关键字来判断类型
 */

void main() {
  var str = false;
  //  is 关键字来判断类型
  if (str is String) {
    print('字符串');
  } else if (str is int) {
    print('数字');
  } else {
    print('其他');
  }
}
