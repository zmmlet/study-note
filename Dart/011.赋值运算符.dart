/* 赋值运算 */

void main() {
  // 1.基础赋值运算 =  ??=
  int a = 10;
  print(a);

  int c = 3 + a; // 从右向左
  print(c);

  // b??=23 表示如果b为空的话把23赋值给b
  int b;
  b ??= 12;
  print(b);

  int u;
  u ??= 8;
  print(u);

  // 2.复合赋值运算符 += -= *= /= %= ~/=
  var k = 12;
  k += 10; // 表示k = k + 10;
  print(k);

  k -= 2;
  print(k);

  k *= 12;
  print(k);

  k %= 5;
  print(k);

  k ~/= 4;
  print(k);
}
