// class MyList {
//   List list = <int>[];
//   void add(int value) {
//     this.list.add(value);
//   }

//   List getList() {
//     return list;
//   }
// }

// 定义泛型类
class MyList<T> {
  List list = <T>[];
  void add(T value) {
    this.list.add(value);
  }

  List getList() {
    return list;
  }
}

void main() {
  // 创建固定长度的list
  // List list = List.filled(2, '');
  // list[0] = "张三";
  // list[1] = "李四";
  // print(list);

  // List list = new List.filled(2, '');
  // list[0] = "张三";
  // list[1] = "李四";
  // print(list);

  // 指定类型的list
  // List list = new List<String>.filled(2, '');
  // list[0] = "张三";
  // list[1] = "李四";
  // print(list);

  // 调用 指定类型的 list 类
  // MyList list = MyList();
  // list.add(1);
  // list.add(2);
  // list.add(3);
  // print(list.getList());

  // 调用泛型类 - 不指定类型
  MyList list = new MyList();
  list.add(1);
  list.add('张三');
  list.add(3);
  print(list.getList());

  // 调用泛型类 - 指定类型
  MyList listT = new MyList<int>();
  listT.add(1);
  listT.add(3);
  print(listT.getList());
}
