/* 
语法格式： 
while (表达式/循环条件) {

} 
 
 
do {
  语句/循环体
} while (表达式/循环条件);

 注意：1.最后的分号不要忘记
      2.循环条件中使用的变量需要经过初始化
      3.循环体中，应有结束循环的条件，否则会造成死循环

 */
void main() {
  // 1.打印 1-10
  // int i = 1;
  // while (i <= 10) {
  //   print(i);
  //   i++;
  // }

  // 2. 求 1+2+3+....+100 的和
  // int j = 1;
  // int total = 0;
  // while (j <= 100) {
  //   total += j;
  //   j++;
  // }
  // print(total);

  int s = 1;
  int ss = 0;
  do {
    ss += s;
    s++;
  } while (s <= 100);

  print(ss);
}
