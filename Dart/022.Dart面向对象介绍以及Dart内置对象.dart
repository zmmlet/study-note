/* 
面向对象编程（OOP）的三个基本特征是：封装、继承、多态
  封装：封装是对象和类概念的主要特性。封装，把客观事物封装成抽象的类，
      并且把自己的部分属性和方法提供给其他对象
  继承：面向对象变成（OOP）语言的一个主要功能就是“继承”。继承是指这样一种能力：
  它可以使用现有类的功能
  多态：允许将子类类型的指针赋值给父类类型的指针，同一个函数调用会有不同的执行效果

  Dart 所有的东西都是对象，所有的对象都继承自Object类
  Dart 是一门使用类和单继承的面向对象语言，所有的对象都是类的实例，并且所有的类都是Object的子类
  一个类通常由属性和方法组成
 */
void main() {
  List list = new List();
  list.isEmpty;
  list.addAll(['香蕉', '苹果']);

  Map mapObj = new Map();
  mapObj["username"] = "张三";
  mapObj.addAll({"password": "123456"});

  Object a = 132;
  Object v = true;
  print(a);
  print(v);
}
