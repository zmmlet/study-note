/* 
mixins 的实例类型
mixns 的类型就是其超类的子类型
 */

class A {
  String info = 'this is A';
  void printA() {
    print('A');
  }
}

class B {
  void printB() {
    print('B');
  }
}

class C with B, A {}

void main() {
  var c = new C();
  print(c is A); // true
  print(c is B); // true
  print(c is C); // true

  var a = new A();
  // 所有的类都继承自 Object 类
  print(a is Object); // true
}
