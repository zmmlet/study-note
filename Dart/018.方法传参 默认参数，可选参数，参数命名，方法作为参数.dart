// 调用方法传参
// 1.求1到某个数的和
int addUmber(int number) {
  int totalNum = 0;
  for (var i = 1; i <= number; i++) {
    totalNum += i;
  }
  return totalNum;
}

/* 打印用户信息 */
String printUserInfo(String username, int age, [String sex]) {
  if (sex != null) {
    return "姓名：$username --- 年龄：$age --- 性别：$sex";
  }
  return "姓名：$username --- 年龄：$age";
}

void main() {
  // 1.定义一个方法，求1到这个数组所有数的和 60 1+2+...+60
  print(addUmber(5));

  // 2.定义一个方法打印用户信息
  print(printUserInfo('张三', 25, '男'));

  // 3.定义一个带默认参数的方法
  String deafultParams(String color, [String params = "background"]) {
    return "$params:$color";
  }

  print(deafultParams('#333', 'color'));
}
