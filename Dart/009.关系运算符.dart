/* 
关系运算符 返回为布尔值
== != > < >= <=
 */

void main() {
  int a = 13;
  int b = 5;

  print(a == b);
  print(a != b);
  print(a > b);
  print(a < b);
  print(a >= b);
  print(a <= b);

  if (a > b) {
    print('a 大于 b');
  } else {
    print('a 小于 b');
  }
}
