/* 
Dar 数据类型：字符串类型
 */

void main() {
  // 1.字符串定义的几种方式

  // var str1 = 'this is srt1';
  // var str2 = 'this is srt2';
  // print(str1);
  // print(str2);

  // String str1 = 'this is srt1';
  // String str2 = 'this is srt2';
  // print(str1);
  // print(str2);

  // 多行字符串
  // String str1 = '''this is srt1
  // this is srt1
  // this is srt1
  // this is srt1
  // ''';
  // print(str1);

  // 2. 字符串的拼接
  String str1 = '你好';
  String srt2 = 'Dart';
  print("${str1} ${srt2}");
  print("$str1 $srt2");
  print(str1 + srt2);
}
