/* 

 Dart数据类型：布尔类型
 bool 值 true/false
 */

void main() {
  // 1.bool
  bool flag = true;
  print(flag);
  bool flag1 = false;
  print(flag1);

  // 2. 条件判断语句(判断语句和逻辑运算符会进行隐性转换 true 或 false) 和javaScript一样
  // 判断语句
  var falg3 = true;
  if (falg3) {
    print('true');
  } else {
    print('false');
  }

  // 逻辑运算符
  int a = 132;
  int b = 456;
  a == b ? print('相等') : print('不等');
}
