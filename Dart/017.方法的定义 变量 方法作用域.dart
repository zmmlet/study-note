/* 
内置方法/函数：
print();

自定义方法：
  自定义方法的基本格式
  
  返回类型 方法名称(参数1,参数2,...){
    方法体
    return 返回值;
  }
 */

void printInfo() {
  print('自定义方法');
}

String printUserInfo() {
  return 'this is str';
}

// void 表示 main 方法没有返回值
void main() {
  print('调用系统内置方法');
  printInfo();

  int getNumber() {
    int myNum = 123;
    return myNum;
  }

  var n = getNumber();
  print(n);

  print(printUserInfo());

  // 演示方法作用域
  void xxx() {
    aaa() {
      print('aaa');
    }

    aaa();
  }

  xxx(); // 调用方法
}
