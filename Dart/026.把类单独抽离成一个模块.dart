// 引入抽离出去的模块
import 'lib/Person.dart';

void main() {
  Person p = new Person("张三", 25);
  p.getInfo();
  /* 命名构造函数调用 */
  Person p2 = new Person.now("zhangsan", "1524545");
}
