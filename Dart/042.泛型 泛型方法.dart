/* 
泛型就是解决 类 接口 方法的复用性，以及对不特定数据类型的支持（类型校验）
 */

// 只能传入和返回 string 类型
String getData(String value) {
  return value;
}

// 只能传入和返回 int 类型
int getDataInt(int value) {
  return value;
}

// 采用泛型同时支持 返回 string类型和int类型
// 泛型标识 可以随意写，一般采用T来表示
T getDataT<T>(T value) {
  return value;
}

void main() {
  // 调用时不传入参数
  print(getDataT('xxz'));
  print(getDataT(54545));
  // 调用时指定类型
  print(getDataT<String>('string'));
  print(getDataT<int>(54545));
}
