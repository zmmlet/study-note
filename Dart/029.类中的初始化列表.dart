// Dart 中可以在构造函数体运行之前初始化实例变量

class Rect {
  int height;
  int width;
  // 实例化之前进行操作：初始化列表
  Rect()
      : height = 11,
        width = 2 {
    print('${this.height}---${this.width}');
  }
  getArea() {
    return this.height * this.width;
  }
}

void main() {
  Rect r = new Rect();
  print(r.getArea());
}
