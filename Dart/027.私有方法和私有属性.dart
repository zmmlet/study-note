/* 
Dart和其他面向对象的语言不一样，Dart 中没有 pubilc private protected 这些访问修饰符
但是我们可以使用_把一个属性或方法定义成私有，但是必须是在一个单独的文件，不然还是共有的
 */

import 'lib/Animal.dart';

void main() {
  Animal a = new Animal("zhang", 22);

  print(a.age);
  print(a.getName()); // 使用共有方法获取私有属性
  a.exceRun(); // 间接的调用私有方法
}
