/* 
Dart 中的静态成员
1. 使用 static 关键字来实现类级别的变量和函数
2. 静态方法不能访问非静态成员，非静态方法可以访问静态成员‘
 */
class Person {
  String name = "张三";
  void show() {
    print(name);
  }
}

// 静态
class PersonInfo {
  static String name = '李四';
  int age = 20;
  static void show() {
    print(name);
  }

  /* 非静态方法可以访问静态成员以及非静态成员 */
  void printInfo() {
    print(name); // 访问静态属性
    show(); // 访问静态方法
    print(this.age); // 访问非静态属性
  }

  // 静态方法
  static void printUserInfo() {
    print(name); // 静态属性
    show(); // 静态静态方法
    // print(this.age); // 静态方法不可以访问非静态的属性和方法
  }
}

void main() {
  // 非静态必须实例化后进行调用
  var p = new Person();
  p.show();

  // 静态成员访问
  print(PersonInfo.name);
  PersonInfo.show();
}
