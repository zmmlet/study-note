/* 
面向对象三大特性：封装、继承、多态
Dart 中的类的继承
  1. 子类使用 extends 关键字来继承父类
  2. 子类会继承父类里面可见的属性和方法但是不会继承构造函数
  3. 子类能覆写父类的方法 getter 和 setter
 */
class Person {
  String name;
  num age;
  // 实例化动态传入
  // 匿名构造函数
  Person(this.name, this.age);
  void printInfo() {
    print("${this.name}---${this.age}");
  }

  work() {
    print("${this.name}---${this.age}");
  }
}

class Web extends Person {
  Web(String name, num age) : super(name, age);

  run() {
    print('run');
    super.work(); // 调用父类的方法
  }

  // 覆写父类的方法 命名和父类一样 @override 标识重写父类
  @override
  void printInfo() {
    print("姓名：${this.name}-年龄：${this.age}");
  }

  @override
  work() {
    print('${this.name}code');
  }
}

void main() {
  Web w = new Web('ZHANG', 20);
  w.printInfo();
}
