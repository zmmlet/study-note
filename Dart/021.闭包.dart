void main() {
  /* 
  闭包：
    1.全局变量特点：全局变量常驻内存、全局变量污染全局
    2.局部变量特点：不常驻内存回被垃圾机制回收，不会污染全家具

    需要实现功能：
      1.常驻内存
      2.不污染全局

      产生了闭包、闭包可以解决这一问题
      闭包：函数嵌套函数，内部函数会调用外部函数的变量或参数，
           变量或参数不会被系统
      闭包的写法：函数嵌套函数，并 return 里面的函数，这样就形成了闭包
   */

  // printInfo() {
  //   var mynum = 123;
  //   mynum++;
  //   print(mynum);
  // }

  // 闭包
  fn() {
    var number = 123;
    return () {
      number++;
      print(number);
    };
  }

  var constA = fn();
  constA();
  constA();
  constA();
}
