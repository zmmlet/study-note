class Person {
  String name;
  int age;
  String username;
  String password;

  // 默认构造函数名称和类名一样
  Person(this.name, this.age);

  /* dart 命名构造函数可以写多个 */
  // 命名构造函数
  Person.now(String username, String password) {
    print('我是命名构造函数');
    this.username = username;
    this.password = password;
    print("账号：${this.username}---密码：${this.password}");
  }

  Person.setUserInfo(String username, String password) {
    this.username = username;
    this.password = password;
    print("账号1：${this.username}---密码1：${this.password}");
  }

  // void 标识类中的方法没有返回值
  void getInfo() {
    print("${this.age}----${this.name}");
  }
}

void main() {
  Person p = new Person("张三", 25);
  p.getInfo();
  /* 命名构造函数调用 */
  Person p2 = new Person.now("zhangsan", "1524545");

  Person p3 = new Person.setUserInfo("hei", 'LL');
}
