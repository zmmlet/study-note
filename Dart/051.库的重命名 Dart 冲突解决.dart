/* 
1.冲突解决
当引入两个库中有相同名称表示符的时候，如果是java通常我们通过写完整包路径来指定使用的库

import 'package:lib1/lib1.dart';
import 'package:lib2/lib2.dart as lib2';

Element element1 = new Element(); // User Element from lib1. 
Element element2 = new lib2.Element(); // User Element from lib2. 
 */

import 'lib/Person1.dart';
// 通过 as 对引入冲突的库进行重新命名
import 'lib/Person2.dart' as lib;

main(List<String> args) {
  Person p1 = new Person('张三', 23);
  p1.printInfo();

  lib.Person p2 = new lib.Person('李四', 24);
  p2.printInfo();
}
